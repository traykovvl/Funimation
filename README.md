# Funimation Social Network

Telerik Academy Alpha Java May 2019 - Team 1 
& QA Team July\
(The project was mentored by Upnetix & ScaleFocus)


| | Team members | |
| --- | :----: | --- | 
| <b>Java</b> |   | <b>QA</b> |
Kalina Zhecheva  |  | Iva Stoycheva |
Lyubka Nikolova  |  | Martin Yordanov | 
Vladimir Traykov  |  | Kremena Stoyanova | 

## Project description:


Funimation is a dynamic web page application written in Java that enables users to search and offer Stand-up comedians and Kids entertainment by connection with people, create, comment and like posts and get a feed of the newest/most relevant posts of their connections.

It uses Spring MVC Framework with Thymeleaf template engine along with REST API. The API is documented using Swagger.

The public part of the projects is visible without authentication - at the home page - with public profile search and public feed, Login form with forgotten password and registration form with Email confirmation.

The list of offers for Stand-up comedians and Kids entertainment are reachable from the home page by image links.

Registered users have a profile page where they can edit their personal and login information, upload a profile and background pictures and set their visibility (public – for everyone incl. non-registered users & private – only for connected profiles). Users can change password.
Once registered – the user can make posts for Stand-up comedians and Kids entertainment with specific details for each (age, city, date, time). The posts can be both public (visible for everyone, including non-registered users) and private (visible for friends). 

Users have personalized post feed, where they can see all the posts from their friends in chronological order. They can also: Like or unlike a post or comment it with reply. The post total like count is showed along the post content.
System administrators have administrative access to the system and all major information objects as Edit/Delete profiles, posts & comments.

The project is being verified for stability and functionality by the <b>QA team</b> and the raised issues are timely reviewed and fixed.

The data of the application is stored in a relational database – MySQL/MariaDB.

The project is written in Java - JDK version is 1.8. It uses Spring Security and users have two roles: user and administrator.
JUnit tests have 98% coverage.
The Front-end is based on HTML5 with Thymeleaf, JavaScript and Ajax.

<hr>

How to run the project:
1.	You will find SQL creation script in the folder named: “sql_database”. The SQL script must be run against a MySQL/MariaDB database.
2.	After step 1 you can compile and run the project and access it on a local machine by opening the web address: http://localhost:8080
3. Then the API documentation can be seen at
http://localhost:8080/swagger-ui.html or http://localhost:8080/v2/api-docs

<hr>

The project is available <b>live</b> at: <b>http://www.funimation.ml</b> or <b>https://funimation1.herokuapp.com/</b>

The live path to the application's API documentation for the current setup is https://funimation1.herokuapp.com/swagger-ui.html

The development process can be traced into the <b>Trello</b> board at:
https://trello.com/b/3IbkajSC/social-network



### Project database structure


![alt text1][logo]

[logo]: ./Schema_team-one-soc-net.jpg "Project database structure"