// SEARCH ANIMATION START

$("#searchAnimation").click(function(){
    var x = document.getElementById("searchingNow")
    x.style.display = "block";
});

$("#modalSearching").click (function(){
        var x = document.getElementById("searchingNow")
        x.style.display = "none";
});

// SEARCH ANIMATION END


//REMEMBER CHECKBOX COOKIE

var formValues = JSON.parse(localStorage.getItem('formValues')) || {};
var $checkboxes = $("#checkbox-container :checkbox");
var $button = $("#checkbox-container button");

function updateStorage(){
    $checkboxes.each(function(){
        formValues[this.id] = this.checked;
    });

    formValues["buttonText"] = $button.text();
    localStorage.setItem("formValues", JSON.stringify(formValues));
}

$checkboxes.on("change", function(){
    updateButtonStatus();
    updateStorage();
});

// On page load
$.each(formValues, function(key, value) {
    $("#" + key).prop('checked', value);
});

//REMEMBER CHECKBOX COOKIE



//FUNCTION TO SEARCH IN DATABASE FOR USERS BY USERNAME AND EMAIL
let timeout = null;
$('#search_input1').on('keyup', function () {
    const $URL = `/api/v1/users/filtered?filterParam=${this.value}`;
    const $VERB = 'GET';
    const $DATATYPE = 'json';
    const $usersDiv = $('#users-container');
    clearTimeout(timeout);
    timeout = setTimeout(function () {
        $.ajax({
            type: $VERB,
            url: $URL,
            dataType: $DATATYPE,
            success: function (users) {
                $usersDiv.empty();
                users.forEach((user) => {
                    const $userDate = user.updateDate;
                    const $date = convertTimeStampToDate($userDate);
                    $usersDiv.append(`<div class="feed-item" style="background-color: #f5e79e">
<a href="/timeline-about.html/${user.id}" title="${user.firstName + ' ' + user.lastName}">
<img src="${user.userPhoto}" alt="" class="img-responsive profile-photo-sm"><span class="online-dot"></span><a/>
<div class="live-activity">
<p>
<span>
<a href="/timeline-about.html/${user.id}" class="profile-link" title="${user.firstName + ' ' + user.lastName}">${user.username}</a> last updated on ${$date}
</span>
<br/>
<span>${user.email}</span>
</p>
</div>
</div>`)
                }, 500);
            },
            error: function () {
                alert('Error loading users!')
            }
        });
    });
});


// mobile menu button actions
function openNav() {
    var x = document.getElementById("navMobileLinks");
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else {
        x.className = x.className.replace(" w3-show", "");
    }
}


var cutain1 = document.getElementById('curtain1');
curtain1.addEventListener('click', function () {
    curtain1.offsetHeight; /* CSS reflow */
    curtain1.classList.add('moved');
});


/* SHOW SNACKBAR - CHEERS ON PRESS */
function launch_toast(href) {
    var x = document.getElementById("toast")
    x.className = "show";
    setTimeout(function () {
        x.className = x.className.replace("show", "");
    }, 5000);
    setTimeout(function () {
        window.location.href = href;
    }, 7000);
    return true;
}

/* END SHOW SNACKBAR - CHEERS ON PRESS */


/* FILTER CARDS FUNCTION */

function myFilterFunction() {
    // var input, filter, cards, cardContainer, h5, title, i;
    input = document.getElementById("myFilter");
    filter = input.value.toUpperCase();
    cardContainer = document.getElementById("myItems");
    cards = cardContainer.getElementsByClassName("card sticky-action");
    for (i = 0; i < cards.length; i++) {
        title = cards[i].querySelector(".card-action .card-content h5.card-title");
        if (title.innerText.toUpperCase().indexOf(filter) > -1) {
            cards[i].style.display = "";
        } else {
            cards[i].style.display = "none";
        }
    }
}

// function myFilterFunction() {
//   var input, filter, cards, cardContainer, h5, title, i;
//   input = document.getElementById("myFilter");
//   filter = input.value.toUpperCase();
//   cardContainer = document.getElementById("myItems");
//   cards = cardContainer.getElementsByClassName("card");
//   for (i = 0; i < cards.length; i++) {
//       title = cards[i].querySelector(".card-body h5.card-title");
//       if (title.innerText.toUpperCase().indexOf(filter) > -1) {
//           cards[i].style.display = "";
//       } else {
//           cards[i].style.display = "none";
//       }
//   }
// }


/* END FILTER CARDS FUNCTION */

/* FILTER ELEMENTS BY BUTTON FUNCTION */
filterSelection("all")

function filterSelection(c) {
    var x, i;
    x = document.getElementsByClassName("filterDiv");
    if (c == "all") c = "";
    for (i = 0; i < x.length; i++) {
        w3RemoveClass(x[i], "show");
        if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
    }
}

function w3AddClass(element, name) {
    var i, arr1, arr2;
    arr1 = element.className.split(" ");
    arr2 = name.split(" ");
    for (i = 0; i < arr2.length; i++) {
        if (arr1.indexOf(arr2[i]) == -1) {
            element.className += " " + arr2[i];
        }
    }
}

function w3RemoveClass(element, name) {
    var i, arr1, arr2;
    arr1 = element.className.split(" ");
    arr2 = name.split(" ");
    for (i = 0; i < arr2.length; i++) {
        while (arr1.indexOf(arr2[i]) > -1) {
            arr1.splice(arr1.indexOf(arr2[i]), 1);
        }
    }
    element.className = arr1.join(" ");
}

// Add active class to the current button (highlight it)
var btnContainer = document.getElementById("myBtnContainer");
var btns = btnContainer.getElementsByClassName("btn");
for (var i = 0; i < btns.length; i++) {
    btns[i].addEventListener("click", function () {
        var current = document.getElementsByClassName("active");
        current[0].className = current[0].className.replace(" active", "");
        this.className += " active";
    });
}
/* FILTER ELEMENTS BY BUTTON FUNCTION */


/* DARK MODE */
$('.dark-toggle').on('click', function () {
    if ($(this).find('i').text() == 'brightness_4') {
        $(this).find('i').text('brightness_high');
    } else {
        $(this).find('i').text('brightness_4');
    }
});
// });
/*END DARK MODE*/


$(document).ready(function () {
    $('.fixed-action-btn').floatingActionButton();
});


// (function($){
// $(function(){

// $('.sidenav').sidenav();
// $('.parallax').parallax();

// }); // end of document ready
// })(jQuery); // end of jQuery name space
