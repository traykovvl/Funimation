/* GO TO BOTTOM START */

$(document).ready(function () {
    $("#user-div2").click(function () {
        $("html, body").animate({scrollTop: $(document).height() - $(window).height()});
    });
});

/* GO TO BOTTOM END */

/* POST APPEND IN INDEX PAGE START*/
$(document).ready(function () {
    $.ajax({
        type: "GET",
        // dataType: 'jsonp',
        url: "/api/users/all/0/6"
        ,
        success: function (urlObject) {
            if (urlObject && urlObject[1]) {
                $(getObjectsElement(urlObject)).append(urlObject);
                // $("#post-div1").hide();
            } else {
                $("#loading").replaceWith('<p style="text-align: center; color: red;">End of list.</p>')
            }
        },
        error: (err) => console.log(err)
    }).always(function () {
        is_loading = false;
    });

    function getObjectsElement(users) {
        var allObjects = Object.values(users);
        for (let i = 0; i < allObjects.length; i++) {
            let object = `
        <div class="w3-container w3-card w3-white w3-round w3-margin"><br>
                            <img src="data:image/png;base64,${allObjects[i].photo.photo}" alt="Avatar" class="w3-left w3-circle w3-margin-right"

                                 style="width:60px"> <span class="w3-right w3-opacity">16
                    min</span>
                            <h4 style="text-align: left;">User ID: ${allObjects[i].userId}</h4>
                            <h4 style="text-align: left;">${allObjects[i].email}</h4>
                            <br>
                            <hr class="w3-clear">
                            <p style="text-align: left;">Full name: ${allObjects[i].firstName} ${allObjects[i].lastName}</p>
                            <p style="text-align: justify;">Occupation: ${allObjects[i].occupation.occupationArea}</p>
                            <button type="button" class="w3-button w3-theme-d1 w3-margin-bottom"><i

                                    class="fa fa-thumbs-up"></i> Like</button> <button type="button"

                                                                                       class="w3-button w3-theme-d2 w3-margin-bottom"><i class="fa fa-comment"></i>
                                Comment</button> </div>`;

            $('#user-div1').append(object);
        }
    }
});

var startPage = 2;
$(document).ready(function () {
    $(document).ajaxStart(function () {
        $("#loading").show();
    }).ajaxStop(function () {
        $("#loading").hide();
    });
    var is_loading = false;

    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 100) {
            if (is_loading) {
                return;
            }
            is_loading = true;

            $.ajax({
                type: "GET",
                // dataType: 'jsonp',
                url: "/api/users/all/" + startPage + "/3"
                ,
                success: function (urlObject) {
                    if (urlObject && urlObject[1]) {
                        $(getObjectsElement(urlObject)).append(urlObject);
                    } else {
                        $("#loading").replaceWith('<p style="text-align: center; color: red;">End of list.</p>')
                    }
                },
                error: (err) => console.log(err)
            }).always(function () {
                is_loading = false;
            });
        }

        function getObjectsElement(users) {
            var allObjects = Object.values(users);

            for (let i = 0; i < allObjects.length; i++) {
                let object = `
        <div class="w3-container w3-card w3-white w3-round w3-margin"><br>
                            <img src="data:image/png;base64,${allObjects[i].photo.photo}" alt="Avatar" class="w3-left w3-circle w3-margin-right"

                                 style="width:60px"> <span class="w3-right w3-opacity">16
                    min</span>
                            <h4 style="text-align: left;">User ID: ${allObjects[i].userId}</h4>
                            <h4 style="text-align: left;">${allObjects[i].email}</h4>
                            <br>
                            <hr class="w3-clear">
                            <p style="text-align: left;">Full name: ${allObjects[i].firstName} ${allObjects[i].lastName}</p>
                            <p style="text-align: justify;">Occupation: ${allObjects[i].occupation.occupationArea}</p>
                            <button type="button" class="w3-button w3-theme-d1 w3-margin-bottom"><i

                                    class="fa fa-thumbs-up"></i> Like</button> <button type="button"

                                                                                       class="w3-button w3-theme-d2 w3-margin-bottom"><i class="fa fa-comment"></i>
                                Comment</button> </div>`;

                $('#user-div1').append(object);
            }
            startPage++;
        }
    });
});
/* POST APPEND IN INDEX PAGE END*/