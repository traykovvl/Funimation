/* GO TO BOTTOM START */

$(document).ready(function () {
    $("#post-div2").click(function () {
        $("html, body").animate({scrollTop: $(document).height() - $(window).height()});
    });
});

/* GO TO BOTTOM END */

/* POST APPEND IN INDEX PAGE START*/
$(document).ready(function () {
    $.ajax({
        type: "GET",
        // dataType: 'jsonp',
        url: "/api/posts/0/6"
        ,
        success: function (urlObject) {
            if (urlObject && urlObject[1]) {
                $(getObjectsElement(urlObject)).append(urlObject);
            } else {
                $("#loading").replaceWith('<p style="text-align: center; color: red;">End of list.</p>')
            }
        },
        error: (err) => console.log(err)
    }).always(function () {
        let is_loading = false;
    });

    function getObjectsElement(posts) {
        var allObjects = Object.values(posts);
        for (let i = 0; i < allObjects.length; i++) {
            let commentHref = "/comments/new/" + allObjects[i].postId;
            let postHref = "/posts/like/" + allObjects[i].postId + "?page=one";
            let photo = "/pictures/default_avatar.png";
            let publicPhoto = allObjects[i].author.photo.photoSrc;
            if (allObjects[i].author.photo.privacy.privacyId === 1) {
                publicPhoto = photo;
            }
            let counter = 0;
            for (let j = 0; j < allObjects[i].usersWhoLikedPost.length; j++) {
                counter++;
            }
            let object = `
                            <div id="post-${allObjects[i].postId}" class="w3-container w3-card w3-white w3-margin">
                            <br>
                            <img src="${publicPhoto}" alt="Avatar" class="w3-left w3-circle w3-small w3-margin-right" style="width:40px; height:40px">
                            <span class="w3-right w3-opacity">${allObjects[i].timeStamp}</span>
                            <h5 style="text-align: left;">${allObjects[i].author.firstName} ${allObjects[i].author.lastName}</h5>
                            <p style="text-align: justify; margin: 0%"><b>${allObjects[i].title}</b></p>
                            <p style="text-align: justify; margin: 0%">${allObjects[i].postMessage}</p>
                            <form class="w3-button w3-theme-d1" style="padding: 0%; margin-bottom: 0%;" action="${commentHref}">
                                <button class="w3-button w3-theme-d2 w3-margin-bottom" style="margin-bottom: 0% !important;" type="submit"><i class="fa fa-comment"></i> Comment</button>
                            </form>
                            <form class="w3-button w3-theme-d1" style="padding: 0%; margin-bottom: 0%;" action="${postHref}" method="post">
                                <button class="w3-button w3-theme-d1 w3-margin-bottom" style="margin-bottom: 0% !important;" type="submit"><i class="fa fa-thumbs-up"></i> Like
                                <span class="w3-badge w3-right w3-small w3-green">${counter}</span></a></button>
                            </form>
                            <div id="comments-list" style="margin-left: 5%; text-align: justify;">
                            <!-- Comments -->
                            </div>
                            </div>`;

            $('#post-div1').append(object);
            getComments(allObjects[i].postId);
        }
    }
});

var startPage = 2;
$(document).ready(function () {
    $(document).ajaxStart(function () {
        $("#loading").show();
    }).ajaxStop(function () {
        $("#loading").hide();
    });
    var is_loading = false;

    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 100) {
            if (is_loading) {
                return;
            }
            is_loading = true;

            $.ajax({
                type: "GET",
                // dataType: 'jsonp',
                url: "/api/posts/" + startPage + "/3"
                ,
                success: function (urlObject) {
                    if (urlObject && urlObject[1]) {
                        $(getObjectsElement(urlObject)).append(urlObject);
                    } else {
                        $("#loading").replaceWith('<p style="text-align: center; color: red;">End of list.</p>')
                    }
                },
                error: (err) => console.log(err)
            }).always(function () {
                is_loading = false;
            });
        }

        function getObjectsElement(posts) {
            var allObjects = Object.values(posts);
            for (let i = 0; i < allObjects.length; i++) {
                let commentHref = "/comments/new/" + allObjects[i].postId;
                let postHref = "/posts/like/" + allObjects[i].postId;
                let photo = "/pictures/default_avatar.png";
                let publicPhoto = allObjects[i].author.photo.photoSrc;
                if (allObjects[i].author.photo.privacy.privacyId === 1) {
                    publicPhoto = photo;
                }
                let counter = 0;
                for (let j = 0; j < allObjects[i].usersWhoLikedPost.length; j++) {
                    counter++;
                }
                let object = `
      <div id="post-${allObjects[i].postId}" class="w3-container w3-card w3-white w3-round w3-margin">
                            <br>
                            <img src="${publicPhoto}" alt="Avatar" class="w3-left w3-circle w3-small w3-margin-right" style="width:40px; height:40px">
                            <span class="w3-right w3-opacity">${allObjects[i].timeStamp}</span>
                            <h5 style="text-align: left;">${allObjects[i].author.firstName} ${allObjects[i].author.lastName}</h5>
                            <p style="text-align: justify; margin: 0%"><b>${allObjects[i].title}</b></p>
                            <p style="text-align: justify; margin: 0%">${allObjects[i].postMessage}</p>
                            <form class="w3-button w3-theme-d1" style="padding: 0%; margin-bottom: 0%;" action="${commentHref}">
                                <button class="w3-button w3-theme-d2 w3-margin-bottom" style="margin-bottom: 0% !important;" type="submit">
                                <i class="fa fa-comment"></i> Comment</button>
                            </form>
                            <form class="w3-button w3-theme-d1" style="padding: 0%; margin-bottom: 0%;" action="${postHref}" method="post">
                                <button class="w3-button w3-theme-d1 w3-margin-bottom" style="margin-bottom: 0% !important;" type="submit">
                                <i class="fa fa-thumbs-up"></i> Like
                                <span class="w3-badge w3-right w3-small w3-green">${counter}</span></a></button>
                            </form>
                            <div id="comments-list" style="margin-left: 5%; text-align: justify;">
                            <!-- Comments -->
                            </div>
                            </div>`;

                $('#post-div1').append(object);
                getComments(allObjects[i].postId);
            }
            startPage++;
        }
    });
});

"use strict";
const adminCommentOptions = {
    method: 'GET',
    headers: {
        'Content-Type': 'application/json'
    }
};

function getComments(currentPostId) {
    const commentsList = $('#post-' + currentPostId + ' #comments-list');
    fetch("/api/comments/byPost/" + currentPostId + "/0/100/", adminCommentOptions).then((data) => data.json()).then((data) => {
        data.forEach(comment => {
            let photo = "/pictures/default_avatar.png";
            let publicPhoto = comment.user.photo.photoSrc;
            if (comment.user.photo.privacy.privacyId === 1) {
                publicPhoto = photo;
            }
            commentsList.append(`
                    <div id="replycomment-${comment.commentId}"  class="w3-container w3-card w3-white w3-round w3-margin">
                    <br>
                    <img src="${publicPhoto}" alt="Avatar" class="w3-left w3-circle w3-small w3-margin-right" style="width:20px; height=20px;">
                    <span class="w3-right w3-opacity">${comment.timeStamp}</span>
                    <h5 style="text-align: left;">${comment.user.firstName} ${comment.user.lastName}</h5>
                    <p style="text-align: justify; margin: 0%">${comment.commentBody}</p>
                    <a href="/replyComments/new/${comment.commentId}">
                    <button type="button" class="w3-button w3-theme  w3-right" style="margin-left: 5%"><i class="fa fa-pencil"></i> Reply</button></a>
                    <br><br>
                    </div>
                    `);
            getRepliesToComments(currentPostId, comment.commentId);
        });
    })
}

function getRepliesToComments(currentPostId, currentCommentId) {
    const repliesList = $('#post-' + currentPostId + ' #comments-list #replycomment-' + currentCommentId);
    fetch("/api/replyComments/byComment/" + currentCommentId + "/0/100/", adminCommentOptions).then((data) => data.json()).then((data) => {
        data.forEach(comment => {
            let photo = "/pictures/default_avatar.png";
            let publicPhoto = comment.user.photo.photoSrc;
            if (comment.user.photo.privacy.privacyId === 1) {
                publicPhoto = photo;
            }
            repliesList.append(`
                    <div class="w3-container w3-card w3-white w3-round w3-margin">
                    <br>
                    <img src="${publicPhoto}" alt="Avatar" class="w3-left w3-circle w3-small w3-margin-right" style="width:20px; height=20px;">
                    <span class="w3-right w3-opacity">${comment.timeStamp}</span>
                    <h5 style="text-align: left;">${comment.user.firstName} ${comment.user.lastName}</h5>
                    <p style="text-align: justify; margin: 0%">${comment.commentBody}</p>
                    </div>
                    `);
        });
    })
}


/* POST APPEND IN INDEX PAGE END*/