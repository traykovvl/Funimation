package team1.socnet.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import team1.socnet.models.Photo;

public interface PhotoRepository extends JpaRepository<Photo, Long> {

}
