package team1.socnet.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import team1.socnet.models.City;

public interface CityRepository extends JpaRepository<City, Long> {

    City findByCityName(String cityName);

}
