package team1.socnet.repositories;

import org.springframework.data.repository.CrudRepository;
import team1.socnet.models.FriendshipStatus;

public interface FriendshipStatusRepository extends CrudRepository<FriendshipStatus, Long> {
    FriendshipStatus findByStatus(String status);
}
