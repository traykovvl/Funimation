package team1.socnet.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import team1.socnet.models.Country;

public interface CountryRepository extends JpaRepository<Country, Long> {
}
