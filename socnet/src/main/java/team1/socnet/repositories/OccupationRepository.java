package team1.socnet.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import team1.socnet.models.Occupation;

public interface OccupationRepository extends JpaRepository<Occupation, Long> {

    Occupation findByOccupationArea(String occupationArea);

    Occupation getByOccupationArea(String occupationArea);
}
