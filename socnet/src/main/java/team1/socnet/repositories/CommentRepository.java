package team1.socnet.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import team1.socnet.models.Comment;
import team1.socnet.models.Post;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {
    Page<Comment> findAll(Pageable pageable);

    List<Comment> findAllByPostAndCommentToReplyIsNullOrderByTimeStampAsc(Post post);

    List<Comment> findAllByCommentToReplyIsNotNullOrderByTimeStampAsc();

    Page<Comment> findAllByCommentToReplyIsNotNullOrderByTimeStampAsc(Pageable pageable);

    List<Comment> findAllByCommentToReplyEqualsOrderByTimeStampAsc(Comment comment);

    Page<Comment> findAllByCommentToReplyEqualsOrderByTimeStampAsc(Comment comment, Pageable pageable);

    Page<Comment> findAllByPostAndCommentToReplyIsNullOrderByTimeStampAsc(Post post, Pageable pageable);
}
