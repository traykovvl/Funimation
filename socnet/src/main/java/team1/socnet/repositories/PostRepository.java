package team1.socnet.repositories;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import team1.socnet.models.Post;
import team1.socnet.models.User;

import java.util.List;

public interface PostRepository extends JpaRepository<Post, Long> {
    List<Post> findByAuthorOrderByTimeStampDesc(User author);

    List<Post> findAllByOrderByTimeStampDesc();

    List<Post> findByAuthorAndPrivacy_PrivacyTypeOrderByTimeStampDesc(User author, String privacy);

    List<Post> findAllByPrivacy_PrivacyTypeAndTitleIsNotContainingOrderByTimeStampDesc(String privacyStatus, String title);

    List<Post> findAllByTitleOrderByTimeStampDesc(String title);

    List<Post> findAllByTitleContainsOrPostMessageContainsAndPrivacy_PrivacyType(String title, String body, String privacy);

    List<Post> findAllByOrderByTimeStampDesc(Pageable pageable);

    List<Post> findByAuthorOrderByTimeStampDesc(User author, Pageable pageable);

    List<Post> findByAuthorAndPrivacy_PrivacyTypeOrderByTimeStampDesc(User author, String privacy, Pageable pageable);

    List<Post> findAllByTitleOrderByTimeStampDesc(String title, Pageable pageable);

    List<Post> findAllByPrivacy_PrivacyTypeAndTitleIsNotContainingOrderByTimeStampDesc(String privacyStatus, String title, Pageable pageable);

    @Query(value = "select post_id, user_id, post_message, time_stamp, privacy_id, title from posts p " +
            "join friendships f " +
            "on p.user_id = f.user_id_for_connection " +
            "where f.connection_status_id = 2 and f.user_id_asking_connection = ?1 " +
            "union " +
            "select post_id,user_id,post_message, time_stamp, privacy_id, title from posts p " +
            "join friendships f on p.user_id = f.user_id_asking_connection " +
            "where f.connection_status_id = 2 and f.user_id_for_connection = ?1 " +
            "union select post_id,user_id,post_message, time_stamp, privacy_id, title from posts p " +
            "where p.user_id= ?1 " +
            "order by time_stamp DESC ", nativeQuery = true)
    List<Post> generateFeedByDate(long userId, long statusId, Pageable pageable);
}
