package team1.socnet.repositories;

import org.springframework.data.repository.CrudRepository;
import team1.socnet.models.Friendship;
import team1.socnet.models.User;

import java.util.List;

public interface FriendShipRepository extends CrudRepository<Friendship,Long> {
    Friendship findByUserReceivingRequestAndUserSendingRequest(User one, User two);
    Friendship findByConnectionId(long connectionId);
    List<Friendship> findByUserReceivingRequestAndFriendshipStatus_Status(User one, String status);
    List<Friendship> findByFriendshipStatus_StatusAndUserReceivingRequestOrUserSendingRequestAndFriendshipStatus_Status(String connected, User one, User two, String friends);
    boolean existsByConnectionIdAndFriendshipStatus_Status(long id, String status);
}

