package team1.socnet.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import team1.socnet.models.PasswordResetToken;

public interface PasswordResetTokenRepository extends JpaRepository<PasswordResetToken, Long> {

    PasswordResetToken findByToken(String token);

}
