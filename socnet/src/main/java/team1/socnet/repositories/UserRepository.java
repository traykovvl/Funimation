package team1.socnet.repositories;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import team1.socnet.models.User;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByEmail(String email);

    User findByUserId(long id);

    List<User> findAllByValidIsTrue();

    List<User> findAllByValidIsTrue(Pageable pageable);

    List<User> findAllByEmailStartingWithAndValidIsTrueOrFirstNameStartingWithAndValidIsTrueOrLastNameStartingWithAndValidIsTrue(
            String email,
            String firstName,
            String lastName);

    @Modifying
    @Query("update User u set u.password = :password where u.userId = :id")
    void updatePassword(@Param("password") String password, @Param("id") Long id);

    @Modifying
    @Query("update User u set u.email = :email where u.userId = :id")
    void updateEmail(@Param("email") String email, @Param("id") Long id);
}
