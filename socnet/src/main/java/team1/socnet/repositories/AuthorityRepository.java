package team1.socnet.repositories;

import org.springframework.data.repository.CrudRepository;
import team1.socnet.models.Authority;

import java.util.List;

public interface AuthorityRepository extends CrudRepository<Authority, Long> {
    Authority getByAuthorityName(String authorityName);

    List<Authority> findAll();

}
