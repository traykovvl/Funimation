package team1.socnet.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import team1.socnet.models.Privacy;

import java.util.List;

public interface PrivacyRepository extends JpaRepository<Privacy, Long> {
    List<Privacy> findAllByOrderByPrivacyIdDesc();
}
