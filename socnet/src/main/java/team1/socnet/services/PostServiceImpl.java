package team1.socnet.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import team1.socnet.models.Post;
import team1.socnet.models.User;
import team1.socnet.repositories.PostRepository;
import team1.socnet.repositories.UserRepository;
import team1.socnet.services.contracts.FriendshipService;
import team1.socnet.services.contracts.PostService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static team1.socnet.services.constants.Constant.*;

@Service
public class PostServiceImpl implements PostService {
    private static final String POST_NOT_EXISTS_ERROR_MESSAGE = "Post doesn't exists";

    private PostRepository postRepository;
    private UserRepository userRepository;
    private FriendshipService friendShipService;
    private PrivacyServiceImpl privacyService;

    @Autowired
    public PostServiceImpl(PostRepository postRepository, UserRepository userRepository,
                           FriendshipService friendShipService, PrivacyServiceImpl privacyService) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
        this.friendShipService = friendShipService;
        this.privacyService = privacyService;
    }

    @Override
    public List<Post> findAll() {
        return postRepository.findAll();
    }

    @Override
    public List<Post> findPublicPost() {
        return postRepository.findAllByPrivacy_PrivacyTypeAndTitleIsNotContainingOrderByTimeStampDesc(PUBLIC_STATUS,
                REQUEST_OFFER_TITLE);
    }

    @Override
    public void savePost(Post post, User author) {
        post.setAuthor(author);
        setPostPrivacy(post);
        postRepository.save(post);
    }

    @Override
    public Post findById(long postId) {
        checkIfPostExists(postId);
        return postRepository.getOne(postId);
    }

    @Override
    public void updatePost(Post post) {
        Post postToBeUpdated = postRepository.getOne(post.getPostId());
        postToBeUpdated.setTitle(post.getTitle());
        postToBeUpdated.setPostMessage(post.getPostMessage());
        postToBeUpdated.setPrivacy(post.getPrivacy());
        postToBeUpdated.setTimeStamp(new Date());
        postRepository.save(post);
    }

    @Override
    public void deletePost(long postId) {
        checkIfPostExists(postId);
        postRepository.deleteById(postId);
    }

    private void checkIfPostExists(long postId) {
        if (!postRepository.findById(postId).isPresent()) {
            throw new IllegalArgumentException(POST_NOT_EXISTS_ERROR_MESSAGE);
        }
    }

    @Override
    public List<Post> findByAuthor(long authorId) {
        User author = getUser(authorId);
        return postRepository.findByAuthorOrderByTimeStampDesc(author);
    }

    private User getUser(long authorId) {
        return userRepository.findByUserId(authorId);
    }

    @Override
    public List<Post> findAuthorPublicPost(long authorId) {
        User author = getUser(authorId);
        return postRepository.findByAuthorAndPrivacy_PrivacyTypeOrderByTimeStampDesc(author, PUBLIC_STATUS);
    }

    @Override
    public List<Post> allFriendsPosts(User loggedUser) {
        List<Post> allPosts = postRepository.findAllByOrderByTimeStampDesc();
        List<Post> friendsPosts = new ArrayList<>();
        for (Post post : allPosts) {
            if (friendShipService.areFriends(post.getAuthor().getUserId(), loggedUser) ||
                    (post.getAuthor() == loggedUser)) {
                friendsPosts.add(post);
            }
        }
        return friendsPosts;
        }

    @Override
    public void likeDislikePost(Post postToLike, User loggedUser) {
        List<User> postLikes = postToLike.getUsersWhoLikedPost();
        boolean userExists = isUserExists(loggedUser, postLikes);

        if (!userExists) {
            postLikes.add(loggedUser);
        } else {
            postLikes.stream()
                    .filter(likedByUser -> likedByUser.getUserId() == loggedUser.getUserId())
                    .findFirst()
                    .ifPresent(postLikes::remove);
        }

        postToLike.setUsersWhoLikedPost(postLikes);
        postRepository.save(postToLike);
    }

    public void setPostPrivacy(Post post) {
        if (post.getPrivacy() == null) {
            post.setPrivacy(privacyService.findAll().get(0));
        }
    }

    private boolean isUserExists(User loggedUser, List<User> postLikes) {
        return postLikes
                .stream()
                .anyMatch(user -> user.getUserId() == loggedUser.getUserId());
    }

    @Override
    public List<Post> findAllRequestOfferForKids() {
        return postRepository.findAllByTitleOrderByTimeStampDesc(REQUEST_FOR_KID_S_ENTERTAINER);
    }

    @Override
    public List<Post> findAllRequestOffersForAdults() {
        return postRepository.findAllByTitleOrderByTimeStampDesc(REQUEST_STAND_UP_COMEDIAN);
    }

    @Override
    public List<Post> filterAllPublicPostByPostMessageContaining(String filterParam) {
        return postRepository.findAllByTitleContainsOrPostMessageContainsAndPrivacy_PrivacyType(filterParam, filterParam, "public");
    }
}
