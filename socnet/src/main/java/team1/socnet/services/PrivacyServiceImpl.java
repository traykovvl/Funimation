package team1.socnet.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import team1.socnet.models.Privacy;
import team1.socnet.repositories.PrivacyRepository;
import team1.socnet.services.contracts.PrivacyService;

import java.util.List;

@Service
public class PrivacyServiceImpl implements PrivacyService {
    private PrivacyRepository privacyRepository;

    @Autowired
    public PrivacyServiceImpl(PrivacyRepository privacyRepository) {
        this.privacyRepository = privacyRepository;
    }

    @Override
    public List<Privacy> findAll() {
        return privacyRepository.findAllByOrderByPrivacyIdDesc();
    }
}
