package team1.socnet.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import team1.socnet.models.City;
import team1.socnet.repositories.CityRepository;
import team1.socnet.services.contracts.CityService;

import java.util.List;

@Service
public class CityServiceImpl implements CityService {

    private static final String CITY_NOT_EXISTS_ERROR_MESSAGE = "City doesn't exists";
    private static final String CITY_ALREADY_EXISTS = "City already exists";
    private CityRepository cityRepository;

    @Autowired
    public CityServiceImpl(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    @Override
    public List<City> findAll() {
        return cityRepository.findAll();
    }

    @Override
    public City getById(long id) {
        City city = cityRepository.getOne(id);
        checkIfCityExists(city == null, CITY_NOT_EXISTS_ERROR_MESSAGE);
        return city;

    }

    private void checkIfCityExists(boolean b, String cityNotExistsErrorMessage) {
        if (b) {
            throw new IllegalArgumentException(cityNotExistsErrorMessage);
        }
    }

    @Override
    public City findByCityName(String cityName) {
        City city = cityRepository.findByCityName(cityName);
        checkIfCityExists(city == null, CITY_NOT_EXISTS_ERROR_MESSAGE);
        return city;
    }

    @Override
    public void insertCity(City city) {
        checkIfCityExists(cityRepository.findByCityName(city.getCityName()) != null, CITY_ALREADY_EXISTS);
        cityRepository.save(city);
    }

    @Override
    public void UpdateCity(City city) {
        cityRepository.save(city);
    }

    @Override
    public void deleteCity(City city) {
        checkIfCityExists(cityRepository.getOne(city.getCityId()) == null, CITY_NOT_EXISTS_ERROR_MESSAGE);
        cityRepository.delete(city);
    }

}
