package team1.socnet.services.contracts;

import team1.socnet.models.Friendship;
import team1.socnet.models.User;

import java.util.List;

public interface FriendshipService {
    void saveFriendshipRequest(long userReceivingId, User userSendingRequest);
    void acceptFriendshipRequest(long userSendingId, User userAcceptingRequest);
    void deleteFriendship(long friendShipId);
    List<Friendship> findAllFriends (User user);
    List<Friendship> findAllPendingFriends (User user);
    boolean areFriends(long friendId, User principal);
}