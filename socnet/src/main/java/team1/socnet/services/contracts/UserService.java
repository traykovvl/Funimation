package team1.socnet.services.contracts;

import org.springframework.security.core.userdetails.UserDetailsService;
import team1.socnet.models.User;
import team1.socnet.models.dto.UserRegistrationDto;
import team1.socnet.models.dto.UserUpdateDto;

import java.io.IOException;
import java.util.List;

public interface UserService extends UserDetailsService {

    User findByEmail(String email);

    User findByUserId(long id);

    User updateUserRegistrationStatus(User user);

    User save(UserRegistrationDto registration);

    void updateUser(UserUpdateDto userUpdateDto, long id) throws IOException;

    void delete(long id, long loggedUserId);

    void updatePassword(String password, Long userId);

    List<User> findAll();

    List<User> findAllByPages(int fromPage, int usersPerPage);

    List<User> findAllByEnabled();

    List<User> filterByUsernameAndEmailAndFirstNameAndLastName(String filterParam);

    String getUserAuthority(User user);

    void updateEmail(String email, User loggedUser);

}