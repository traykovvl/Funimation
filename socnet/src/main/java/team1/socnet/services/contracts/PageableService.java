package team1.socnet.services.contracts;

import team1.socnet.models.Comment;
import team1.socnet.models.Post;
import team1.socnet.models.User;

import java.util.List;

public interface PageableService {
    List<Post> findAllAuthorsPosts(long authorId, int fromPage, int postsPerPage);

    List<Post> findAllAuthorsPublicPosts(long authorId, int fromPage,
                                         int postsPerPage);

    List<Post> findAllRequestOfferForKids(int fromPage, int postsPerPage);

    List<Post> findAllRequestOfferForAdults(int fromPage, int postsPerPage);

    List<Post> findAllByPages(int fromPage, int postsPerPage);

    List<Post> findPublicPost(int fromPage, int postsPerPage);

    List<Comment> findAllCommentsByPage(int fromPage, int commentsPerPage);

    List<Comment> findAllCommentsByPostByPage(Post post, int fromPage, int commentsPerPage);

    List<Comment> findAllRepliesToCommentByPage(int fromPage, int replyCommentsPerPage);

    List<Post> allFriendsPosts(User loggedUser, int fromPage, int commentsPerPage);

    List<Comment> findAllRepliesToCommentByCommentByPage(Comment comment, int fromPage, int replyCommentsPerPage);
}
