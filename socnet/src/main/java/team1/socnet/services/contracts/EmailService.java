package team1.socnet.services.contracts;

import team1.socnet.models.Email;

import javax.mail.MessagingException;

public interface EmailService {
    void sendEmailForResetOfPassword(Email mail) throws MessagingException;
    void sendEmailForConfirmationOfRegistration(Email mail) throws MessagingException;
}
