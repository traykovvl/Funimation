package team1.socnet.services.contracts;

import team1.socnet.models.Occupation;

import java.util.List;

public interface OccupationService{
    List<Occupation> findAll();

    Occupation getById(Long id);

}
