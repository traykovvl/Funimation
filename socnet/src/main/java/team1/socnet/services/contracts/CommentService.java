package team1.socnet.services.contracts;

import team1.socnet.models.Comment;
import team1.socnet.models.Post;
import team1.socnet.models.User;

import java.util.List;

public interface CommentService {
    Comment findById(long commentId);

    Comment saveComment(Comment comment, User author, Post post);

    Comment updateComment(Comment comment);

    void deleteComment(long commentId);

    List<Comment> findAll();

    List<Comment> findAllByPost(Post post);

}
