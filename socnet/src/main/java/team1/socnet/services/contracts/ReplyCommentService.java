package team1.socnet.services.contracts;

import team1.socnet.models.Comment;
import team1.socnet.models.User;

import java.util.List;

public interface ReplyCommentService {
    void saveReply(Comment reply, User author, Comment comment);

    void updateReply(Comment comment);

    void deleteReply(long commentId);

    List<Comment> findAll();

    List<Comment> findAllRepliesToComment(Comment comment);
}
