package team1.socnet.services.contracts;

import team1.socnet.models.City;
import team1.socnet.models.Country;

import java.util.List;

public interface CityService {
    List<City> findAll();

    City getById(long id);

    City findByCityName(String cityName);

    void insertCity(City city);

    void UpdateCity(City city);

    void deleteCity(City city);
}
