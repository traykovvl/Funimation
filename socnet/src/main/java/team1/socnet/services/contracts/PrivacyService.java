package team1.socnet.services.contracts;


import team1.socnet.models.Privacy;

import java.util.List;

public interface PrivacyService {
    List<Privacy> findAll();
}
