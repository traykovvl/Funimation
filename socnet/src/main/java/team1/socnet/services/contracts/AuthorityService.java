package team1.socnet.services.contracts;

import team1.socnet.models.Authority;

import java.util.List;

public interface AuthorityService {
    List<Authority> findAll();

    Authority getUserAuthority(String authorityName);

    void updateAuthority(Authority authority);
}
