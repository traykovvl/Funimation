package team1.socnet.services.contracts;

import team1.socnet.models.Country;

import java.util.List;

public interface CountryService {

    List<Country> findAll();

}
