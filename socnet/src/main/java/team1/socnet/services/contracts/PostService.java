package team1.socnet.services.contracts;

import team1.socnet.models.Post;
import team1.socnet.models.User;

import java.util.List;

public interface PostService {
    Post findById(long postId);

    void savePost(Post post, User user);

    void updatePost(Post post);

    void deletePost(long postId);

    void likeDislikePost(Post postToLike, User loggedUser);

    List<Post> findAll();

    List<Post> findPublicPost();

    List<Post> findByAuthor(long authorId);

    List<Post> findAuthorPublicPost(long authorId);

    List<Post> allFriendsPosts(User loggedUser);

    List<Post> findAllRequestOfferForKids();

    List<Post> findAllRequestOffersForAdults();

    List<Post> filterAllPublicPostByPostMessageContaining(String filterParam);
}
