package team1.socnet.services.contracts;

import team1.socnet.models.PasswordResetToken;

public interface PasswordResetTokenService {
    PasswordResetToken findByToken(String token);
    void save (PasswordResetToken token);
    void delete (PasswordResetToken token);
}
