package team1.socnet.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import team1.socnet.models.Comment;
import team1.socnet.models.Post;
import team1.socnet.models.User;
import team1.socnet.repositories.CommentRepository;
import team1.socnet.services.contracts.CommentService;

import java.util.Date;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {
    private static final String COMMENT_NOT_EXISTS_ERROR_MESSAGE = "Comment does not exists";

    private CommentRepository commentRepository;

    @Autowired
    public CommentServiceImpl(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @Override
    public List<Comment> findAll() {
        return commentRepository.findAll();
    }



    @Override
    public Comment saveComment(Comment comment, User author, Post post) {
        comment.setUser(author);
        comment.setPost(post);
        return commentRepository.save(comment);
    }

    @Override
    public Comment findById(long commentId) {
        checkIfCommentExists(commentId);
        return commentRepository.getOne(commentId);
    }

    @Override
    public Comment updateComment(Comment comment) {
        checkIfCommentExists(comment.getCommentId());
        Comment commentToUpdate = commentRepository.getOne(comment.getCommentId());
        commentToUpdate.setCommentBody(comment.getCommentBody());
        commentToUpdate.setTimeStamp(new Date());
        return commentRepository.save(comment);
    }

    @Override
    public void deleteComment(long commentId) {
        checkIfCommentExists(commentId);
        commentRepository.deleteById(commentId);
    }

    @Override
    public List<Comment> findAllByPost(Post post) {
        return commentRepository.findAllByPostAndCommentToReplyIsNullOrderByTimeStampAsc(post);
    }

    private void checkIfCommentExists(long commentId) {
        if (!commentRepository.findById(commentId).isPresent()) {
            throw new IllegalArgumentException(COMMENT_NOT_EXISTS_ERROR_MESSAGE);
        }
    }
}

