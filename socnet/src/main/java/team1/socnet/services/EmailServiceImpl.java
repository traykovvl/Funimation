package team1.socnet.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;
import team1.socnet.models.Email;
import team1.socnet.services.contracts.EmailService;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;

@Service
public class EmailServiceImpl implements EmailService {
    private JavaMailSender emailSender;
    private ITemplateEngine templateEngine;

    @Autowired
    public EmailServiceImpl(JavaMailSender emailSender, ITemplateEngine templateEngine) {
        this.emailSender = emailSender;
        this.templateEngine = templateEngine;
    }

    public void sendEmailForResetOfPassword(Email mail) throws MessagingException {
        sendEmail(mail, "email/email-template");
    }

    @Override
    public void sendEmailForConfirmationOfRegistration(Email mail) throws MessagingException {
        sendEmail(mail, "email/email-template-confirm-password");
    }

    private void sendEmail(Email mail, String s) throws MessagingException {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = getMimeMessageHelper(message);

        Context context = getContext(mail);
        String html = templateEngine.process(s, context);

        preparingEmailMessage(mail, helper, html);
        emailSender.send(message);
    }

    private void preparingEmailMessage(Email mail, MimeMessageHelper helper, String html) throws MessagingException {
        helper.setTo(mail.getTo());
        helper.setText(html, true);
        helper.setSubject(mail.getSubject());
        helper.setFrom(mail.getFrom());
    }

    private MimeMessageHelper getMimeMessageHelper(MimeMessage message) throws MessagingException {
        return new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());
    }

    private Context getContext(Email mail) {
        Context context = new Context();
        context.setVariables(mail.getModel());
        return context;
    }
}
