package team1.socnet.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import team1.socnet.models.*;
import team1.socnet.models.dto.UserRegistrationDto;
import team1.socnet.models.dto.UserUpdateDto;
import team1.socnet.repositories.*;
import team1.socnet.services.contracts.OccupationService;
import team1.socnet.services.contracts.UserService;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static team1.socnet.services.constants.Constant.*;

@Service
public class UserServiceImpl implements UserService {


    private PasswordEncoder passwordEncoder;
    private UserRepository userRepository;
    private AuthorityRepository authorityRepository;
    private CityRepository cityRepository;
    private PhotoRepository photoRepository;
    private PrivacyRepository privacyRepository;
    private OccupationService occupationService;

    @Autowired
    public UserServiceImpl(PasswordEncoder passwordEncoder,
                           UserRepository userRepository,
                           AuthorityRepository authorityRepository,
                           CityRepository cityRepository,
                           PhotoRepository photoRepository,
                           PrivacyRepository privacyRepository,
                           OccupationService occupationService) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.authorityRepository = authorityRepository;
        this.cityRepository = cityRepository;
        this.photoRepository = photoRepository;
        this.privacyRepository = privacyRepository;
        this.occupationService = occupationService;
    }

    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public List<User> findAllByPages(int fromPage, int usersPerPage) {
        Pageable pageable = PageRequest.of(fromPage, usersPerPage, Sort.by(Sort.Direction.ASC, "email"));
        return userRepository.findAllByValidIsTrue(pageable);
    }

    @Override
    public List<User> findAllByEnabled() {
        return userRepository.findAllByValidIsTrue();
    }

    @Override
    public List<User> filterByUsernameAndEmailAndFirstNameAndLastName(String filterParam) {
        return userRepository.findAllByEmailStartingWithAndValidIsTrueOrFirstNameStartingWithAndValidIsTrueOrLastNameStartingWithAndValidIsTrue
                (filterParam, filterParam, filterParam);
    }

    @Override
    public User findByUserId(long id) {
        return userRepository.findByUserId(id);
    }

    @Override
    public User updateUserRegistrationStatus(User user) {
        user.setValid(true);
        return userRepository.save(user);
    }

    @Override
    public User save(UserRegistrationDto registration) {
        User user = new User();
        user.setFirstName(registration.getFirstName());
        user.setLastName(registration.getLastName());
        user.setEmail(registration.getEmail());
        user.setPassword(passwordEncoder.encode(registration.getPassword()));
        Authority auth = authorityRepository.getByAuthorityName("ROLE_USER");
        Set<Authority> set = new HashSet<>();
        set.add(auth);
        user.setAuthorities(set);
        user.setValid(false);
        user.setPhoto(photoRepository.getOne(1L));
        updatePhotoPrivacy(user, privacyRepository.getOne(2L));
        user.setCity(cityRepository.getOne(1L));
        user.setOccupation(occupationService.getById(3L));
        return userRepository.save(user);
    }

    @Override
    public void delete(long id, long loggedUserId) {
        User user = userRepository.findByUserId(id);
        if(id != loggedUserId){
            user.setBanned(true);
        }
        user.setValid(false);
        userRepository.save(user);
    }

    @Override
    public void updatePassword(String password, Long userId) {
        userRepository.updatePassword(password, userId);
    }
    @Override
    public void updateEmail(String email, User loggedUser) {
       loggedUser.setValid(false);
       loggedUser.setEmail(email);
        userRepository.save(loggedUser);
    }

    @Override
    public UserDetails loadUserByUsername(String email) {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException(INVALID_USERNAME_OR_PASSWORD);
        }
        if (!user.isValid()) {
            throw new UsernameNotFoundException(INVALID_USERNAME_OR_PASSWORD);
        }
        return new org.springframework.security.core.userdetails.User(user.getEmail(),
                user.getPassword(),
                mapRolesToAuthorities(user.getAuthorities()));
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Authority> roles) {
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getAuthorityName()))
                .collect(Collectors.toList());
    }

    @Override
    public void updateUser(UserUpdateDto userUpdateDto, long id) throws IOException {
        User user = getUserToUpdate(id);
        updateUserFields(userUpdateDto, user);
        userRepository.save(user);
    }

    public User getUserToUpdate(long id) {
        Optional<User> userOpt = userRepository.findById(id);
        if (!userOpt.isPresent()) {
            throw new IllegalArgumentException(USER_NOT_EXISTS_ERROR_MESSAGE);
        }
        return userOpt.get();
    }

    public void updateUserFields(UserUpdateDto userUpdateDto, User user) throws IOException {
        updateAuthority(userUpdateDto, user);
        updateFirstName(userUpdateDto, user);
        updateLastName(userUpdateDto, user);
        updateOccupation(userUpdateDto, user);
        updateCity(userUpdateDto, user);
        updatePhoto(userUpdateDto, user);
        updateCoverImage(userUpdateDto, user);
        updatePhotoPrivacy(user, userUpdateDto.getPrivacy());
    }

    public void updatePhotoPrivacy(User user, Privacy privacy) {
        user.getPhoto().setPrivacy(privacy);
    }

    public void updateCity(UserUpdateDto userUpdateDto, User user) {
        City city = cityRepository.findByCityName(userUpdateDto.getCity());
        if (city != null) {
            user.setCity(city);
        }
    }

    public void updateOccupation(UserUpdateDto userUpdateDto, User user) {
        if (!userUpdateDto.getOccupation().getOccupationArea().equals(NONE)) {
            user.setOccupation(userUpdateDto.getOccupation());
        }
    }

    public void updateFirstName(UserUpdateDto userUpdateDto, User user) {
        if (!userUpdateDto.getFirstName().isEmpty() && userUpdateDto.getFirstName() != null) {
            user.setFirstName(userUpdateDto.getFirstName());
        }
    }

    public void updateLastName(UserUpdateDto userUpdateDto, User user) {
        if (!userUpdateDto.getLastName().isEmpty() && userUpdateDto.getLastName() != null) {
            user.setLastName(userUpdateDto.getLastName());
        }
    }

    public void updateAuthority(UserUpdateDto userUpdateDto, User user) {
        Authority auth = authorityRepository.getByAuthorityName(
                userUpdateDto.getAuthority() != null ? userUpdateDto.getAuthority() : "ROLE_USER");
        user.getAuthorities().clear();
        user.getAuthorities().add(auth);
    }

    public void updatePhoto(UserUpdateDto userUpdateDto, User user) throws IOException {
        MultipartFile photo = userUpdateDto.getPhoto();
        if (photo != null && !photo.isEmpty()) {
            setPhotoIfNotDefault(user);
            user.getPhoto().setPhoto(photo.getBytes());
        }
    }

    public void updateCoverImage(UserUpdateDto userUpdateDto, User user) throws IOException {
        final MultipartFile coverImage = userUpdateDto.getCoverImage();
        if (coverImage != null && !coverImage.isEmpty()) {
            setPhotoIfNotDefault(user);
            user.getPhoto().setCoverImage(coverImage.getBytes());
        }
    }

    private void setPhotoIfNotDefault(User user) {
        if (user.getPhoto().getPhotoId() == 1L) {
            user.setPhoto(new Photo());
        }
    }
    public String getUserAuthority(User user) {
        String authorityType ="";
        for (Authority authority : user.getAuthorities()) {
            authorityType = authority.getAuthorityName();
        }
        return authorityType;
    }
}