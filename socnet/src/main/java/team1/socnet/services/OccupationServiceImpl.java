package team1.socnet.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import team1.socnet.models.Occupation;
import team1.socnet.repositories.OccupationRepository;
import team1.socnet.services.contracts.OccupationService;

import java.util.List;
import java.util.Optional;

@Service
public class OccupationServiceImpl implements OccupationService {

    public static final String OCCUPATION_NOT_EXISTS_ERROR_MESSAGE = "Occupation does not exists.";
    private OccupationRepository occupationRepository;

    @Autowired
    public OccupationServiceImpl(OccupationRepository occupationRepository) {
        this.occupationRepository = occupationRepository;
    }

    @Override
    public List<Occupation> findAll() {
        return occupationRepository.findAll();
    }

    @Override
    public Occupation getById(Long id) {
        Optional<Occupation> occupationOpt = occupationRepository.findById(id);
        if (!occupationOpt.isPresent()) {
            throw new IllegalArgumentException(OCCUPATION_NOT_EXISTS_ERROR_MESSAGE);
        }
        return occupationOpt.get();
    }
}
