package team1.socnet.services;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import team1.socnet.models.Comment;
import team1.socnet.models.Post;
import team1.socnet.models.User;
import team1.socnet.repositories.CommentRepository;
import team1.socnet.repositories.PostRepository;
import team1.socnet.services.contracts.PageableService;
import team1.socnet.services.contracts.UserService;

import java.util.List;

import static team1.socnet.services.constants.Constant.*;

@Service
public class PageableServiceImpl implements PageableService {
    private PostRepository postRepository;
    private UserService userService;
    private CommentRepository commentRepository;

    public PageableServiceImpl(PostRepository postRepository, UserService userService,
                               CommentRepository commentRepository) {
        this.postRepository = postRepository;
        this.userService = userService;
        this.commentRepository = commentRepository;
    }

    @Override
    public List<Post> findAllByPages(int fromPage, int postsPerPage) {
        Pageable pageable = PageRequest.of(fromPage, postsPerPage);
        return postRepository.findAllByOrderByTimeStampDesc(pageable);
    }

    @Override
    public List<Post> findAllAuthorsPosts(long authorId, int fromPage, int postsPerPage) {
        Pageable pageable = PageRequest.of(fromPage, postsPerPage);
        User author = userService.findByUserId(authorId);
        return postRepository.findByAuthorOrderByTimeStampDesc(author, pageable);
    }

    @Override
    public List<Post> findAllAuthorsPublicPosts(long authorId, int fromPage, int postsPerPage) {
        Pageable pageable = PageRequest.of(fromPage, postsPerPage);
        User author = userService.findByUserId(authorId);
        return postRepository.findByAuthorAndPrivacy_PrivacyTypeOrderByTimeStampDesc(author, PUBLIC_STATUS, pageable);
    }

    @Override
    public List<Post> findAllRequestOfferForKids(int fromPage, int postsPerPage) {
        Pageable pageable = PageRequest.of(fromPage, postsPerPage);
        return postRepository.findAllByTitleOrderByTimeStampDesc(REQUEST_FOR_KID_S_ENTERTAINER, pageable);
    }

    @Override
    public List<Post> findAllRequestOfferForAdults(int fromPage, int postsPerPage) {
        Pageable pageable = PageRequest.of(fromPage, postsPerPage);
        return postRepository.findAllByTitleOrderByTimeStampDesc(REQUEST_STAND_UP_COMEDIAN, pageable);
    }

    @Override
    public List<Post> findPublicPost(int fromPage, int postsPerPage) {
        Pageable pageable = PageRequest.of(fromPage, postsPerPage);
        return postRepository.findAllByPrivacy_PrivacyTypeAndTitleIsNotContainingOrderByTimeStampDesc(PUBLIC_STATUS,
                REQUEST_OFFER_TITLE,
                pageable);
    }

    @Override
    public List<Comment> findAllCommentsByPage(int fromPage, int commentsPerPage) {
        Pageable pageable = PageRequest.of(fromPage, commentsPerPage);
        return commentRepository.findAll(pageable).getContent();
    }

    @Override
    public List<Comment> findAllCommentsByPostByPage(Post post, int fromPage, int commentsPerPage) {
        Pageable pageable = PageRequest.of(fromPage, commentsPerPage);
        return commentRepository.findAllByPostAndCommentToReplyIsNullOrderByTimeStampAsc(post, pageable).getContent();
    }

    @Override
    public List<Comment> findAllRepliesToCommentByPage(int fromPage, int commentsPerPage) {
        Pageable pageable = PageRequest.of(fromPage, commentsPerPage);
        return commentRepository.findAllByCommentToReplyIsNotNullOrderByTimeStampAsc(pageable).getContent();
    }

    @Override
    public List<Comment> findAllRepliesToCommentByCommentByPage(Comment comment, int fromPage, int commentsPerPage) {
        Pageable pageable = PageRequest.of(fromPage, commentsPerPage);
        return commentRepository.findAllByCommentToReplyEqualsOrderByTimeStampAsc(comment, pageable).getContent();
    }

    @Override
    public List<Post> allFriendsPosts(User loggedUser, int fromPage, int postPerPage) {
        Pageable pageable = PageRequest.of(fromPage, postPerPage);
    return postRepository.generateFeedByDate(loggedUser.getUserId(),2,pageable);
    }
}
