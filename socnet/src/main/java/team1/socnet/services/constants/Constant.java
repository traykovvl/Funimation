package team1.socnet.services.constants;

public class Constant {
    public static final String USER_NOT_EXISTS_ERROR_MESSAGE = "User does not exists.";
    public static final String INVALID_USERNAME_OR_PASSWORD = "Invalid username or password.";
    public static final String NONE = "-";
    public static final String PUBLIC_STATUS = "public";
    public static final String REQUEST_OFFER_TITLE = "Request offer";
    public static final String REQUEST_STAND_UP_COMEDIAN = "Request offer for stand up comedian";
    public static final String REQUEST_FOR_KID_S_ENTERTAINER = "Request offer for kid's entertainer";
}
