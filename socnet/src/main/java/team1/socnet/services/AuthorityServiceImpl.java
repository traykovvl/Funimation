package team1.socnet.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import team1.socnet.models.Authority;
import team1.socnet.repositories.AuthorityRepository;
import team1.socnet.services.contracts.AuthorityService;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class AuthorityServiceImpl implements AuthorityService {

    private static final String AUTHORITY_NOT_EXIST_ERROR_MESSAGE = "Authority %s does not exist";
    private AuthorityRepository authorityRepository;

    @Autowired
    public AuthorityServiceImpl(AuthorityRepository authorityRepository) {
        this.authorityRepository = authorityRepository;
    }

    @Override
    public List<Authority> findAll() {
        return authorityRepository.findAll();
    }

    @Override
    public Authority getUserAuthority(String authorityName) {
        Authority authority = authorityRepository.getByAuthorityName(authorityName);
        checkIfEntityExists(authorityName, authority);
        return authority;
    }

    private void checkIfEntityExists(String authorityName, Authority authority) {
        if (authority == null) {
            throw new EntityNotFoundException(
                    String.format(AUTHORITY_NOT_EXIST_ERROR_MESSAGE, authorityName));
        }
    }

    public void updateAuthority(Authority authority) {
        authorityRepository.save(authority);
    }
}