package team1.socnet.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import team1.socnet.models.Friendship;
import team1.socnet.models.FriendshipStatus;
import team1.socnet.models.User;
import team1.socnet.repositories.FriendShipRepository;
import team1.socnet.repositories.FriendshipStatusRepository;
import team1.socnet.repositories.UserRepository;
import team1.socnet.services.contracts.FriendshipService;

import java.util.List;
import java.util.Optional;

import static team1.socnet.services.constants.Constant.USER_NOT_EXISTS_ERROR_MESSAGE;

@Service
public class FriendshipServiceImpl implements FriendshipService {
    private static final String CONNECTED = "connected";
    private static final String PENDING = "pending";
    private static final String SEND_REQUEST_ERROR_MESSAGE = "You have already sent request";
    private static final String CONFIRM_FRIENDSHIP_ERROR_MESSAGE = "You are already friends";
    private static final String FRIENDSHIP_NOT_EXISTS_ERROR_MESSAGE = "No Friendship found";
    private static final String STATUS_NOT_EXISTS_ERROR_MESSAGE = "Status does not exists.";

    private FriendShipRepository friendShipRepository;
    private UserRepository userRepository;
    private FriendshipStatusRepository friendshipStatusRepository;

    @Autowired
    public FriendshipServiceImpl(FriendShipRepository friendShipRepository,
                                 UserRepository userRepository,
                                 FriendshipStatusRepository friendshipStatusRepository) {
        this.friendShipRepository = friendShipRepository;
        this.userRepository = userRepository;
        this.friendshipStatusRepository = friendshipStatusRepository;
    }

    @Override
    public void saveFriendshipRequest(long userReceivingId, User userSending) {
        User userReceivingRequest = getUserById(userReceivingId);
        Friendship friendShip = getFriendShip(userReceivingRequest, userSending);
        if (friendShip != null) {
            checkFriendshipWithStatusPendingExists(getFriendShip(userReceivingRequest, userSending));
            checkFriendshipWithStatusConnectedExists(getFriendShip(userReceivingRequest, userSending));
        } else {
            FriendshipStatus statusPending = getFriendshipStatus(PENDING);
            Friendship newFriendship = new Friendship(userReceivingRequest, userSending, statusPending);
            friendShipRepository.save(newFriendship);
        }
    }

    public FriendshipStatus getFriendshipStatus(String status) {
        FriendshipStatus statusEx = friendshipStatusRepository.findByStatus(status);
        if (statusEx == null) {
            throw new IllegalArgumentException(STATUS_NOT_EXISTS_ERROR_MESSAGE);
        }
        return statusEx;
    }

    public User getUserById(long userReceivingId) {
        Optional<User> userOpt = userRepository.findById(userReceivingId);
        if (!userOpt.isPresent()) {
            throw new IllegalArgumentException(USER_NOT_EXISTS_ERROR_MESSAGE);
        }
        return userOpt.get();
    }

    @Override
    public void acceptFriendshipRequest(long userRequestingFriendshipId, User userAcceptingFriendship) {
        User userRequesting = getUserById(userRequestingFriendshipId);
        Friendship friendShip = getFriendShip(userRequesting, userAcceptingFriendship);
        if (friendShip == null) {
            throw new IllegalArgumentException(FRIENDSHIP_NOT_EXISTS_ERROR_MESSAGE);
        } else {
            checkFriendshipWithStatusConnectedExists(friendShip);
            FriendshipStatus status = getFriendshipStatus(CONNECTED);
            updateFriendshipStatus(friendShip, status);
        }
    }

    public Friendship getFriendShip(User userRequestingFriendship, User userAcceptingFriendship) {
        Friendship userSendingRequestAndUserReceivingRequest = friendShipRepository.findByUserReceivingRequestAndUserSendingRequest(userRequestingFriendship, userAcceptingFriendship);
        Friendship userReceivingRequestAndUserSendingRequest = friendShipRepository.findByUserReceivingRequestAndUserSendingRequest(userAcceptingFriendship, userRequestingFriendship);
        if (userSendingRequestAndUserReceivingRequest != null) {
            return userSendingRequestAndUserReceivingRequest;
        } else {
            return userReceivingRequestAndUserSendingRequest;
        }
    }

    public void checkFriendshipWithStatusPendingExists(Friendship friendShip) {
        if (friendShip.getFriendshipStatus().getStatus().equals(PENDING)) {
            throw new IllegalArgumentException(SEND_REQUEST_ERROR_MESSAGE);
        }
    }

    public void checkFriendshipWithStatusConnectedExists(Friendship friendShip) {
        if (friendShip.getFriendshipStatus().getStatus().equals(CONNECTED)) {
            throw new IllegalArgumentException(CONFIRM_FRIENDSHIP_ERROR_MESSAGE);
        }
    }

    public void updateFriendshipStatus(Friendship friendShip, FriendshipStatus status) {
        friendShip.setFriendshipStatus(status);
        friendShipRepository.save(friendShip);
    }

    @Override
    public void deleteFriendship(long friendShipId) {
        if (friendShipRepository.findByConnectionId(friendShipId) == null) {
            throw new IllegalArgumentException(FRIENDSHIP_NOT_EXISTS_ERROR_MESSAGE);
        } else {
            friendShipRepository.deleteById(friendShipId);
        }
    }

    @Override
    public List<Friendship> findAllFriends(User user) {
        return friendShipRepository.findByFriendshipStatus_StatusAndUserReceivingRequestOrUserSendingRequestAndFriendshipStatus_Status(CONNECTED, user, user, CONNECTED);
    }

    @Override
    public List<Friendship> findAllPendingFriends(User user) {
        return friendShipRepository.findByUserReceivingRequestAndFriendshipStatus_Status(user, PENDING);
    }

    @Override
    public boolean areFriends(long friendId, User principal) {
        User friend = getUserById(friendId);
        Friendship friendShip = getFriendShip(friend, principal);
        return (friendShip != null && friendShip.getFriendshipStatus().getStatus().equals(CONNECTED));
    }
}
