package team1.socnet.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import team1.socnet.models.Comment;
import team1.socnet.models.User;
import team1.socnet.repositories.CommentRepository;
import team1.socnet.services.contracts.ReplyCommentService;

import java.util.Date;
import java.util.List;

@Service
public class ReplyCommentServiceImpl implements ReplyCommentService {
    private static final String REPLY_NOT_EXISTS_ERROR_MESSAGE = "Reply does not exists";

    private CommentRepository commentRepository;
    @Autowired
     public ReplyCommentServiceImpl(CommentRepository commentRepository){
        this.commentRepository = commentRepository;
    }

    @Override
    public List<Comment> findAll() {
        return commentRepository.findAllByCommentToReplyIsNotNullOrderByTimeStampAsc();
    }


    @Override
    public void saveReply(Comment reply, User author, Comment comment) {
        reply.setUser(author);
        reply.setPost(comment.getPost());
        reply.setCommentToReply(comment);
        commentRepository.save(reply);
    }

    @Override
    public void updateReply(Comment reply) {
        checkIfReplyExists(reply.getCommentId());
        Comment dbReply = commentRepository.getOne(reply.getCommentId());
        dbReply.setCommentBody(reply.getCommentBody());
        dbReply.setTimeStamp(new Date());
        commentRepository.save(dbReply);
    }

    @Override
    public void deleteReply(long replyId) {
        checkIfReplyExists(replyId);
        commentRepository.deleteById(replyId);
    }

    @Override
    public List<Comment> findAllRepliesToComment(Comment comment) {
        return commentRepository.findAllByCommentToReplyEqualsOrderByTimeStampAsc(comment);
    }
    private void checkIfReplyExists(Long replyId) {
        if(!commentRepository.findById(replyId).isPresent()){
            throw new IllegalArgumentException(REPLY_NOT_EXISTS_ERROR_MESSAGE);
        }
    }
}
