package team1.socnet.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Base64;

@Entity
@Table(name = "photos")
public class Photo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "photo_id")
    private long photoId;

    @Column(name = "photo")
    private byte[] photo;

    @Column(name = "cover_image")
    private byte[] coverImage;

    @ManyToOne
    @JoinColumn(name = "privacy_id")
    private Privacy privacy;

    @OneToOne(mappedBy = "photo")
    @JsonIgnore
    private User user;

    public long getPhotoId() {
        return photoId;
    }

    public void setPhotoId(long photoId) {
        this.photoId = photoId;
    }


    public byte[] getPhoto() {
        return photo;
    }

    public String getPhotoSrc() {
        String result = "/pictures/no_image.jpg";
        if (photo != null) {
            result = "data:image/png;base64," + Base64.getEncoder().encodeToString(photo);
        }
        return result;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public byte[] getCoverImage() {
        return coverImage;
    }

    public String getCoverPhotoSrc() {
        String result = "/pictures/no_image.jpg";
        if (coverImage != null) {
            result = "data:image/png;base64," + Base64.getEncoder().encodeToString(coverImage);
        }
        return result;    }

    public void setCoverImage(byte[] coverImage) {
        this.coverImage = coverImage;
    }

    public Privacy getPrivacy() {
        return privacy;
    }

    public boolean matchPhotoPrivacy(Privacy photoPrivacy){
        return photoPrivacy.equals(privacy);
    }


    public void setPrivacy(Privacy privacy) {
        this.privacy = privacy;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
