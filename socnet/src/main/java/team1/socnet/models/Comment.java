package team1.socnet.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import team1.socnet.controllers.helpers.MethodHelper;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "comments")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comment_id")
    private long commentId;

    @Size(max = 255)
    @Column(name = "comment_body")
    @NotNull(message = "Field cannot be empty.")
    private String commentBody;

    @Column(name = "time_stamp")
    private Date timeStamp = new Date();

    @ManyToOne
//            (fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinColumn(name = "reply_id")
    private Comment commentToReply;

    @ManyToOne(fetch = FetchType.EAGER,
            cascade = {CascadeType.MERGE,
                    CascadeType.DETACH})
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "post_id")
    @JsonIgnore
    private Post post;


    public long getCommentId() {
        return commentId;
    }

    public void setCommentId(long commentId) {
        this.commentId = commentId;
    }

    public String getCommentBody() {
        return commentBody;
    }

    public void setCommentBody(String commentBody) {
        String htmlEscapedCommentBody = MethodHelper.replaceHtmlInputEscapeCode(commentBody);
        this.commentBody = htmlEscapedCommentBody;
    }

    public String getTimeStamp() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMMM-yyyy HH:mm");
        return formatter.format(timeStamp);
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Comment getCommentToReply() {
        return commentToReply;
    }
    public void setCommentToReply(Comment comment) {
        this.commentToReply = comment;
    }
}