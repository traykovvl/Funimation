package team1.socnet.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "friendships")
public class Friendship {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "connection_id")
    private long connectionId;

    @ManyToOne(fetch = FetchType.LAZY,
            cascade = {CascadeType.MERGE,
                    CascadeType.DETACH})
    @JoinColumn(name = "user_id_for_connection")
    @JsonIgnore
    private User userReceivingRequest;

    @ManyToOne
    @JoinColumn(name = "connection_status_id")
    @JsonIgnore
    private FriendshipStatus friendshipStatus;

    @ManyToOne(fetch = FetchType.LAZY,
            cascade = {CascadeType.MERGE,
                    CascadeType.DETACH})
    @JoinColumn(name = "user_id_asking_connection")
    @JsonIgnore
    private User userSendingRequest;

    public Friendship() {
    }

    public Friendship(User userReceivingRequest, User userSendingRequest, FriendshipStatus connection) {
        this.userReceivingRequest = userReceivingRequest;
        this.friendshipStatus = connection;
        this.userSendingRequest = userSendingRequest;
    }

    public long getConnectionId() {
        return connectionId;
    }

    public User getUserReceivingRequest() {
        return userReceivingRequest;
    }

    public void setUserReceivingRequest(User userReceivingRequest) {
        this.userReceivingRequest = userReceivingRequest;
    }

    public FriendshipStatus getFriendshipStatus() {
        return friendshipStatus;
    }

    public void setFriendshipStatus(FriendshipStatus friendshipStatus) {
        this.friendshipStatus = friendshipStatus;
    }

    public User getUserSendingRequest() {
        return userSendingRequest;
    }

    public void setUserSendingRequest(User userSendingRequest) {
        this.userSendingRequest = userSendingRequest;
    }
}

