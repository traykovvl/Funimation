package team1.socnet.models.dto;
import team1.socnet.models.Occupation;

import javax.validation.constraints.NotNull;

public class OccupationDto {

    @NotNull
    private String occupationArea;

    public OccupationDto(Occupation occupation) {
        this.occupationArea = occupation.getOccupationArea();
    }

    public String getOccupationArea() {
        return occupationArea;
    }

    public void setOccupationArea(String occupationArea) {
        this.occupationArea = occupationArea;
    }
}
