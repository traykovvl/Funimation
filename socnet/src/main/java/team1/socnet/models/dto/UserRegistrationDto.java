package team1.socnet.models.dto;

import team1.socnet.constrains.FieldMatch;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@FieldMatch.List({
        @FieldMatch(first = "password", second = "confirmPassword", message = "The password fields must match"),
        @FieldMatch(first = "email" , second = "confirmEmail", message = "The email fields must match")
})
public class UserRegistrationDto {
    @NotEmpty(message = "First name should not be empty")
    private String firstName;

    @NotEmpty(message = "Last name should not be empty")
    private String lastName;

    @NotEmpty
    @Size(min = 8, max = 16, message = "Password should be between 8 and 16 symbols")
    private String password;

    @NotEmpty
    private String confirmPassword;

    @NotEmpty
    @Email(message = "Email should be valid")
    private String email;

    @Email
    @NotEmpty
    private String confirmEmail;

    @AssertTrue
    private Boolean terms;

//    private Long city;
//
//    private String authority;
//
//    private String occupation;

    public UserRegistrationDto() {
    }

//    public String getAuthority() {
//        return authority;
//    }
//
//    public void setAuthority(String authority) {
//        this.authority = authority;
//    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName.trim();
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password.trim();
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email.trim();
    }

    public String getConfirmEmail() {
        return confirmEmail;
    }

    public void setConfirmEmail(String confirmEmail) {
        this.confirmEmail = confirmEmail;
    }

    public Boolean getTerms() {
        return terms;
    }

//    public void setOccupation(String occupation) {
//        this.occupation = occupation;
//    }
//
//    public String getOccupation() {
//        return occupation;
//    }
//
//    public void setCity(Long city) {
//        this.city = city;
//    }
//
//    public Long getCity() {
//        return city;
//    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword.trim();
    }

    public void setTerms(Boolean terms) {
        this.terms = terms;
    }
}
