package team1.socnet.models.dto;

import org.springframework.web.multipart.MultipartFile;
import team1.socnet.models.Occupation;
import team1.socnet.models.Privacy;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class UserUpdateDto {
    @NotEmpty
    private String firstName;

    @NotEmpty
    private String lastName;

    private String city;

    private String authority;

    private Occupation occupation;
@Size()
    private MultipartFile photo;

    private MultipartFile coverImage;

    private Privacy privacy;

    public UserUpdateDto() {
    }

//    public UserUpdateDto(User user) {
//        Iterator<Authority> iterator = user.getAuthorities().iterator();
//        if (iterator.hasNext()) {
//            this.authority = iterator.next().getAuthorityName();
//        }
//        this.firstName = user.getFirstName();
//        this.lastName = user.getLastName();
//        this.occupation = user.getOccupation();
//        City city = user.getCity();
//        if (city != null) {
//            this.city = city.getCityId();
//        }
//        this.privacy = user.getPhoto().getPrivacy();
//    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName.trim();
    }

    public void setOccupation(Occupation occupation) {
        this.occupation = occupation;
    }

    public Occupation getOccupation() {
        return occupation;
    }

    public MultipartFile getPhoto() {
        return photo;
    }

    public void setPhoto(MultipartFile photo) {
        this.photo = photo;
    }

    public MultipartFile getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(MultipartFile coverImage) {
        this.coverImage = coverImage;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public Privacy getPrivacy() {
        return privacy;
    }

    public void setPrivacy(Privacy privacy) {
        this.privacy = privacy;
    }
}
