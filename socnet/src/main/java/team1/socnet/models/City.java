package team1.socnet.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import team1.socnet.controllers.helpers.MethodHelper;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "cities")
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "city_id")
    private long cityId;

    @Column(name = "city_name")
    private String cityName;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;

    @OneToMany(mappedBy = "city")
    @JsonIgnore
    private List<User> users = new ArrayList<>();

    public long getCityId() {
        return cityId;
    }

    public void setCityId(long cityId) {
        this.cityId = cityId;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        String htmlEscapedCityName = MethodHelper.replaceHtmlInputEscapeCode(cityName);
        this.cityName = htmlEscapedCityName;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
