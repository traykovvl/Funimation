package team1.socnet.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import team1.socnet.controllers.helpers.MethodHelper;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "posts")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "post_id")
    private long postId;


    @ManyToOne(fetch = FetchType.EAGER,
            cascade = {CascadeType.MERGE,
                    CascadeType.DETACH})
    @JoinColumn(name = "user_id")
    private User author;

    @NotNull(message = "Field cannot be empty.")
    @Size(max = 255)
    @Column(name = "title")
    private String title;

    @NotNull(message = "Field cannot be empty.")
    @Column(name = "post_message")
    private String postMessage;

    @Column(name = "time_stamp")
    private Date timeStamp = new Date();

    @ManyToOne
    @JoinColumn(name = "privacy_id")
    @JsonIgnore
    private Privacy privacy;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "likes",
            joinColumns = @JoinColumn(name = "post_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private List<User> usersLikedPost;

    public Post() {
    }

    public Post(String title, String postMessage, User author) {
        this.title = title;
        this.postMessage = postMessage;
        this.author = author;
    }
    public long getPostId() {
        return postId;
    }

    public void setPostId(long postId) {
        this.postId = postId;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getPostMessage() {
        return postMessage;
    }

    public void setPostMessage(String postMessage) {
        String htmlEscapedPostMessage = MethodHelper.replaceHtmlInputEscapeCode(postMessage);
        this.postMessage = htmlEscapedPostMessage;
    }

    public String getTimeStamp() {
      SimpleDateFormat formatter = new SimpleDateFormat("dd-MMMM-yyyy HH:mm");
        return formatter.format(timeStamp);
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        String htmlEscapedPostTitle = MethodHelper.replaceHtmlInputEscapeCode(title);
        this.title = htmlEscapedPostTitle;
    }

    public Privacy getPrivacy() {
        return privacy;
    }

    public void setPrivacy(Privacy privacy) {
        this.privacy = privacy;
    }

    public List<User> getUsersWhoLikedPost() {
        return usersLikedPost;
    }

    public void setUsersWhoLikedPost(List<User> usersLikedPost) {
        this.usersLikedPost = new ArrayList<>(usersLikedPost);
    }
}
