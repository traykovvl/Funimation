package team1.socnet.models;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "privacies")
public class Privacy {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "privacy_id")
  private long privacyId;

  @NotNull
  @Column(name = "privacy_type")
  private String privacyType;

  @OneToMany(mappedBy = "privacy")
  @JsonIgnore
  private Set<Photo> photos;


  @OneToMany(mappedBy = "privacy")
  @JsonIgnore
  private Set<Post> posts;

  public long getPrivacyId() {
    return privacyId;
  }

  public void setPrivacyId(long privacyId) {
    this.privacyId = privacyId;
  }


  public String getPrivacyType() {
    return privacyType;
  }

  public void setPrivacyType(String privacyType) {
    this.privacyType = privacyType;
  }

  public Set<Photo> getPhotos() {
    return photos;
  }

  public void setPhotos(Set<Photo> photos) {
    this.photos = photos;
  }

  public Set<Post> getPosts() {
    return posts;
  }

  public void setPosts(Set<Post> posts) {
    this.posts = posts;
  }
}
