package team1.socnet.controllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;
import team1.socnet.controllers.helpers.MethodHelper;
import team1.socnet.models.User;
import team1.socnet.models.dto.UserUpdateDto;
import team1.socnet.services.contracts.*;

import java.io.IOException;

import static team1.socnet.controllers.helpers.ControllerConstant.*;
import static team1.socnet.controllers.helpers.MethodHelper.getErrorMessage;

@Controller
@RequestMapping("/users")
public class UserEditingController {
    private static final String SHOW_ALL_USERS_USER_PAGE = "show-all-users";
    private static final String USER_UPDATE_DTO = "userUpdateDto";
    private static final String USERS_AUTHORITIES = "usersAuthorities";
    private static final String USER_CITY = "userCity";
    private static final String USER_OCCUPATION = "userOccupation";
    private static final String USER_PHOTOS_PRIVACY = "userPhotosPrivacy";
    private static final String UPDATE_USER_HTML_PAGE = "update-user";
    private static final String USERS = "users";

    private UserService userService;
    private AuthorityService authorityService;
    private OccupationService occupationService;
    private CityService cityService;
    private PrivacyService privacyService;
    private org.modelmapper.ModelMapper modelMapper;

    @Autowired
    public UserEditingController(UserService userService,
                                 OccupationService occupationService,
                                 AuthorityService authorityService,
                                 CityService cityService,
                                 PrivacyService privacyService,
                                 ModelMapper modelMapper) {
        this.userService = userService;
        this.authorityService = authorityService;
        this.occupationService = occupationService;
        this.cityService = cityService;
        this.privacyService = privacyService;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public String getAllUsersPage(Model model, Authentication authentication) {
        User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
        model.addAttribute(USER, loggedUser);
        model.addAttribute(USER_LIST, userService.findAllByEnabled());
        return SHOW_ALL_USERS_USER_PAGE;
    }

    @GetMapping("/update/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model, Authentication authentication) {
        User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
        String authority = MethodHelper.getUserAuthorityRole(loggedUser, userService);
        User user = userService.findByUserId(id);
        UserUpdateDto userUpdateDto = modelMapper.map(user, UserUpdateDto.class);
        if (checkAuthority(user.getUserId(), loggedUser, authority, "ROLE_USER")) return ACCESS_DENIED_HTML_PAGE;
//        UserUpdateDto userUpdateDto = new UserUpdateDto(user);

        model.addAttribute(USER, loggedUser);
        model.addAttribute(USER_UPDATE_DTO, userUpdateDto);
        model.addAttribute(USERS_AUTHORITIES, authorityService.findAll());
        model.addAttribute(USER_CITY, cityService.findAll());
        model.addAttribute(USER_OCCUPATION, occupationService.findAll());
        model.addAttribute(USER_PHOTOS_PRIVACY, privacyService.findAll());
        return UPDATE_USER_HTML_PAGE;
    }

    @PostMapping("/update/{id}")
    public String updateUser(@PathVariable("id") long id, UserUpdateDto userUpdateDto,
                             BindingResult bindingResult) {


        if (bindingResult.hasErrors()) {
            return UPDATE_USER_HTML_PAGE;
        }

        try {
            userService.updateUser(userUpdateDto, id);
        } catch (IOException ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return "redirect:/myPage/{id}";
    }

    @GetMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") long id, Model model, Authentication authentication) {
        User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
        String authority = MethodHelper.getUserAuthorityRole(loggedUser, userService);
        if (checkAuthority(id, loggedUser, authority, ROLE_USER)) return ACCESS_DENIED_HTML_PAGE;
        try {
            userService.delete(id, loggedUser.getUserId());
        } catch (IllegalArgumentException ex) {
            return getErrorMessage(model, ex);
        }
        if ((authority.equals(ROLE_USER)) || loggedUser.getUserId() == id) {
            authentication.setAuthenticated(false);
            return LOGIN_HTML_PAGE;
        } else {
            model.addAttribute(USERS, userService.findAllByEnabled());

            return "redirect:/users";
        }
    }

    private boolean checkAuthority(@PathVariable("id") long id, User loggedUser, String authority, String roleUser) {
        if (loggedUser.getUserId() != id) {
            if ((authority.equals(roleUser)))
                return true;
        }
        return false;
    }
}
