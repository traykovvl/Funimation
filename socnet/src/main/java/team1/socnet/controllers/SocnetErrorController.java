package team1.socnet.controllers;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import static team1.socnet.controllers.helpers.ControllerConstant.*;

@Controller
public class SocnetErrorController implements ErrorController {

    @RequestMapping("/error")
    public String handleError(HttpServletRequest request) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        if (status != null) {
            Integer statusCode = Integer.valueOf(status.toString());

            if (statusCode == HttpStatus.FORBIDDEN.value()) {
                return ACCESS_DENIED_HTML_PAGE;
            } else if (statusCode == HttpStatus.NOT_FOUND.value()) {
                return PAGE_NOT_FOUND_HTML_PAGE;
            } else if (statusCode == HttpStatus.METHOD_NOT_ALLOWED.value()) {
                return METHOD_NOT_ALLOWED_HTML_PAGE;
            } else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                return INTERNAL_SERVER_ERROR_HTML_PAGE;
            }
        }
        return ERROR;
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
