package team1.socnet.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import team1.socnet.controllers.helpers.MethodHelper;
import team1.socnet.models.User;
import team1.socnet.services.contracts.UserService;

import java.security.Principal;

import static team1.socnet.controllers.helpers.ControllerConstant.USER;

@Controller
public class HomePageController {
    private static final String INDEX_HTML_PAGE = "index";
    private static final String INDEX_1_HTML_PAGE = "index1_model";
    private static final String TERMS = "terms";

    private UserService userService;

    public HomePageController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/")
    public String getHomePage(Model model, Authentication authentication, Principal principal) {
        if (principal != null) {
            User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
            model.addAttribute(USER, loggedUser);
        return INDEX_HTML_PAGE;
        } else {
            return INDEX_HTML_PAGE;
        }
    }

    @GetMapping("/index1")
    public String getHomePage1(Model model, Authentication authentication, Principal principal) {
        if (principal != null) {
            User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
            model.addAttribute(USER, loggedUser);
            return INDEX_1_HTML_PAGE;
        } else {
            return INDEX_1_HTML_PAGE;
        }
    }

    @GetMapping("/terms")
    public String getTermsAndConditions(){
        return TERMS;
    }
}