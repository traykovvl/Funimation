package team1.socnet.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import team1.socnet.controllers.helpers.MethodHelper;
import team1.socnet.models.User;
import team1.socnet.services.contracts.FriendshipService;
import team1.socnet.services.contracts.UserService;

import static team1.socnet.controllers.helpers.ControllerConstant.USER;
import static team1.socnet.controllers.helpers.MethodHelper.getErrorMessage;

@Controller
@RequestMapping("/friendship")
public class FriendShipController {
    private static final String SENT_REQUEST_SUCCESS_HTML_PAGE = "sent-request-success";
    private static final String ACCEPT_REQUEST_SUCCESS_HTML_PAGE = "accept-request-success";
    private static final String UNFRIEND_SUCCESS_HTML_PAGE = "unfriend-success";
    private static final String FRIENDS_LIST_HTML_PAGE = "friendsList";
    private static final String PENDING_FRIENDS_LIST_HTML_PAGE = "pending-friends-list";
    private static final String FRIENDS_LIST = "friendsList";
    private static final String FRIEND = "friend";

    private FriendshipService friendShipService;
    private UserService userService;

    @Autowired
    public FriendShipController(FriendshipService friendShipService, UserService userService) {
        this.friendShipService = friendShipService;
        this.userService = userService;
    }

    @GetMapping("/request/{userReceivingRequestId}")
    public String askingFriendship(@PathVariable("userReceivingRequestId") long userReceivingRequestId,
                                   Authentication authentication, Model model) {
        User userRequesting = userService.findByEmail(authentication.getName());
        User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
        model.addAttribute(USER, loggedUser);

        try {
            friendShipService.saveFriendshipRequest(userReceivingRequestId, userRequesting);
        } catch (IllegalArgumentException ex) {
            return getErrorMessage(model, ex);
        }
        return SENT_REQUEST_SUCCESS_HTML_PAGE;
    }

    @GetMapping("/accept/{userSendingRequestId}")
    public String acceptingFriendship(@PathVariable("userSendingRequestId") long userSendingRequestId,
                                      Authentication authentication, Model model) {

        User userAccepting = userService.findByEmail(authentication.getName());
        model.addAttribute(FRIEND, userService.findByUserId(userSendingRequestId));
        User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
        model.addAttribute(USER, loggedUser);

        try {
            friendShipService.acceptFriendshipRequest(userSendingRequestId, userAccepting);
        } catch (IllegalArgumentException ex) {
            return getErrorMessage(model, ex);
        }
        return ACCEPT_REQUEST_SUCCESS_HTML_PAGE;
    }

    @GetMapping("/delete/{friendShipId}")
    public String deletingFriendship(@PathVariable("friendShipId") long friendShipId, Model model, Authentication authentication) {
        User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
        model.addAttribute(USER, loggedUser);

        try {
            friendShipService.deleteFriendship(friendShipId);
        } catch (IllegalArgumentException ex) {
            return getErrorMessage(model, ex);
        }
        return UNFRIEND_SUCCESS_HTML_PAGE;
    }

    @GetMapping("/friendsList")
    public String showFriendsList(Authentication authentication, Model model) {
        User user = userService.findByEmail(authentication.getName());
        model.addAttribute(USER, user);
        model.addAttribute(FRIENDS_LIST, friendShipService.findAllFriends(user));
        return FRIENDS_LIST_HTML_PAGE;
    }

    @GetMapping("/pendingFriendsList")
    public String showPendingFriendsList(Authentication authentication, Model model) {
        User user = userService.findByEmail(authentication.getName());
        model.addAttribute(USER, user);
        model.addAttribute(FRIENDS_LIST, friendShipService.findAllPendingFriends(user));
        return PENDING_FRIENDS_LIST_HTML_PAGE;
    }
}
