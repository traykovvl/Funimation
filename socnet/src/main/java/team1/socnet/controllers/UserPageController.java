package team1.socnet.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import team1.socnet.controllers.helpers.MethodHelper;
import team1.socnet.models.User;
import team1.socnet.services.contracts.FriendshipService;
import team1.socnet.services.contracts.PostService;
import team1.socnet.services.contracts.UserService;

import static team1.socnet.controllers.helpers.ControllerConstant.FRIENDS;
import static team1.socnet.controllers.helpers.ControllerConstant.USER;

@Controller
@RequestMapping("/myPage")
public class UserPageController {
    private static final String AUTHOR = "author";
    private static final String USER_PUBLIC_POSTS = "userPublicPosts";
    private static final String ALL_USER_POSTS = "allUserPosts";
    private static final String USER_PAGE_HTML_PAGE = "user-page";
    private static final String NEWS_FEED_HTML_PAGE = "news-feed";
    private static final String FRIENDS_POSTS = "friendsPosts";

    private UserService userService;
    private PostService postService;
    private FriendshipService friendshipService;

    public UserPageController(UserService userService, PostService postService, FriendshipService friendshipService) {
        this.userService = userService;
        this.postService = postService;
        this.friendshipService = friendshipService;
    }

    @GetMapping("/{userPageId}")
    public String getPageView(Model model, @PathVariable("userPageId") long userPageId, Authentication authentication ) {
        User loggedUser = MethodHelper.getLoggedUser(authentication,userService);
        model.addAttribute(USER, loggedUser );
        model.addAttribute(AUTHOR,userService.findByUserId(userPageId));
        model.addAttribute(FRIENDS, friendshipService.areFriends(userPageId,loggedUser));
        model.addAttribute(USER_PUBLIC_POSTS, postService.findAuthorPublicPost(userPageId));
        model.addAttribute(ALL_USER_POSTS, postService.findByAuthor(userPageId));
        return USER_PAGE_HTML_PAGE;
    }
    @GetMapping
    public String getNewsFeedPageView(Model model, Authentication authentication ) {
        User loggedUser = MethodHelper.getLoggedUser(authentication,userService);
        model.addAttribute(USER, loggedUser );
        model.addAttribute(FRIENDS_POSTS,postService.allFriendsPosts(loggedUser));
        return NEWS_FEED_HTML_PAGE;
    }
}
