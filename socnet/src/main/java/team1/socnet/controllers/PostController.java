package team1.socnet.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import team1.socnet.controllers.helpers.MethodHelper;
import team1.socnet.models.Post;
import team1.socnet.models.User;
import team1.socnet.services.contracts.PostService;
import team1.socnet.services.contracts.PrivacyService;
import team1.socnet.services.contracts.UserService;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

import static team1.socnet.controllers.helpers.ControllerConstant.*;
import static team1.socnet.controllers.helpers.MethodHelper.getErrorMessage;
import static team1.socnet.controllers.helpers.MethodHelper.getLoggedUser;

@Controller
@RequestMapping("/posts")
public class PostController {
    private static final String PRIVACIES = "privacies";
    private static final String ALL_PUBLIC_POSTS = "allPublicPosts";
    private static final String ALL_POSTS = "allPosts";
    private static final String POST_ID = "id";
    private static final String POST_MESSAGE_CREATE_ERROR_MASSAGE = "Post message field cannot be empty";
    private static final String POST_TITLE_CREATE_ERROR_MASSAGE = "Post title field cannot be empty";
    private static final String SHOW_ALL_PUBLICPOSTS_HTML_PAGE = "show-all-publicposts";
    private static final String SHOW_ALL_REQUEST_OFFERS_FOR_KIDS_HTML_PAGE = "show-all-request-offers-for-kids";
    private static final String SHOW_ALL_REQUEST_OFFERS_FOR_ADULTS_HTML_PAGE = "show-all-request-offers-for-adults";
    private static final String SHOW_ALL_POSTS_HTML_PAGE = "show-all-posts";
    private static final String CREATE_POST_HTML_PAGE = "create-post";
    private static final String TITLE = "title";
    private static final String POST_MESSAGE = "postMessage";
    private static final String MY_PAGE_URL = "redirect:/myPage";
    private static final String UPDATE_POST_HTML_PAGE = "update-post";
    private static final String CREATE_REQUEST_OFFER_HTML_PAGE = "create-request-offer";

    private PostService postService;
    private UserService userService;
    private PrivacyService privacyService;

    @Autowired
    public PostController(PostService postService,
                          UserService userService,
                          PrivacyService privacyService) {
        this.postService = postService;
        this.userService = userService;
        this.privacyService = privacyService;
    }

    @GetMapping
    public String getAllPublicPost(Model model, Authentication authentication, Principal principal) {
        List<Post> postList = postService.findPublicPost();
        String result = SHOW_ALL_PUBLICPOSTS_HTML_PAGE;

        if (getPostListIfPrincipleNotNull(model, authentication, principal, postList)) {
            return result;
        }

        model.addAttribute(ALL_PUBLIC_POSTS, postList);
        return result;
    }

    @GetMapping("/requestOfferForKids")
    public String getAllRequestOfferForKids(Model model, Authentication authentication, Principal principal) {
        List<Post> postList = postService.findAllRequestOfferForKids();
        String result = SHOW_ALL_REQUEST_OFFERS_FOR_KIDS_HTML_PAGE;

        if (getPostListIfPrincipleNotNull(model, authentication, principal, postList)) {
            return result;
        }

        model.addAttribute(ALL_PUBLIC_POSTS, postList);
        return result;
    }

    @GetMapping("/requestOfferForAdult")
    public String getAllRequestOfferForAdult(Model model, Authentication authentication, Principal principal) {
        List<Post> postList = postService.findAllRequestOffersForAdults();
        String result = SHOW_ALL_REQUEST_OFFERS_FOR_ADULTS_HTML_PAGE;

        if (getPostListIfPrincipleNotNull(model, authentication, principal, postList)) {
            return result;
        }

        model.addAttribute(ALL_PUBLIC_POSTS, postList);
        return result;
    }

    @GetMapping("/admin")
    public String getAllPost(Model model, Authentication authentication) {
        User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
        model.addAttribute(USER, loggedUser);
        model.addAttribute(ALL_POSTS, postService.findAll());
        return SHOW_ALL_POSTS_HTML_PAGE;
    }

    @GetMapping("/new")
    public String getNewPostView(Model model, Authentication authentication) {
        User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
        model.addAttribute(USER, loggedUser);
        model.addAttribute(POST, new Post());
        model.addAttribute(PRIVACIES, privacyService.findAll());
        return CREATE_POST_HTML_PAGE;
    }

    @PostMapping("/new")
    public String newPost(Model model, @ModelAttribute @Valid Post post,
                          BindingResult bindingResult, Authentication authentication) {

        User author = MethodHelper.getLoggedUser(authentication, userService);

        if (post.getTitle().isEmpty()) {
            bindingResult.rejectValue(TITLE, null, POST_TITLE_CREATE_ERROR_MASSAGE);
        }

        if (post.getPostMessage().isEmpty()) {
            bindingResult.rejectValue(POST_MESSAGE, null, POST_MESSAGE_CREATE_ERROR_MASSAGE);
        }

        if (bindingResult.hasErrors()) {
            model.addAttribute(POST, post);
            model.addAttribute(PRIVACIES, privacyService.findAll());
            return CREATE_POST_HTML_PAGE;
        }
        postService.savePost(post, author);
        return MY_PAGE_URL;
    }

    @GetMapping("/update/{id}")
    public String showPostForm(Model model, @PathVariable(POST_ID) long id, Authentication authentication) {
        User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
        String authority = MethodHelper.getUserAuthorityRole(loggedUser,userService);
        Post post = postService.findById(id);
        if (checkAuthority(post.getAuthor(), authority, loggedUser)) return ACCESS_DENIED_HTML_PAGE;
        model.addAttribute(USER, loggedUser);
        model.addAttribute(POST, post);
        model.addAttribute(PRIVACIES, privacyService.findAll());

        return UPDATE_POST_HTML_PAGE;
    }

    @PostMapping("/update/{id}")
    public String updatePost(Model model, @Valid @ModelAttribute Post post, @PathVariable(POST_ID) long id,
                             Authentication authentication, BindingResult bindingResult) {

        User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
        model.addAttribute(USER, loggedUser);

        if (post.getTitle().isEmpty()) {
            bindingResult.rejectValue(TITLE, null, POST_TITLE_CREATE_ERROR_MASSAGE);
        }

        if (post.getPostMessage().isEmpty()) {
            bindingResult.rejectValue(POST_MESSAGE, null, POST_MESSAGE_CREATE_ERROR_MASSAGE);
        }

        if (bindingResult.hasErrors()) {
            model.addAttribute(POST, post);
            model.addAttribute(PRIVACIES, privacyService.findAll());
            return UPDATE_POST_HTML_PAGE;
        }
        try {
            postService.updatePost(post);
        } catch (IllegalArgumentException ex) {
            return getErrorMessage(model, ex);
        }
        return MY_PAGE_URL;
    }

    @GetMapping("/delete/{id}")
    public String deletePost(Model model, @PathVariable(POST_ID) long id, Authentication authentication) {
        User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
        String authority = MethodHelper.getUserAuthorityRole(loggedUser,userService);
        if (checkAuthority(loggedUser, authority, postService.findById(id).getAuthor())) return ACCESS_DENIED_HTML_PAGE;
        try {
            postService.deletePost(id);
        } catch (IllegalArgumentException ex) {
            return getErrorMessage(model, ex);
        }
        return MY_PAGE_URL;
    }

    @GetMapping("/like/{id}")
    public String showLikeForm(Model model, @PathVariable(POST_ID) long id,
                               @RequestParam(name = "page", required = false) String page) {
        try {
            Post post = postService.findById(id);
            model.addAttribute(POST, post);
        } catch (IllegalArgumentException ex) {
            return getErrorMessage(model, ex);
        }
        return "redirect:/posts";
    }

    @PostMapping("/like/{id}")
    public String likePost(@PathVariable(POST_ID) long id, @RequestParam(name = "page", required = false) String page,
                           Authentication authentication) {
        Post post = postService.findById(id);
        User userWhoLikes = getLoggedUser(authentication, userService);
        postService.likeDislikePost(post, userWhoLikes);
        if (page.equals("one")) {
            return "redirect:/";
        }
        if (page.equals("two")) {
            return "redirect:/index1";
        }
        if (page.equals("kids")) {
            return "redirect:/posts/requestOfferForKids";
        }
        if (page.equals("adults")) {
            return "redirect:/posts/requestOfferForAdult";
        }
        return "redirect:/";
    }

    @GetMapping("/newRequestOffer")
    public String getNewRequestOfferView(Model model, Authentication authentication) {
        User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
        model.addAttribute(USER, loggedUser);
        model.addAttribute(POST, new Post());
        model.addAttribute(PRIVACIES, privacyService.findAll());
        return CREATE_REQUEST_OFFER_HTML_PAGE;
    }

    @PostMapping("/newRequestOffer")
    public String newRequestOffer(Model model, @ModelAttribute @Valid Post post,
                                  BindingResult bindingResult, Authentication authentication) {

        User author = getUser(model, authentication);
        if (post.getTitle().isEmpty()) {
            bindingResult.rejectValue(TITLE, null, POST_TITLE_CREATE_ERROR_MASSAGE);
        }

        if (post.getPostMessage().isEmpty()) {
            bindingResult.rejectValue(POST_MESSAGE, null, POST_MESSAGE_CREATE_ERROR_MASSAGE);
        }

        if (bindingResult.hasErrors()) {
            model.addAttribute(POST, post);
            model.addAttribute(PRIVACIES, privacyService.findAll());
            return CREATE_REQUEST_OFFER_HTML_PAGE;
        }

        postService.savePost(post, author);
        return MY_PAGE_URL;
    }

    private User getUser(Model model, Authentication authentication) {
        User author = MethodHelper.getLoggedUser(authentication, userService);
        model.addAttribute(USER, author);
        return author;
    }
    private boolean getPostListIfPrincipleNotNull(Model model, Authentication authentication, Principal principal, List<Post> postList) {
        if (principal != null) {
            User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
            model.addAttribute(USER, loggedUser);
            model.addAttribute(ALL_PUBLIC_POSTS, postList);
            return true;
        }
        return false;
    }
    private boolean checkAuthority(User loggedUser, String authority, User author) {
        if (loggedUser != author) {
            if (authority.equals(ROLE_USER))
                return true;
        }
        return false;
    }
}
