package team1.socnet.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import team1.socnet.controllers.helpers.MethodHelper;
import team1.socnet.models.PasswordResetToken;
import team1.socnet.models.User;
import team1.socnet.services.contracts.PasswordResetTokenService;
import team1.socnet.services.contracts.UserService;
import team1.socnet.models.dto.PasswordResetDto;

import javax.transaction.Transactional;
import javax.validation.Valid;

import static team1.socnet.controllers.helpers.ControllerConstant.ERROR;
import static team1.socnet.controllers.helpers.ControllerConstant.USER;

@Controller
@RequestMapping("/reset-password")
public class PasswordResetController {
    private static final String RESET_TOKEN_NOT_FOUND_ERROR_MESSAGE = "Could not find password reset token.";
    private static final String TOKEN_EXPIRED_ERROR_MESSAGE = "Token has expired, please request a new password reset.";
    private static final String RESET_PASSWORD_HTML_PAGE = "reset-password";
    private static final String TOKEN = "token";
    private static final String PASSWORD_RESET_FORM = "passwordResetForm";

    private UserService userService;
    private PasswordResetTokenService passwordResetTokenService;
    private PasswordEncoder passwordEncoder;

    public PasswordResetController(UserService userService,
                                   PasswordResetTokenService passwordResetTokenService,
                                   PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordResetTokenService = passwordResetTokenService;
        this.passwordEncoder = passwordEncoder;
    }

    @ModelAttribute(PASSWORD_RESET_FORM)
    public PasswordResetDto passwordReset() {
        return new PasswordResetDto();
    }

    @GetMapping
    public String displayResetPasswordPage(@RequestParam(required = false) String token,
                                           Model model, Authentication authentication) {

        User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
        model.addAttribute(USER, loggedUser);
        PasswordResetToken resetToken = passwordResetTokenService.findByToken(token);
        if (resetToken == null){
            model.addAttribute(ERROR, RESET_TOKEN_NOT_FOUND_ERROR_MESSAGE);
        } else if (resetToken.isExpired()){
            model.addAttribute(ERROR, TOKEN_EXPIRED_ERROR_MESSAGE);
        } else {
            model.addAttribute(TOKEN, resetToken.getToken());
        }

        return RESET_PASSWORD_HTML_PAGE;
    }

    @PostMapping
    @Transactional
    public String handlePasswordReset(@ModelAttribute(PASSWORD_RESET_FORM) @Valid PasswordResetDto form,
                                      BindingResult result, Model model,
                                      RedirectAttributes redirectAttributes, Authentication authentication) {

        User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
        model.addAttribute(USER, loggedUser);

        if (result.hasErrors()){
            redirectAttributes.addFlashAttribute(BindingResult.class.getName() + ".passwordResetForm", result);
            redirectAttributes.addFlashAttribute(PASSWORD_RESET_FORM, form);
            return "redirect:/reset-password?token=" + form.getToken();
        }

        PasswordResetToken token = passwordResetTokenService.findByToken(form.getToken());
        User user = token.getUser();
        String updatedPassword = passwordEncoder.encode(form.getPassword());
        userService.updatePassword(updatedPassword, user.getUserId());
        passwordResetTokenService.delete(token);

        return "redirect:/login?resetSuccess";
    }

}