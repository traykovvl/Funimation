package team1.socnet.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import team1.socnet.controllers.helpers.MethodHelper;
import team1.socnet.models.Comment;
import team1.socnet.models.Post;
import team1.socnet.models.User;
import team1.socnet.services.contracts.CommentService;
import team1.socnet.services.contracts.FriendshipService;
import team1.socnet.services.contracts.PostService;
import team1.socnet.services.contracts.UserService;

import javax.validation.Valid;

import static team1.socnet.controllers.helpers.ControllerConstant.*;
import static team1.socnet.controllers.helpers.MethodHelper.getErrorMessage;

@Controller
@RequestMapping("/comments")
public class CommentController {
    private static final String POST_ID = "postId";
    private static final String COMMENT_ID = "id";
    private static final String SHOW_ALL_COMMENTS_HTML_PAGE = "show-all-comments";
    private static final String SHOW_ALL_POST_COMMENTS_HTML_PAGE = "show-all-post-comments";
    private static final String CREATE_COMMENT_HTML_PAGE = "create-comment";
    private static final String UPDATE_COMMENT_HTML_PAGE = "update-comment";
    private static final String COMMENT_BODY = "commentBody";
    private static final String COMMENTS_BY_POST_URL = "redirect:/comments/byPost/{postId}";
    private final String ALL_COMMENTS = "allComments";
    private static final String COMMENT_CREATE_ERROR_MASSAGE = "Comment field cannot be empty";

    private CommentService commentService;
    private UserService userService;
    private PostService postService;
    private FriendshipService friendShipService;

    @Autowired
    public CommentController(CommentService commentService, UserService userService,
                             PostService postService,
                             FriendshipService friendShipService) {
        this.commentService = commentService;
        this.userService = userService;
        this.postService = postService;
        this.friendShipService = friendShipService;
    }

    @GetMapping
    public String getAllComments(Model model, Authentication authentication) {
        User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
        model.addAttribute(USER, loggedUser);
        model.addAttribute(ALL_COMMENTS, commentService.findAll());
        return SHOW_ALL_COMMENTS_HTML_PAGE;
    }

    @GetMapping("/byPost/{postId}")
    public String getCommentsByPostView(Model model, @PathVariable(POST_ID) long postId, Authentication authentication) {

        try {
            Post post = postService.findById(postId);
            long postAuthorId = post.getAuthor().getUserId();
            User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
            model.addAttribute(USER, loggedUser);
            model.addAttribute(POST, post);
            model.addAttribute(FRIENDS, friendShipService.areFriends(postAuthorId, loggedUser));
            model.addAttribute(ALL_COMMENTS, commentService.findAllByPost(post));
        } catch (IllegalArgumentException ex) {
            return getErrorMessage(model, ex);
        }
        return SHOW_ALL_POST_COMMENTS_HTML_PAGE;
    }

    @GetMapping("/new/{postId}")
    public String getNewCommentView(Model model, @PathVariable(POST_ID) long postId,Authentication authentication) {

        try {
            User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
            model.addAttribute(USER, loggedUser);
            Post post = postService.findById(postId);
            model.addAttribute(POST, post);
            model.addAttribute(COMMENT, new Comment());
        } catch (IllegalArgumentException ex) {
            return getErrorMessage(model, ex);
        }
        return CREATE_COMMENT_HTML_PAGE;
    }

    @PostMapping("/new/{postId}")
    public String newComment(Model model, @Valid @ModelAttribute Comment comment, @PathVariable(POST_ID) long postId,
                             BindingResult bindingResult, Authentication authentication) {

        User author = MethodHelper.getLoggedUser(authentication, userService);
        model.addAttribute(USER, author);
        Post post = postService.findById(postId);

        checkIfCommentBodyIsNull(comment, bindingResult);
        if (checkIfCommentHasBeenAttached(model, comment, bindingResult, post)) {
            return CREATE_COMMENT_HTML_PAGE;
        }

        commentService.saveComment(comment, author, post);
        return COMMENTS_BY_POST_URL;
    }

    @GetMapping("/update/{id}/{postId}")
    public String showCommentForm(Model model, @PathVariable(COMMENT_ID) long id, @PathVariable(POST_ID) long postId,
                                  Authentication authentication) {

        User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
        model.addAttribute(USER, loggedUser);

        try {
            Comment comment = commentService.findById(id);
            Post post = postService.findById(postId);
            model.addAttribute(COMMENT, comment);
            model.addAttribute(POST, post);
        } catch (IllegalArgumentException ex) {
            return getErrorMessage(model, ex);
        }

        return UPDATE_COMMENT_HTML_PAGE;
    }

    @PostMapping("/update/{id}/{postId}")
    public String updateComment(Model model, @Valid @ModelAttribute Comment comment,
                                @PathVariable(COMMENT_ID) long id, @PathVariable(POST_ID) long postId,
                                BindingResult bindingResult, Authentication authentication) {

        User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
        model.addAttribute(USER, loggedUser);
        Post post = postService.findById(postId);

        checkIfCommentBodyIsNull(comment, bindingResult);
        if (checkIfCommentHasBeenAttached(model, comment, bindingResult, post)){
            return UPDATE_COMMENT_HTML_PAGE;
        }

        commentService.updateComment(comment);
        return COMMENTS_BY_POST_URL;
    }

    @GetMapping("/delete/{id}/{postId}")
    public String deleteComment(Model model, @PathVariable(COMMENT_ID) long id, @PathVariable(POST_ID) long postId) {
        try {
            commentService.deleteComment(id);
        } catch (IllegalArgumentException ex) {
            return getErrorMessage(model, ex);
        }

        return COMMENTS_BY_POST_URL;
    }

    private void checkIfCommentBodyIsNull(@ModelAttribute @Valid Comment comment, BindingResult bindingResult) {
        if (comment.getCommentBody().isEmpty()) {
            bindingResult.rejectValue(COMMENT_BODY, null, COMMENT_CREATE_ERROR_MASSAGE);
        }
    }

    private boolean checkIfCommentHasBeenAttached(Model model, @ModelAttribute @Valid Comment comment, BindingResult bindingResult, Post post) {
        if (bindingResult.hasErrors()) {
            model.addAttribute(POST, post);
            model.addAttribute(COMMENT, comment);
            return true;
        }
        return false;
    }
}
