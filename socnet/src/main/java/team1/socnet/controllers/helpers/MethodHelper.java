package team1.socnet.controllers.helpers;

import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import team1.socnet.models.User;
import team1.socnet.services.contracts.UserService;

import static team1.socnet.controllers.helpers.ControllerConstant.ERROR;

public class MethodHelper {
    public static User getLoggedUser(Authentication authentication, UserService userService) {
        return userService.findByEmail(authentication.getName());
    }

    public static String getErrorMessage(Model model, IllegalArgumentException ex) {
        model.addAttribute(ERROR, ex.getMessage());
        return "error-display-message";
    }

    public static String replaceHtmlInputEscapeCode (String inputString) {
        String firstReplace = inputString.replaceAll("<", "~<~");
        String secondReplace = firstReplace.replaceAll(">", "~>~");
        return secondReplace;
    }

    public static String getUserAuthorityRole(User loggedUser, UserService userService) {
        return userService.getUserAuthority(loggedUser);
    }
}
