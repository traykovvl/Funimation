package team1.socnet.controllers.helpers;

import team1.socnet.models.Email;
import team1.socnet.models.PasswordResetToken;
import team1.socnet.models.User;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class EmailDetailsSetUpHelper {
    public static PasswordResetToken getPasswordResetToken(User createdUser) {
        PasswordResetToken token = new PasswordResetToken();
        token.setToken(UUID.randomUUID().toString());
        token.setUser(createdUser);
        token.setExpiryDate(30);
        return token;
    }

    public static Email getEmailDetails(User createdUser, String emailSender, String emailSubject) {
        Email mail = new Email();
        mail.setFrom(emailSender);
        mail.setTo(createdUser.getEmail());
        mail.setSubject(emailSubject);
        return mail;
    }

    public static void getEmailContentForm(HttpServletRequest request, User createdUser, PasswordResetToken token,
                                           Email mail, String modelParameterName, String definedUrl) {
        Map<String, Object> model = new HashMap<>();
        model.put("token", token);
        model.put("user", createdUser);
        String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
        model.put(modelParameterName, url + definedUrl + token.getToken());
        mail.setModel(model);
    }
}
