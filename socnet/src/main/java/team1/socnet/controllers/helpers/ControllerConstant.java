package team1.socnet.controllers.helpers;

public class ControllerConstant {
    public static final String POST = "post";
    public static final String USER = "user";
    public static final String ERROR = "error";
    public static final String COMMENT = "comment";
    public static final String FRIENDS = "friends";
    public static final String CITY = "city";
    public static final String COUNTRIES = "cityCountry";
    public static final String EMAIL = "email";
    public static final String ACCOUNT_EXISTS_ERROR_MESSAGE = "There is already an account registered with that email";
    public static final String ACCESS_DENIED_HTML_PAGE = "403-access-denied";
    public static final String PAGE_NOT_FOUND_HTML_PAGE = "404-page-not-found";
    public static final String METHOD_NOT_ALLOWED_HTML_PAGE = "405-method-not-allowed";
    public static final String INTERNAL_SERVER_ERROR_HTML_PAGE = "500-internal-server-error";
    public static final String ACCOUNT_DOES_NOT_EXISTS = "Account with this e-mail address does not exists. Please sign up";
    public static final String USER_LIST = "userList";
    public static final String LOGIN_HTML_PAGE = "login";
    public static final String ROLE_USER = "ROLE_USER";

    public static final String TEAM_1_SOCNET_EMAIL = "no-reply@team1.socnet.com";
    public static final String FUNIMATION_COMPLETE_REGISTRATION_SUBJECT_EMAIL = "Funimation - Complete Registration!";
    public static final String CONFIRM_URL_MODEL_PATTERN = "confirmUrl";
    public static final String CONFIRM_ACCOUNT_TOKEN_URL = "/confirm-account?token=";
}
