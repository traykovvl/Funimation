package team1.socnet.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import team1.socnet.controllers.helpers.EmailDetailsSetUpHelper;
import team1.socnet.models.Email;
import team1.socnet.models.PasswordResetToken;
import team1.socnet.models.User;
import team1.socnet.services.contracts.EmailService;
import team1.socnet.services.contracts.PasswordResetTokenService;
import team1.socnet.services.contracts.UserService;
import team1.socnet.models.dto.PasswordForgotDto;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import static team1.socnet.controllers.helpers.ControllerConstant.*;

@Controller
@RequestMapping("/forgot-password")
public class PasswordForgotController {

    public static final String FORGOT_PASSWORD_HTML_PAGE = "forgot-password";
    public static final String FORGOT_PASSWORD_FORM = "forgotPasswordForm";
    private UserService userService;
    private PasswordResetTokenService passwordResetTokenService;
    private EmailService emailService;

    @Autowired
    public PasswordForgotController(UserService userService, PasswordResetTokenService passwordResetTokenService, EmailService emailService) {
        this.userService = userService;
        this.passwordResetTokenService = passwordResetTokenService;
        this.emailService = emailService;
    }

    @ModelAttribute(FORGOT_PASSWORD_FORM)
    public PasswordForgotDto forgotPasswordDto() {
        return new PasswordForgotDto();
    }

    @GetMapping
    public String displayForgotPasswordPage() {
        return FORGOT_PASSWORD_HTML_PAGE;
    }

    @PostMapping
    public String processForgotPasswordForm(@ModelAttribute(FORGOT_PASSWORD_FORM) @Valid PasswordForgotDto form,
                                            BindingResult result,
                                            HttpServletRequest request) {
        if (result.hasErrors()){
            return FORGOT_PASSWORD_HTML_PAGE;
        }

        User user = userService.findByEmail(form.getEmail());
        if (user == null){
            result.rejectValue(EMAIL, null, ACCOUNT_DOES_NOT_EXISTS);
            return FORGOT_PASSWORD_HTML_PAGE;
        }

        PasswordResetToken token = EmailDetailsSetUpHelper.getPasswordResetToken(user);
        passwordResetTokenService.save(token);

        Email mail = EmailDetailsSetUpHelper.getEmailDetails(user, TEAM_1_SOCNET_EMAIL,
                                                            "Password reset request");

        EmailDetailsSetUpHelper.getEmailContentForm(request, user, token, mail,
                "resetUrl",
                "/reset-password?token=");

        try {
            emailService.sendEmailForResetOfPassword(mail);
        } catch (MessagingException e) {
            e.printStackTrace();
            return "redirect:/forgot-password?failure";
        }

        return "redirect:/forgot-password?success";
    }
}
