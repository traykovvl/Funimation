package team1.socnet.controllers.restcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import team1.socnet.models.Post;
import team1.socnet.models.User;
import team1.socnet.services.contracts.PageableService;
import team1.socnet.services.contracts.PostService;
import team1.socnet.services.contracts.UserService;

import java.util.List;

@RestController
@RequestMapping("/api/posts")
public class PostRestController {
    private PageableService pageableService;
    private PostService postService;
    private UserService userService;

    @Autowired
    public PostRestController(PageableService pageableService, PostService postService, UserService userService) {
        this.pageableService = pageableService;
        this.postService = postService;
        this.userService = userService;
    }

    @GetMapping("/{fromPage}/{postsPerPage}")
    public List<Post> getAllPublicPost(@PathVariable int fromPage, @PathVariable int postsPerPage) {
        return pageableService.findPublicPost(fromPage, postsPerPage);
    }

    @GetMapping("/requestOfferForKids/{fromPage}/{postsPerPage}")
    public List<Post> getAllRequestOfferForKids(@PathVariable int fromPage, @PathVariable int postsPerPage) {
        return pageableService.findAllRequestOfferForKids(fromPage, postsPerPage);
    }

    @GetMapping("/requestOfferForAdult/{fromPage}/{postsPerPage}")
    public List<Post> getAllRequestOfferForAdult(@PathVariable int fromPage, @PathVariable int postsPerPage) {
        return pageableService.findAllRequestOfferForAdults(fromPage, postsPerPage);
    }

    @GetMapping("/admin/{fromPage}/{postsPerPage}")
    public List<Post> getAllPost(@PathVariable int fromPage, @PathVariable int postsPerPage) {
        return pageableService.findAllByPages(fromPage, postsPerPage);
    }

    @GetMapping("/{userPageId}/{fromPage}/{postsPerPage}")
    public List<Post> getAllAuthorsPosts(@PathVariable long userPageId, @PathVariable int fromPage, @PathVariable int postsPerPage) {
        return pageableService.findAllAuthorsPosts(userPageId, fromPage, postsPerPage);
    }

    @GetMapping("/authorPublic/{userPageId}/{fromPage}/{postsPerPage}")
    public List<Post> getAllAuthorsPublicPosts(@PathVariable long userPageId, @PathVariable int fromPage, @PathVariable int postsPerPage) {
        return pageableService.findAllAuthorsPublicPosts(userPageId, fromPage, postsPerPage);
    }

    @GetMapping("/newsFeed/{fromPage}/{postsPerPage}")
    public List<Post> getAllAuthorsAndFriendsPosts(Authentication authentication, @PathVariable int fromPage,
                                                   @PathVariable int postsPerPage) {
        User user = userService.findByEmail(authentication.getName());
        return pageableService.allFriendsPosts(user, fromPage, postsPerPage);
    }
}
