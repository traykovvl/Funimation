package team1.socnet.controllers.restcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import team1.socnet.models.User;
import team1.socnet.services.contracts.UserService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserRestController {
    private UserService userService;

    @Autowired
    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{id}")
    public User getById(@PathVariable long id) {
        return userService.findByUserId(id);
    }

    @GetMapping("/all/{fromPage}/{usersPerPage}")
    public List<User> getUsers(@PathVariable int fromPage, @PathVariable int usersPerPage) {
        return userService.findAllByPages(fromPage, usersPerPage);
    }

    @GetMapping(path = "filtered")
    public List<User> getAllByFilter(@RequestParam @Valid String filterParam) {
        return userService.filterByUsernameAndEmailAndFirstNameAndLastName(filterParam);
    }
}
