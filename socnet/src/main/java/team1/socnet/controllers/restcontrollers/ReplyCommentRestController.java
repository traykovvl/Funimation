package team1.socnet.controllers.restcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import team1.socnet.models.Comment;
import team1.socnet.services.contracts.CommentService;
import team1.socnet.services.contracts.PageableService;

import java.util.List;

@RestController
@RequestMapping("/api/replyComments")
public class ReplyCommentRestController {
    private PageableService pageableService;
    private CommentService commentService;

    @Autowired
    public ReplyCommentRestController(CommentService commentService,
                                      PageableService pageableService) {
        this.commentService = commentService;
        this.pageableService = pageableService;
    }

    @GetMapping("/admin/{fromPage}/{replyCommentsPerPage}")
    public List<Comment> getAllReplyComments(@PathVariable int fromPage, @PathVariable int replyCommentsPerPage) {
        return pageableService.findAllRepliesToCommentByPage(fromPage, replyCommentsPerPage);
    }

    @GetMapping("/byComment/{commentId}/{fromPage}/{replyCommentsPerPage}")
    public List<Comment> getCommentsByPostView(@PathVariable long commentId,
                                             @PathVariable int fromPage,
                                             @PathVariable int replyCommentsPerPage) {
        try {
            Comment comment = commentService.findById(commentId);
            return pageableService.findAllRepliesToCommentByCommentByPage(comment,fromPage, replyCommentsPerPage);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

}
