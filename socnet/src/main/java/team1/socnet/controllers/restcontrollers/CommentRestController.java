package team1.socnet.controllers.restcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import team1.socnet.models.Comment;
import team1.socnet.models.Post;
import team1.socnet.services.contracts.PageableService;
import team1.socnet.services.contracts.PostService;

import java.util.List;

@RestController
@RequestMapping("/api/comments")
public class CommentRestController {

    private PostService postService;
    private PageableService pageableService;

    @Autowired
    public CommentRestController(PostService postService,
                                 PageableService pageableService) {
        this.postService = postService;
        this.pageableService = pageableService;
    }

    @GetMapping("/admin/{fromPage}/{commentsPerPage}")
    public List<Comment> getAllComments(@PathVariable int fromPage, @PathVariable int commentsPerPage) {
        return pageableService.findAllCommentsByPage(fromPage, commentsPerPage);
    }

    @GetMapping("/byPost/{postId}/{fromPage}/{commentsPerPage}")
    public List<Comment> getCommentsByPostView(@PathVariable long postId,
                                               @PathVariable int fromPage,
                                               @PathVariable int commentsPerPage) {
        try {
            Post post = postService.findById(postId);
            return pageableService.findAllCommentsByPostByPage(post, fromPage, commentsPerPage);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }
}
