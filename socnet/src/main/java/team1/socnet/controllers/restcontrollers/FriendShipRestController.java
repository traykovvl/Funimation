package team1.socnet.controllers.restcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import team1.socnet.models.Friendship;
import team1.socnet.models.User;
import team1.socnet.services.contracts.FriendshipService;
import team1.socnet.services.contracts.UserService;

import java.util.List;


@RestController
@RequestMapping("/api/friendship")
public class FriendShipRestController {
    private FriendshipService friendShipService;
    private UserService userService;

    @Autowired
    public FriendShipRestController(FriendshipService friendShipService, UserService userService) {
        this.friendShipService = friendShipService;
        this.userService = userService;
    }

    @GetMapping("/friendsList")
    public List<Friendship> showFriendsList(Authentication authentication) {
        User user = userService.findByEmail(authentication.getName());
        return friendShipService.findAllFriends(user);
    }

    @GetMapping("/pendingFriendsList")
    public List<Friendship> showPendingFriendsList(Authentication authentication) {
        User user = userService.findByEmail(authentication.getName());
        return friendShipService.findAllPendingFriends(user);
    }
}
