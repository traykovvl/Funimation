package team1.socnet.controllers.restcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import team1.socnet.models.City;
import team1.socnet.services.contracts.CityService;

import java.util.List;

@RestController
@RequestMapping("/api/cities")
public class CityRestController {

    private CityService cityService;

    @Autowired
    public CityRestController(CityService cityService) {
        this.cityService = cityService;
    }

    @GetMapping
    public List<City> getAllCitiesPage() {
        return cityService.findAll();
    }

}

