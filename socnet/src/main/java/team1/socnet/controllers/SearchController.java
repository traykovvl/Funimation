package team1.socnet.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import team1.socnet.controllers.helpers.MethodHelper;
import team1.socnet.models.User;
import team1.socnet.services.contracts.PostService;
import team1.socnet.services.contracts.UserService;

import javax.validation.Valid;

import java.security.Principal;

import static team1.socnet.controllers.helpers.ControllerConstant.USER;
import static team1.socnet.controllers.helpers.ControllerConstant.USER_LIST;


@Controller
@RequestMapping("/search")
public class SearchController {

    private static final String PUBLIC_POST_LIST = "publicPostList";
    private static final String SEARCH_RESULTS_HTML_PAGE = "search-results";
    private UserService userService;
    private PostService postService;

    @Autowired
    public SearchController(UserService userService, PostService postService) {
        this.userService = userService;
        this.postService = postService;
    }

    @PostMapping
    public String getAllByFilter(Model model, @RequestParam @Valid String filterParam, Authentication authentication, Principal principal) {
        if (principal != null) {
            User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
            model.addAttribute(USER, loggedUser);
            model.addAttribute(USER_LIST, userService.filterByUsernameAndEmailAndFirstNameAndLastName(filterParam));
            model.addAttribute(PUBLIC_POST_LIST, postService.filterAllPublicPostByPostMessageContaining(filterParam));
            return SEARCH_RESULTS_HTML_PAGE;
        } else {
            model.addAttribute(USER_LIST, userService.filterByUsernameAndEmailAndFirstNameAndLastName(filterParam));
            model.addAttribute(PUBLIC_POST_LIST, postService.filterAllPublicPostByPostMessageContaining(filterParam));
            return SEARCH_RESULTS_HTML_PAGE;
        }
    }
}
