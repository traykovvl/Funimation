package team1.socnet.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import team1.socnet.controllers.helpers.EmailDetailsSetUpHelper;
import team1.socnet.models.Email;
import team1.socnet.models.PasswordResetToken;
import team1.socnet.models.User;
import team1.socnet.models.dto.PasswordForgotDto;
import team1.socnet.services.contracts.EmailService;
import team1.socnet.services.contracts.PasswordResetTokenService;
import team1.socnet.services.contracts.UserService;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import static team1.socnet.controllers.helpers.ControllerConstant.*;

@Controller
@RequestMapping("/new-validation")
public class NewEmailValidationAccountController {
    private static final String ACCOUNT_BANNED_ERROR_MESSAGE = "Your account has been banned! " +
            "For further information please contact us on: funimationone@gmail.com";
    private static final String NEW_VALIDATION_HTML_PAGE = "new-validation";

    private UserService userService;
    private PasswordResetTokenService passwordResetTokenService;
    private EmailService emailService;

    @Autowired
    public NewEmailValidationAccountController(UserService userService,
                                               PasswordResetTokenService passwordResetTokenService,
                                               EmailService emailService) {
        this.userService = userService;
        this.passwordResetTokenService = passwordResetTokenService;
        this.emailService = emailService;
    }

    @ModelAttribute("validationRequestForm")
    public PasswordForgotDto validationDto() {
        return new PasswordForgotDto();
    }

    @GetMapping
    public String showValidationForm() {
        return NEW_VALIDATION_HTML_PAGE;
    }

    @PostMapping
    public String registerUserAccount(@ModelAttribute("validationRequestForm") @Valid PasswordForgotDto form,
                                      BindingResult result,
                                      HttpServletRequest request) {

        if (result.hasErrors()) {
            return NEW_VALIDATION_HTML_PAGE;
        }
        User existing = userService.findByEmail(form.getEmail());
        if (existing == null) {
            result.rejectValue(EMAIL, null, ACCOUNT_DOES_NOT_EXISTS);
            return NEW_VALIDATION_HTML_PAGE;
        }
        if (!existing.isValid() && existing.isBanned()) {
            result.rejectValue(EMAIL, null, ACCOUNT_BANNED_ERROR_MESSAGE);
            return NEW_VALIDATION_HTML_PAGE;
        }

        if (existing.isValid()) {
            result.rejectValue(EMAIL, null, ACCOUNT_EXISTS_ERROR_MESSAGE);
            return NEW_VALIDATION_HTML_PAGE;
        }

        PasswordResetToken token = EmailDetailsSetUpHelper.getPasswordResetToken(existing);
        passwordResetTokenService.save(token);

        Email mail = EmailDetailsSetUpHelper.getEmailDetails(existing,
                TEAM_1_SOCNET_EMAIL,
                FUNIMATION_COMPLETE_REGISTRATION_SUBJECT_EMAIL);

        EmailDetailsSetUpHelper.getEmailContentForm(request, existing, token, mail,
                CONFIRM_URL_MODEL_PATTERN,
                CONFIRM_ACCOUNT_TOKEN_URL);
        try {
            emailService.sendEmailForConfirmationOfRegistration(mail);
        } catch (MessagingException e) {
            e.printStackTrace();
            return "redirect:/new-validation?failure";
        }
        return "redirect:/new-validation?success";
    }
}
