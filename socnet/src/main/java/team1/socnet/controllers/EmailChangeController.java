package team1.socnet.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import team1.socnet.controllers.helpers.EmailDetailsSetUpHelper;
import team1.socnet.controllers.helpers.MethodHelper;
import team1.socnet.models.Email;
import team1.socnet.models.PasswordResetToken;
import team1.socnet.models.User;
import team1.socnet.models.dto.PasswordForgotDto;
import team1.socnet.services.contracts.EmailService;
import team1.socnet.services.contracts.PasswordResetTokenService;
import team1.socnet.services.contracts.UserService;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import static team1.socnet.controllers.helpers.ControllerConstant.*;

@Controller
@RequestMapping("/changeEmail")
public class EmailChangeController {
    private static final String EMAIL_ADDRESS_EXISTS_ERROR_MESSAGE = "email address exists";
    private static final String NEW_EMAIL_ADDRESS_HTML_PAGE = "new-email-address";

    private UserService userService;
    private PasswordResetTokenService passwordResetTokenService;
    private EmailService emailService;

    public EmailChangeController(UserService userService, PasswordResetTokenService passwordResetTokenService,
                                 EmailService emailService) {
        this.userService = userService;
        this.passwordResetTokenService = passwordResetTokenService;
        this.emailService = emailService;
    }

    @ModelAttribute("validationRequestForm")
    public PasswordForgotDto validationDto() {
        return new PasswordForgotDto();
    }

    @GetMapping
    public String showValidationForm() {
        return NEW_EMAIL_ADDRESS_HTML_PAGE;
    }

    @PostMapping
    public String registerUserAccount(@ModelAttribute("validationRequestForm") @Valid PasswordForgotDto form,
                                      BindingResult result,
                                      Authentication authentication,
                                      HttpServletRequest request) {
        User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
        User existing = userService.findByEmail(form.getEmail());
        if (existing != null) {
            result.rejectValue(EMAIL, null, EMAIL_ADDRESS_EXISTS_ERROR_MESSAGE);
            return NEW_EMAIL_ADDRESS_HTML_PAGE;
        }

        if (result.hasErrors()) {
            return NEW_EMAIL_ADDRESS_HTML_PAGE;
        }
        if (loggedUser == null) {
            result.rejectValue(EMAIL, null, "user is not logged in");
            return NEW_EMAIL_ADDRESS_HTML_PAGE;

        }

        userService.updateEmail(form.getEmail(), loggedUser);
        existing = loggedUser;

        PasswordResetToken token = EmailDetailsSetUpHelper.getPasswordResetToken(existing);
        passwordResetTokenService.save(token);

        Email mail = EmailDetailsSetUpHelper.getEmailDetails(existing,
                TEAM_1_SOCNET_EMAIL,
                FUNIMATION_COMPLETE_REGISTRATION_SUBJECT_EMAIL);

        EmailDetailsSetUpHelper.getEmailContentForm(request, existing, token, mail,
                CONFIRM_URL_MODEL_PATTERN,
                CONFIRM_ACCOUNT_TOKEN_URL);
        try {
            emailService.sendEmailForConfirmationOfRegistration(mail);

        } catch (MessagingException e) {
            e.printStackTrace();
            return "redirect:/changeEmail?failure";
        }
        authentication.setAuthenticated(false);
        return "redirect:/changeEmail?success";
    }
}
