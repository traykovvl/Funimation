package team1.socnet.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import static team1.socnet.controllers.helpers.ControllerConstant.ACCESS_DENIED_HTML_PAGE;
import static team1.socnet.controllers.helpers.ControllerConstant.LOGIN_HTML_PAGE;

@Controller
public class LoginController {

    @GetMapping("/login")
    public String showLogin() {
        return LOGIN_HTML_PAGE;
    }

    @GetMapping("/access-denied")
    public String showAccessDenied() {
        return ACCESS_DENIED_HTML_PAGE;
    }
}
