package team1.socnet.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import team1.socnet.controllers.helpers.MethodHelper;
import team1.socnet.models.City;
import team1.socnet.models.User;
import team1.socnet.services.CountryServiceImpl;
import team1.socnet.services.contracts.CityService;
import team1.socnet.services.contracts.UserService;

import javax.validation.Valid;

import static team1.socnet.controllers.helpers.ControllerConstant.*;
import static team1.socnet.controllers.helpers.MethodHelper.getErrorMessage;

@Controller
@RequestMapping("/cities")
public class CityController {

    private static final String CITY_TITLE_CREATE_ERROR_MASSAGE = "City name cannot be empty";
    private static final String CITIES_LIST = "citiesList";
    private static final String ALL_CITIES_HTML_PAGE = "all-cities";
    private static final String NEW_CITY_HTML_PAGE = "new-city";
    private static final String CITY_NAME = "cityName";
    private static final String UPDATE_CITY_HTML_PAGE = "update-city";

    private CityService cityService;
    private CountryServiceImpl countryService;
    private UserService userService;

    @Autowired
    public CityController(CityService cityService, CountryServiceImpl countryService, UserService userService) {
        this.cityService = cityService;
        this.countryService = countryService;
        this.userService = userService;
    }

    @GetMapping
    public String getAllCitiesPage(Model model, Authentication authentication) {
        User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
        model.addAttribute(USER, loggedUser);
        model.addAttribute(CITIES_LIST, cityService.findAll());
        return ALL_CITIES_HTML_PAGE;
    }

    @GetMapping("/new")
    public String getNewCityView(Model model, Authentication authentication) {
        User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
        model.addAttribute(USER, loggedUser);
        model.addAttribute(CITY, new City());
        model.addAttribute(COUNTRIES, countryService.findAll());
        return NEW_CITY_HTML_PAGE;
    }

    @PostMapping("/new")
    public String newCity(@Valid @ModelAttribute City city,
                          BindingResult bindingResult, Model model, Authentication authentication) {
        User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
        model.addAttribute(USER, loggedUser);

        if (city.getCityName().isEmpty()) {
            bindingResult.rejectValue(CITY_NAME, null, CITY_TITLE_CREATE_ERROR_MASSAGE);
        }

        if (bindingResult.hasErrors()) {
            model.addAttribute(CITY, city);
            model.addAttribute(COUNTRIES, countryService.findAll());
            return NEW_CITY_HTML_PAGE;
        }

        city.setCountry(city.getCountry());

        try {
            cityService.insertCity(city);
        } catch (IllegalArgumentException ex) {
            return getErrorMessage(model, ex);
        }
        return "redirect:/cities";
    }

    @GetMapping("/update/{id}")
    public String showCityForm(Model model, @PathVariable("id") long id, Authentication authentication) {
        try {
            User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
            City city = cityService.getById(id);
            model.addAttribute(USER, loggedUser);
            model.addAttribute(CITY, city);
            model.addAttribute(COUNTRIES, countryService.findAll());
        } catch (IllegalArgumentException ex) {
            return getErrorMessage(model, ex);
        }

        return UPDATE_CITY_HTML_PAGE;
    }

    @PostMapping("/update/{id}")
    public String updateCity(Model model, @PathVariable("id") long id, @Valid City city, Authentication authentication,
                             BindingResult bindingResult) {
        User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
        model.addAttribute(USER, loggedUser);

        if (city.getCityName().isEmpty()) {
            bindingResult.rejectValue(CITY_NAME, null, CITY_TITLE_CREATE_ERROR_MASSAGE);
        }

        if (bindingResult.hasErrors()) {
            model.addAttribute(CITY, city);
            model.addAttribute(COUNTRIES, countryService.findAll());
            return UPDATE_CITY_HTML_PAGE;
        }

        cityService.UpdateCity(city);
        return "redirect:/cities";
    }

    @GetMapping("/delete/{id}")
    public String deleteCity(Model model, @PathVariable("id") long id) {
        City city = cityService.getById(id);
        model.addAttribute(CITY, city);
        try {
            cityService.deleteCity(city);
        } catch (IllegalArgumentException ex) {
            return getErrorMessage(model, ex);
        }
        return "redirect:/cities";
    }
}
