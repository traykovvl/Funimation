package team1.socnet.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import team1.socnet.controllers.helpers.EmailDetailsSetUpHelper;
import team1.socnet.models.Email;
import team1.socnet.models.PasswordResetToken;
import team1.socnet.models.User;
import team1.socnet.services.contracts.EmailService;
import team1.socnet.services.contracts.PasswordResetTokenService;
import team1.socnet.services.contracts.UserService;
import team1.socnet.models.dto.UserRegistrationDto;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import static team1.socnet.controllers.helpers.ControllerConstant.*;

@Controller
@RequestMapping("/registration")
public class UserRegistrationController {


    public static final String REGISTRATION_HTML_PAGE = "registration";
    private UserService userService;
    private PasswordResetTokenService passwordResetTokenService;
    private EmailService emailService;

    @Autowired
    public UserRegistrationController(UserService userService,
                                      PasswordResetTokenService passwordResetTokenService,
                                      EmailService emailService) {
        this.userService = userService;
        this.passwordResetTokenService = passwordResetTokenService;
        this.emailService = emailService;
    }

    @ModelAttribute(USER)
    public UserRegistrationDto userRegistrationDto() {
        return new UserRegistrationDto();
    }

    @GetMapping
    public String showRegistrationForm(Model model) {
        return REGISTRATION_HTML_PAGE;
    }

    @PostMapping
    public String registerUserAccount(@ModelAttribute(USER) @Valid UserRegistrationDto userDto,
                                      BindingResult result, HttpServletRequest request){

        User existing = getUser(userDto);
        if (existing != null){
            result.rejectValue(EMAIL, null, ACCOUNT_EXISTS_ERROR_MESSAGE);
        }

        if (!userDto.getEmail().equals(userDto.getConfirmEmail())){
            result.rejectValue(EMAIL, null, ACCOUNT_EXISTS_ERROR_MESSAGE);
        }

        if (result.hasErrors()){
            return REGISTRATION_HTML_PAGE;
        }

        userService.save(userDto);
        User createdUser = getUser(userDto);
        PasswordResetToken token = EmailDetailsSetUpHelper.getPasswordResetToken(createdUser);
        passwordResetTokenService.save(token);

        Email mail = EmailDetailsSetUpHelper.getEmailDetails(createdUser,
                "no-reply@team1.socnet.com",
                "Funimation - Complete Registration!");

        EmailDetailsSetUpHelper.getEmailContentForm(request, createdUser, token, mail,
                                                    "confirmUrl",
                                                    "/confirm-account?token=");
        try {
            emailService.sendEmailForConfirmationOfRegistration(mail);
        } catch (MessagingException e) {
            e.printStackTrace();
            return "redirect:/registration?failure";
        }
        return "redirect:/registration?success";
    }

    private User getUser(@ModelAttribute(USER) @Valid UserRegistrationDto userDto) {
        return userService.findByEmail(userDto.getEmail());
    }
}
