package team1.socnet.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import team1.socnet.controllers.helpers.MethodHelper;
import team1.socnet.models.Comment;
import team1.socnet.models.User;
import team1.socnet.services.contracts.CommentService;
import team1.socnet.services.contracts.FriendshipService;
import team1.socnet.services.contracts.ReplyCommentService;
import team1.socnet.services.contracts.UserService;

import javax.validation.Valid;

import static team1.socnet.controllers.helpers.ControllerConstant.*;
import static team1.socnet.controllers.helpers.MethodHelper.getErrorMessage;

@Controller
@RequestMapping("/replyComments")
public class ReplyCommentController {
    private static final String REPLY = "reply";
    private static final String COMMENT_ID = "commentId";
    private static final String ALL_REPLY_COMMENTS = "allReplyComments";
    private static final String REPLY_CREATE_ERROR_MASSAGE = "Reply field cannot be empty";
    private static final String SHOW_ALL_REPLIES_HTML_PAGE = "show-all-replies";
    private static final String SHOW_ALL_REPLIES_TO_COMMENT_HTML_PAGE = "show-all-replies-to-comment";
    private static final String CREATE_REPLY_HTML_PAGE = "create-reply";
    private static final String REPLY_COMMENTS_BY_COMMENT_URL = "redirect:/replyComments/byComment/{commentId}";
    private static final String UPDATE_REPLY_HTML_PAGE = "update-reply";
    private static final String COMMENT_BODY = "commentBody";

    private ReplyCommentService replyCommentService;
    private UserService userService;
    private CommentService commentService;
    private FriendshipService friendShipService;

    @Autowired
    public ReplyCommentController(ReplyCommentService replyCommentService, UserService userService,
                                  CommentService commentService,
                                  FriendshipService friendShipService) {
        this.replyCommentService = replyCommentService;
        this.userService = userService;
        this.commentService = commentService;
        this.friendShipService = friendShipService;
    }

    @GetMapping
    public String getAllReplyComments(Model model, Authentication authentication) {
        User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
        model.addAttribute(USER, loggedUser);
        model.addAttribute(ALL_REPLY_COMMENTS, replyCommentService.findAll());
        return SHOW_ALL_REPLIES_HTML_PAGE;
    }

    @GetMapping("/byComment/{commentId}")
    public String getReplyByCommentView(Model model, @PathVariable(COMMENT_ID) long commentId, Authentication authentication) {
        try {
            Comment comment = commentService.findById(commentId);
            User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
            model.addAttribute(USER, loggedUser);
            long postAuthorId = comment.getPost().getAuthor().getUserId();
            model.addAttribute(COMMENT, comment);
            model.addAttribute(FRIENDS,friendShipService.areFriends(postAuthorId,loggedUser));
            model.addAttribute(ALL_REPLY_COMMENTS, replyCommentService.findAllRepliesToComment(comment));

        } catch (IllegalArgumentException ex) {
            return getErrorMessage(model, ex);
        }
        return SHOW_ALL_REPLIES_TO_COMMENT_HTML_PAGE;
    }

    @GetMapping("/new/{commentId}")
    public String getNewReplyView(Model model, @PathVariable(COMMENT_ID) long commentId, Authentication authentication) {
        try {
            Comment comment = commentService.findById(commentId);
            User loggedUser = MethodHelper.getLoggedUser(authentication, userService);
            model.addAttribute(USER, loggedUser);
            model.addAttribute(REPLY, new Comment());
            model.addAttribute(COMMENT,comment);
        } catch (IllegalArgumentException ex) {
            return getErrorMessage(model, ex);
        }

        return CREATE_REPLY_HTML_PAGE;
    }

    @PostMapping("/new/{commentId}")
    public String newReply(Model model, @Valid @ModelAttribute Comment reply, @PathVariable(COMMENT_ID) long commentId,
                           BindingResult bindingResult, Authentication authentication) {
        Comment comment = commentService.findById(commentId);
        User author = MethodHelper.getLoggedUser(authentication, userService);
        model.addAttribute(USER, author);

        if (reply.getCommentBody().isEmpty()) {
            bindingResult.rejectValue(COMMENT_BODY, "", REPLY_CREATE_ERROR_MASSAGE);
        }

        if (bindingResult.hasErrors()) {
            model.addAttribute(COMMENT, comment);
            model.addAttribute(REPLY, reply);
            return CREATE_REPLY_HTML_PAGE;
        }
        replyCommentService.saveReply(reply, author, comment);
        return REPLY_COMMENTS_BY_COMMENT_URL;
    }

    @GetMapping("/update/{commentId}")
    public String showReplyForm(Model model,
                                @PathVariable(COMMENT_ID)long commentId,
                                Authentication authentication) {
        try {
            User author = MethodHelper.getLoggedUser(authentication, userService);
            model.addAttribute(USER, author);
            Comment comment = commentService.findById(commentId);
            model.addAttribute(REPLY, comment);
        } catch (IllegalArgumentException ex) {
            return getErrorMessage(model, ex);
        }
        return UPDATE_REPLY_HTML_PAGE;
    }

    @PostMapping("/update/{commentId}")
    public String updateComment(Model model,
                                @PathVariable(COMMENT_ID)long commentId,
                                @Valid Comment reply,
                                Authentication authentication,
                                BindingResult bindingResult) {
        User author = MethodHelper.getLoggedUser(authentication, userService);
        model.addAttribute(USER, author);
        if (reply.getCommentBody().isEmpty()) {
            bindingResult.rejectValue(COMMENT_BODY, null, REPLY_CREATE_ERROR_MASSAGE);
        }

        if (bindingResult.hasErrors()) {
            model.addAttribute(REPLY, reply);
            return UPDATE_REPLY_HTML_PAGE;
        }

        replyCommentService.updateReply(reply);
        return REPLY_COMMENTS_BY_COMMENT_URL;
    }

    @GetMapping("/delete/{commentId}")
    public String deleteComment(Model model,  @PathVariable(COMMENT_ID)long commentId) {
        try {
            replyCommentService.deleteReply(commentId);
        } catch (IllegalArgumentException ex) {
            return getErrorMessage(model, ex);
        }
        return REPLY_COMMENTS_BY_COMMENT_URL;
    }
}

