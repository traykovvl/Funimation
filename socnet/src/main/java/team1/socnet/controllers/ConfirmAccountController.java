package team1.socnet.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import team1.socnet.models.PasswordResetToken;
import team1.socnet.models.User;
import team1.socnet.services.contracts.PasswordResetTokenService;
import team1.socnet.services.contracts.UserService;

import static team1.socnet.controllers.helpers.ControllerConstant.ERROR;

@Controller
@RequestMapping("/confirm-account")
public class ConfirmAccountController {
    private static final String ACCOUNT_VERIFIED_HTML_PAGE = "account-verified";
    private static final String MESSAGE = "message";
    private static final String LINK_ERROR_MESSAGE = "The link is invalid or broken!";

    private UserService userService;
    private PasswordResetTokenService passwordResetTokenService;

    public ConfirmAccountController(UserService userService, PasswordResetTokenService passwordResetTokenService) {
        this.userService = userService;
        this.passwordResetTokenService = passwordResetTokenService;
    }

    @RequestMapping()
    public ModelAndView confirmUserAccount(ModelAndView modelAndView, @RequestParam(required = false) String token) {
        PasswordResetToken confirmToken = passwordResetTokenService.findByToken(token);

        if (token != null) {
            User user = userService.findByEmail(confirmToken.getUser().getEmail());
            userService.updateUserRegistrationStatus(user);
            passwordResetTokenService.delete(confirmToken);
            modelAndView.setViewName(ACCOUNT_VERIFIED_HTML_PAGE);
        } else {
            modelAndView.addObject(MESSAGE, LINK_ERROR_MESSAGE);
            modelAndView.setViewName(ERROR);
        }
        return modelAndView;
    }
}
