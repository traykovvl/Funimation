package team1.socnet.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import team1.socnet.services.contracts.UserService;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserService userService;

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers(
                        "/registration**",
                        "/forgot-password**",
                        "/reset-password**",
                        "/new-validation",
                        "/login**",
                        "/",
                        "/confirm**",
                        "/api/posts*/**",
                        "/api/comments/byPost*/**",
                        "/api/replyComments/byComment*/**",
                        "/posts",
                        "/terms**",
                        "/js/**",
                        "/css/**",
                        "/pictures/**",
                        "/v2/api-docs",
                        "/configuration/**",
                        "/swagger*/**",
                        "/webjars/**",
                        "/posts/requestOfferForAdult",
                        "/posts/requestOfferForKids",
                        "/search*/**")
                .permitAll()
//TODO                .antMatchers("/api/**")
//                .permitAll()
                .antMatchers(
                        "/posts/admin",
                        "/users",
                        "/api/comments/admin**",
                        "/api/replyComments/admin**",
                        "/comments",
                        "/replyComments"
                       )
                .hasRole("ADMIN")
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .and().csrf().disable()
                .logout()
                .invalidateHttpSession(true)
                .clearAuthentication(true)
                .logoutRequestMatcher(new AntPathRequestMatcher(
                        "/logout"))
                .logoutSuccessUrl("/")
                .permitAll()
                .and()
                .httpBasic()
                .and()
                .exceptionHandling()
                .accessDeniedPage("/access-denied");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}