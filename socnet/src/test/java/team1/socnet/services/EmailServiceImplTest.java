package team1.socnet.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mail.javamail.JavaMailSender;
import org.thymeleaf.ITemplateEngine;
import team1.socnet.models.Email;
import team1.socnet.services.EmailServiceImpl;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@RunWith(MockitoJUnitRunner.class)
public class EmailServiceImplTest {

    @Mock
    JavaMailSender emailSender;

    @Mock
    ITemplateEngine templateEngine;

    @InjectMocks
    EmailServiceImpl emailService;


    @Test(expected = NullPointerException.class)
    public void sendEmailForResetOfPassword_should_return_error_message_when_not_valid() throws MessagingException {
        Email email = new Email();
        emailService.sendEmailForResetOfPassword(email);
    }

    @Test(expected = NullPointerException.class)
    public void sendEmailForConfirmationOfRegistration_should_return_error_message_when_not_valid() throws MessagingException {
        Email email = new Email();
        emailService.sendEmailForConfirmationOfRegistration(email);
    }

    @Test
    public void sendEmail_should_send_email_to_user_requesting_reset_of_password() throws MessagingException {
        Mockito.when(emailSender.createMimeMessage()).thenReturn(Mockito.mock(MimeMessage.class));
        Mockito.when(templateEngine.process(Mockito.any(String.class), Mockito.any())).thenReturn("");
        Email mail = new Email();
        mail.setFrom("no-reply@team1.socnet.com");
        mail.setTo("user@email.com");
        mail.setSubject("Password reset request");
        emailService.sendEmailForResetOfPassword(mail);
        Mockito.verify(emailSender, Mockito.times(1)).createMimeMessage();
        Mockito.verify(templateEngine, Mockito.times(1)).process(Mockito.anyString(), Mockito.any());
        Mockito.verify(emailSender, Mockito.times(1)).send(Mockito.any(MimeMessage.class));
    }

    @Test
    public void sendEmail_should_send_email_to_user_confirming_the_registration() throws MessagingException {
        Mockito.when(emailSender.createMimeMessage()).thenReturn(Mockito.mock(MimeMessage.class));
        Mockito.when(templateEngine.process(Mockito.any(String.class), Mockito.any())).thenReturn("");
        Email mail = new Email();
        mail.setFrom("no-reply@team1.socnet.com");
        mail.setTo("user@email.com");
        mail.setSubject("Password confirmation");
        emailService.sendEmailForConfirmationOfRegistration(mail);
        Mockito.verify(emailSender, Mockito.times(1)).createMimeMessage();
        Mockito.verify(templateEngine, Mockito.times(1)).process(Mockito.anyString(), Mockito.any());
        Mockito.verify(emailSender, Mockito.times(1)).send(Mockito.any(MimeMessage.class));
    }
}
