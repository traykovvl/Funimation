package team1.socnet.services;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import team1.socnet.models.Authority;
import team1.socnet.models.Comment;
import team1.socnet.models.Post;
import team1.socnet.models.User;
import team1.socnet.repositories.CommentRepository;

import java.util.*;

import static org.mockito.Mockito.*;

public class CommentServiceImplTest {
    @Mock
    CommentRepository commentRepository;
    @InjectMocks
    CommentServiceImpl commentServiceImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findAll_should_return_list_of_comments() {
        Comment comment = new Comment();
        List<Comment> commentList = new ArrayList<>();
        commentList.add(comment);
        Mockito.when(commentRepository.findAll()).thenReturn(commentList);
        List<Comment> result = commentServiceImpl.findAll();
        Assert.assertEquals(commentList, result);
    }

    @Test
    public void saveComment_should_save_comment_when_valid(){
        Comment comment = new Comment();
        Mockito.when(commentRepository.save(comment)).thenReturn(comment);
        Comment result = commentServiceImpl.saveComment(comment, new User("firstName", "lastName", "email", "password", new HashSet<Authority>(Arrays.asList(new Authority())), true), new Post("title", "postMessage", new User("firstName", "lastName", "email", "password", new HashSet<Authority>(Arrays.asList(new Authority())), true)));
        Assert.assertEquals(comment, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findById_should_return_error_message_when_not_exist() {
        commentServiceImpl.findById(0L);
    }

    @Test
    public void testFindById_should_return_comment_when_valid(){
        Comment comment = new Comment();
        Mockito.when(commentRepository.findById(0L)).thenReturn(Optional.of(comment));
        Mockito.when(commentRepository.getOne(0L)).thenReturn(comment);
        Comment result = commentServiceImpl.findById(0L);
        Assert.assertEquals(comment, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateComment_should_return_error_message_when_not_exists(){
        Comment result = commentServiceImpl.updateComment(new Comment());
    }

    @Test
    public void updateComment_should_return_comment_when_valid(){
        Comment comment = new Comment();
        Mockito.when(commentRepository.save(comment)).thenReturn(comment);
        Mockito.when(commentRepository.findById(comment.getCommentId())).thenReturn(Optional.of(comment));
        Mockito.when(commentRepository.getOne(comment.getCommentId())).thenReturn(comment);
        comment.setCommentBody("new");
        comment.setTimeStamp(new Date());
        Comment result = commentServiceImpl.updateComment(comment);
        Assert.assertEquals(comment, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteComment_should_return_error_message_when_not_exist() {
        commentServiceImpl.deleteComment(0L);
    }

    @Test
    public void deleteComment_should_delete_when_valid(){
        Comment comment = new Comment();
        Mockito.when(commentRepository.findById(comment.getCommentId())).thenReturn(Optional.of(comment));
        commentServiceImpl.deleteComment(0L);
        Mockito.verify(commentRepository, Mockito.times(1)).deleteById(0L);
    }

    @Test
    public void testFindAllByPost() throws Exception {
        Comment comment = new Comment();
        when(commentRepository.findAllByPostAndCommentToReplyIsNullOrderByTimeStampAsc(any())).thenReturn(Arrays.<Comment>asList(comment));

        List<Comment> result = commentServiceImpl.findAllByPost(new Post("title", "postMessage", new User("firstName", "lastName", "email", "password", new HashSet<Authority>(Arrays.asList(new Authority())), true)));
        Assert.assertEquals(Arrays.<Comment>asList(comment), result);
    }
}