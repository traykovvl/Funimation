package team1.socnet.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import team1.socnet.models.Country;
import team1.socnet.repositories.CountryRepository;
import team1.socnet.services.CountryServiceImpl;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class CountryServiceImplTest {
    @Mock
    CountryRepository mockCountryRepository;
    @InjectMocks
    CountryServiceImpl countryService;

    @Test
    public void findAll_should_return_list_of_countries(){
        Country country1 = new Country();
        Country country2 = new Country();
        List<Country> countryList = new ArrayList<>();
        countryList.add(country1);
        countryList.add(country2);
        Mockito.when(mockCountryRepository.findAll()).thenReturn(countryList);
        List<Country> result = countryService.findAll();
        Assert.assertEquals(countryList.get(0),result.get(0));
    }
}
