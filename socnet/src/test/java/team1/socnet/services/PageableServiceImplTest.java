package team1.socnet.services;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import team1.socnet.models.Authority;
import team1.socnet.models.Comment;
import team1.socnet.models.Post;
import team1.socnet.models.User;
import team1.socnet.repositories.CommentRepository;
import team1.socnet.repositories.PostRepository;
import team1.socnet.services.contracts.FriendshipService;
import team1.socnet.services.contracts.UserService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.mockito.Mockito.*;

public class PageableServiceImplTest {
    @Mock
    PostRepository postRepository;
    @Mock
    UserService userService;
    @Mock
    CommentRepository commentRepository;
    @Mock
    Page page;

    @Mock
    FriendshipService friendShipService;
    @InjectMocks
    PageableServiceImpl pageableServiceImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findAllByPages_should_return_list_of_posts() {
        Post post = new Post("title", "postMessage", new User("firstName", "lastName", "email", "password", new HashSet<Authority>(Arrays.asList(new Authority())), true));
        when(postRepository.findAllByOrderByTimeStampDesc(any())).thenReturn(Arrays.<Post>asList(post));

        List<Post> result = pageableServiceImpl.findAllByPages(0, 1);
        Assert.assertEquals(Arrays.<Post>asList(post), result);
    }

    @Test
    public void findAllAuthorsPosts_should_return_list_of_authors_posts(){
        User user = new User("firstName", "lastName", "email", "password", new HashSet<Authority>(Arrays.asList(new Authority())), true);
        Post post = new Post("title", "postMessage", user);

        when(postRepository.findByAuthorOrderByTimeStampDesc(any(), any())).thenReturn(Arrays.<Post>asList(post));
        when(userService.findByUserId(anyLong())).thenReturn(user);

        List<Post> result = pageableServiceImpl.findAllAuthorsPosts(0L, 0, 1);
        Assert.assertEquals(Arrays.<Post>asList(post),result);
    }

    @Test
    public void findAllAuthorsPublicPosts_should_return_authors_public_posts(){
        User user = new User("firstName", "lastName", "email", "password", new HashSet<Authority>(Arrays.asList(new Authority())), true);
        Post post = new Post("title", "postMessage", user);

        when(postRepository.findByAuthorAndPrivacy_PrivacyTypeOrderByTimeStampDesc(any(), anyString(), any())).thenReturn(Arrays.<Post>asList(post));
        when(userService.findByUserId(anyLong())).thenReturn(user);

        List<Post> result = pageableServiceImpl.findAllAuthorsPublicPosts(0L, 0, 1);
        Assert.assertEquals(Arrays.<Post>asList(post), result);
    }

    @Test
    public void findAllRequestOfferForKids_should_return_list_of_post_request_for_offer_for_kids() {
        User user = new User("firstName", "lastName", "email", "password", new HashSet<Authority>(Arrays.asList(new Authority())), true);
        Post post = new Post("title", "postMessage", user);

        when(postRepository.findAllByTitleOrderByTimeStampDesc(anyString(), any())).thenReturn(Arrays.<Post>asList(post));

        List<Post> result = pageableServiceImpl.findAllRequestOfferForKids(0, 1);
        Assert.assertEquals(Arrays.<Post>asList(post), result);
    }

    @Test
    public void findAllRequestOfferForAdults_should_return_list_of_posts_request_for_offer_for_adult() {
        User user = new User("firstName", "lastName", "email", "password", new HashSet<Authority>(Arrays.asList(new Authority())), true);
        Post post = new Post("title", "postMessage", user);
        when(postRepository.findAllByTitleOrderByTimeStampDesc(anyString(), any())).thenReturn(Arrays.<Post>asList(post));

        List<Post> result = pageableServiceImpl.findAllRequestOfferForAdults(0, 1);
        Assert.assertEquals(Arrays.<Post>asList(post), result);
    }

    @Test
    public void findPublicPost_return_list_of_public_posts() {
        User user = new User("firstName", "lastName", "email", "password", new HashSet<Authority>(Arrays.asList(new Authority())), true);
        Post post = new Post("title", "postMessage", user);
        when(postRepository.findAllByPrivacy_PrivacyTypeAndTitleIsNotContainingOrderByTimeStampDesc(anyString(), anyString(), any())).thenReturn(Arrays.<Post>asList(post));

        List<Post> result = pageableServiceImpl.findPublicPost(0, 1);
        Assert.assertEquals(Arrays.<Post>asList(post), result);
    }

    @Test
    public void findAllCommentsByPage_should_return_list_of_comments() {
        Comment comment = new Comment();
        int startPage= 0;
        int perPage = 1;
        List<Comment> commentList = new ArrayList<>();
        commentList.add(comment);
        Pageable pageable = PageRequest.of(startPage, perPage);
        Page<Comment> commentPage = new PageImpl<>(commentList,pageable,commentList.size());
        when(commentRepository.findAll(pageable)).thenReturn(commentPage);

        List<Comment> result = pageableServiceImpl.findAllCommentsByPage(0, 1);
        Assert.assertEquals(Arrays.<Comment>asList(comment), result);
    }

    @Test
    public void findAllCommentsByPostByPage_return_list_of_comments() {
        User user = new User("firstName", "lastName", "email", "password", new HashSet<Authority>(Arrays.asList(new Authority())), true);
        Post post = new Post("title", "postMessage", user);
        Comment comment = new Comment();
        int startPage= 0;
        int perPage = 1;
        List<Comment> commentList = new ArrayList<>();
        commentList.add(comment);
        Pageable pageable = PageRequest.of(startPage, perPage);
        Page<Comment> commentPage = new PageImpl<>(commentList,pageable,commentList.size());
        when(commentRepository.findAllByPostAndCommentToReplyIsNullOrderByTimeStampAsc(any(), any())).thenReturn(commentPage);

        List<Comment> result = pageableServiceImpl.findAllCommentsByPostByPage(post, 0, 1);
        Assert.assertEquals(Arrays.<Comment>asList(comment), result);
    }

    @Test
    public void findAllRepliesToCommentByPage_should_return_list_of_replies_to_comment() {
        Comment comment = new Comment();
        List<Comment> replyList = new ArrayList<>();
        replyList.add(comment);
        int startPage= 0;
        int perPage = 1;
        Pageable pageable = PageRequest.of(startPage, perPage);
        Page<Comment> replyPage = new PageImpl<>(replyList,pageable,replyList.size());
        when(commentRepository.findAllByCommentToReplyIsNotNullOrderByTimeStampAsc( any())).thenReturn(replyPage);

        List<Comment> result = pageableServiceImpl.findAllRepliesToCommentByPage(0, 1);
        Assert.assertEquals(Arrays.<Comment>asList(comment), result);
    }

    @Test
    public void findAllRepliesToCommentByCommentByPage_should_return_list_of_replies_to_comment(){
       Comment comment = new Comment();
       List<Comment> replyList = new ArrayList<>();
       replyList.add(comment);
        int startPage= 0;
        int perPage = 1;
        Pageable pageable = PageRequest.of(startPage, perPage);
        Page<Comment> replyPage = new PageImpl<>(replyList,pageable,replyList.size());
        when(commentRepository.findAllByCommentToReplyEqualsOrderByTimeStampAsc(any(), any())).thenReturn(replyPage);

        List<Comment> result = pageableServiceImpl.findAllRepliesToCommentByCommentByPage(comment, 0, 1);
        Assert.assertEquals(Arrays.<Comment>asList(comment), result);
    }

    @Test
    public void allFriendsPosts_should_return_authors_and_friends_posts() {
        User user = new User("firstName", "lastName", "email", "password", new HashSet<Authority>(Arrays.asList(new Authority())), true);
        Post post = new Post("title", "postMessage", user);
        int startPage= 0;
        int perPage = 1;
        Pageable pageable = PageRequest.of(startPage, perPage);
        when(postRepository.generateFeedByDate(user.getUserId(),2, pageable)).thenReturn(Arrays.<Post>asList(post));

        List<Post> result = pageableServiceImpl.allFriendsPosts(user, 0, 1);
        Assert.assertEquals(Arrays.<Post>asList(post),result);
    }
}
