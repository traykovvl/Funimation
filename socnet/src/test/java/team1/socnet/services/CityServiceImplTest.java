package team1.socnet.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import team1.socnet.models.City;
import team1.socnet.repositories.CityRepository;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class CityServiceImplTest {
    @Mock
    CityRepository mockCityRepository;
    @InjectMocks
    CityServiceImpl cityService;

    @Test
    public void findAll_should_return_list_of_cities(){
        City city1 = new City();
        City city2 = new City();
        List<City> cityList = new ArrayList<>();
        cityList.add(city1);
        cityList.add(city2);
        Mockito.when(mockCityRepository.findAll()).thenReturn(cityList);
        List<City> result = cityService.findAll();
        Assert.assertEquals(cityList.get(0),result.get(0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getById_should_return_error_message_when_not_exists(){
        cityService.getById(0);
    }

    @Test
    public void getById_should_return_city_when_valid(){
        City city1 = new City();
        long cityId = city1.getCityId();
        Mockito.when(mockCityRepository.getOne(cityId)).thenReturn(city1);
        City result = cityService.getById(cityId);
        Assert.assertEquals(city1.getCityId(),result.getCityId());
    }
    @Test(expected = IllegalArgumentException.class)
    public void findByCityName_should_return_error_message_when_not_exists(){
        cityService.findByCityName("kfdsj");
    }
    @Test
    public void findByCityName_should_return_city_when_valid(){
        City city1 = new City();
        city1.setCityName("kdjfsd");
        Mockito.when(mockCityRepository.findByCityName("kdjfsd")).thenReturn(city1);
        City result = cityService.findByCityName("kdjfsd");
        Assert.assertEquals("kdjfsd",result.getCityName());
    }

    @Test
    public void insertCity_should_create_city_when_valid(){
        City city1 = new City();
        cityService.insertCity(city1);
        Mockito.verify(mockCityRepository, Mockito.times(1)).save(city1);
    }
    @Test(expected = IllegalArgumentException.class)
    public void insertCity_should_return_error_message_when_exists(){
        City city1 = new City();
        city1.setCityName("testName");
        Mockito.when(mockCityRepository.findByCityName("testName")).thenReturn(city1);
        cityService.insertCity(city1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteCity_should_return_error_message_when_not_exists(){
        City city1 = new City();
        cityService.deleteCity(city1);
    }
    @Test
    public void deleteCity_should_delete_entity_when_valid(){
        City city1 = new City();
        Mockito.when(mockCityRepository.getOne(city1.getCityId())).thenReturn(city1);
        cityService.deleteCity(city1);
        Mockito.verify(mockCityRepository, Mockito.times(1)).delete(city1);
    }
    @Test
    public void updateCity_should_amend_city(){
        City city1 = new City();
        cityService.UpdateCity(city1);
        Mockito.verify(mockCityRepository, Mockito.times(1)).save(city1);
    }
}
