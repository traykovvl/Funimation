package team1.socnet.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import team1.socnet.models.PasswordResetToken;
import team1.socnet.repositories.PasswordResetTokenRepository;
import team1.socnet.services.PasswordResetTokenServiceImpl;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class PasswordResetTokenServiceImplTest {

    @Mock
    PasswordResetTokenRepository mockPasswordResetTokenRepository;

    @InjectMocks
    PasswordResetTokenServiceImpl passwordResetTokenService;

    @Test
    public void save_should_create_new_token_when_valid() {
        PasswordResetToken token = new PasswordResetToken();
        passwordResetTokenService.save(token);

        verify(mockPasswordResetTokenRepository, times(1)).save(token);
    }

    @Test
    public void findByToken_should_return_token_when_valid() {
        PasswordResetToken token = new PasswordResetToken();
        Mockito.when(mockPasswordResetTokenRepository.findByToken(token.toString())).thenReturn(token);
        PasswordResetToken result = passwordResetTokenService.findByToken(token.toString());
        Assert.assertEquals(token,result);
    }

    @Test
    public void delete_should_delete_token_when_valid() {
        PasswordResetToken token = new PasswordResetToken();
        passwordResetTokenService.delete(token);
        verify(mockPasswordResetTokenRepository, times(1)).delete(token);
    }
}
