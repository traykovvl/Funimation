package team1.socnet.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.multipart.MultipartFile;
import team1.socnet.models.*;
import team1.socnet.repositories.*;
import team1.socnet.services.contracts.OccupationService;
import team1.socnet.models.dto.UserRegistrationDto;
import team1.socnet.models.dto.UserUpdateDto;

import java.io.IOException;
import java.util.*;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {
    @Mock
    UserRepository userRepository;

    @Mock
    PasswordEncoder passwordEncoder;

    @Mock
    AuthorityRepository authorityRepository;

    @Mock
    CityRepository cityRepository;
    @Mock
    PhotoRepository photoRepository;
    @Mock
    OccupationService occupationService;
    @Mock
    PrivacyRepository privacyRepository;

    @InjectMocks
    UserServiceImpl userService;

    @Test
    public void findByEmail_should_return_user_when_valid() {
        User user = new User();
        user.setEmail("test@gmail.com");
        Mockito.when(userRepository.findByEmail(user.getEmail())).thenReturn(user);
        User result = userService.findByEmail("test@gmail.com");

        Assert.assertEquals(user.getEmail(), result.getEmail());
    }

    @Test
    public void findByUserId_should_return_user_when_valid() {
        User user = new User();
        user.setUserId(1);
        Mockito.when(userRepository.findByUserId(user.getUserId())).thenReturn(user);
        User result = userService.findByUserId(1);

        Assert.assertEquals(user.getUserId(), result.getUserId());
    }

    @Test(expected = UsernameNotFoundException.class)
    public void loadUserByName_should_return_errorMessage_when_not_valid() {
        userService.loadUserByUsername("test");
    }

    @Test
    public void loadUserByName_should_return_userDetails_when_valid() {
        User user = new User();
        user.setEmail("test@gmail.com");
        user.setPassword("password");
        Authority auth = new Authority();
        auth.setAuthorityName("ROLE_USER");
        Set<Authority> set = new HashSet<>();
        set.add(auth);
        user.setAuthorities(set);
        user.setValid(true);
        Mockito.when(userRepository.findByEmail("test@gmail.com")).thenReturn(user);
        UserDetails result = userService.loadUserByUsername("test@gmail.com");
        Assert.assertEquals(result.getUsername(), user.getEmail());
    }

    @Test
    public void delete_should_set_users_valid_to_false() {
        Mockito.when(userRepository.findByUserId(Mockito.anyLong())).thenReturn(Mockito.mock(User.class));
        Mockito.when(userRepository.save(Mockito.any(User.class))).thenReturn(Mockito.mock(User.class));
        User user = new User();
        user.setUserId(1L);
        user.setValid(false);
        User loggedUser = new User();
        loggedUser.setUserId(2L);
        userService.delete(user.getUserId(),loggedUser.getUserId());
        Mockito.verify(userRepository, Mockito.times(1)).findByUserId(Mockito.anyLong());
        Mockito.verify(userRepository, Mockito.times(1)).save(Mockito.any(User.class));
    }

    @Test
    public void save_should_return_new_user_when_valid() {
        UserRegistrationDto registration = new UserRegistrationDto();
        registration.setFirstName("firstName");
        registration.setLastName("lastName");
        registration.setEmail("test@gmail.com");
        registration.setPassword("pass");
        User user = new User();
        user.setFirstName(registration.getFirstName());
        user.setLastName(registration.getLastName());
        user.setEmail(registration.getEmail());
        user.setPassword(passwordEncoder.encode(registration.getPassword()));
        Authority auth = new Authority();
        auth.setAuthorityName("ROLE_USER");
        Mockito.when(authorityRepository.getByAuthorityName("ROLE_USER")).thenReturn(auth);
        Set<Authority> set = new HashSet<>();
        set.add(auth);
        user.setAuthorities(set);
        user.setValid(false);
        Privacy privacy = new Privacy();
        Photo photo = new Photo();
        Mockito.when(privacyRepository.getOne(2L)).thenReturn(privacy);
        Mockito.when(photoRepository.getOne(1L)).thenReturn(photo);
        user.setPhoto(photo);
        user.getPhoto().setPrivacy(privacy);
        user.setCity(cityRepository.getOne(1L));
        user.setOccupation(occupationService.getById(3L));
        User result = userService.save(registration);
        Assert.assertEquals(userRepository.save(user), result);
    }

    @Test
    public void updatePassword_should_replace_oldUserPassword_with_newPassword() {
        User user = new User();
        long userId = user.getUserId();
        user.setPassword("password");

        userService.updatePassword("password", userId);
        verify(userRepository, times(1)).updatePassword("password", userId);
    }

    @Test
    public void saveRegistrationUser_should_return_user_with_confirmed_by_email_registration() {
        User user = new User();
        user.setValid(true);
        Mockito.when(userRepository.save(user)).thenReturn(user);
        User result = userService.updateUserRegistrationStatus(user);
        Assert.assertEquals(user, result);
    }

    @Test
    public void getUserToUpdate_should_return_user_by_id() {
        Optional<User> userOpt = Optional.of(new User());
        Mockito.when(userRepository.findById(anyLong())).thenReturn(userOpt);
        userService.getUserToUpdate(anyLong());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getUserToUpdate_should_return_error_if_user_do_not_exist() {
        userService.getUserToUpdate(anyLong());
    }

    @Test
    public void updateCity_should_update_user_city() {
        Mockito.when(cityRepository.findByCityName(Mockito.mock(UserUpdateDto.class).getCity())).thenReturn(Mockito.mock(City.class));
        City city = new City();
        Mockito.mock(User.class).setCity(city);
        userService.updateCity(Mockito.mock(UserUpdateDto.class), Mockito.mock(User.class));
    }

    @Test
    public void updateOccupation_should_update_user_occupation() {
        User mockUser = Mockito.mock(User.class);
        UserUpdateDto userUpdateDto = Mockito.mock(UserUpdateDto.class);
        Occupation occupationMock = Mockito.mock(Occupation.class);

        Mockito.when(userUpdateDto.getOccupation()).thenReturn(occupationMock);
        Mockito.when(occupationMock.getOccupationArea()).thenReturn("NotNone");

        userService.updateOccupation(userUpdateDto, mockUser);
        Mockito.verify(mockUser, Mockito.times(1)).setOccupation(occupationMock);
    }

    @Test
    public void updateFirstName_should_update_user_first_name() {
        UserUpdateDto userUpdateDto = Mockito.mock(UserUpdateDto.class);
        User mockUser = Mockito.mock(User.class);
        Mockito.when(userUpdateDto.getFirstName()).thenReturn(String.valueOf(String.class));
        userService.updateFirstName(userUpdateDto, mockUser);
        Mockito.verify(mockUser, Mockito.times(1)).setFirstName(userUpdateDto.getFirstName());
    }

    @Test
    public void updateLastName_should_update_user_last_name() {
        UserUpdateDto userUpdateDto = Mockito.mock(UserUpdateDto.class);
        User mockUser = Mockito.mock(User.class);
        Mockito.when(userUpdateDto.getLastName()).thenReturn(String.valueOf(String.class));
        userService.updateLastName(userUpdateDto, mockUser);
        Mockito.verify(mockUser, Mockito.times(1)).setLastName(userUpdateDto.getLastName());
    }

    @Test
    public void updateAuthority_should_update_user_authority() {
        UserUpdateDto userUpdateDto = Mockito.mock(UserUpdateDto.class);
        User mockUser = Mockito.mock(User.class);
        Authority auth = Mockito.mock(Authority.class);
        Mockito.when(authorityRepository.getByAuthorityName(notNull())).thenReturn(auth);
        mockUser.getAuthorities().clear();
        mockUser.getAuthorities().add(auth);
        userService.updateAuthority(userUpdateDto, mockUser);
    }

    @Test
    public void updatePhoto_should_update_user_coverImage_if_image_is_Null() throws IOException {
        User mockUser = Mockito.mock(User.class);
        Photo userPhoto = Mockito.mock(Photo.class);
        UserUpdateDto userUpdateDto = Mockito.mock(UserUpdateDto.class);
        MultipartFile photoMf = Mockito.mock(MultipartFile.class);

        Mockito.when(photoMf.isEmpty()).thenReturn(false);
        Mockito.when(userUpdateDto.getCoverImage()).thenReturn(photoMf);
        Mockito.when(userPhoto.getPhotoId()).thenReturn(1L);
        Mockito.when(mockUser.getPhoto()).thenReturn(userPhoto);
        Mockito.when(mockUser.getPhoto().getPhotoId()).thenReturn(1L);

        userService.updateCoverImage(userUpdateDto, mockUser);
        Mockito.verify(mockUser, Mockito.times(1)).setPhoto(Mockito.any(Photo.class));
    }

    @Test
    public void updatePhoto_should_update_user_coverImage_if_image_is_default() throws IOException {
        User mockUser = Mockito.mock(User.class);
        UserUpdateDto userUpdateDto = Mockito.mock(UserUpdateDto.class);
        MultipartFile photoMf = Mockito.mock(MultipartFile.class);
        Photo photoMock = Mockito.mock(Photo.class);

        Mockito.when(photoMf.isEmpty()).thenReturn(false);
        Mockito.when(userUpdateDto.getCoverImage()).thenReturn(photoMf);
        Mockito.when(mockUser.getPhoto()).thenReturn(photoMock);

        userService.updateCoverImage(userUpdateDto, mockUser);
        Mockito.verify(photoMock, Mockito.times(1)).setCoverImage(Mockito.any());
    }

    @Test
    public void updatePhoto_should_update_user_photo_if_image_is_default() throws IOException {
        User mockUser = Mockito.mock(User.class);
        Photo userPhoto = Mockito.mock(Photo.class);
        UserUpdateDto userUpdateDto = Mockito.mock(UserUpdateDto.class);
        MultipartFile photoMf = Mockito.mock(MultipartFile.class);

        Mockito.when(photoMf.isEmpty()).thenReturn(false);
        Mockito.when(userUpdateDto.getPhoto()).thenReturn(photoMf);
        Mockito.when(userPhoto.getPhotoId()).thenReturn(1L);
        Mockito.when(mockUser.getPhoto()).thenReturn(userPhoto);
        Mockito.when(mockUser.getPhoto().getPhotoId()).thenReturn(1L);

        userService.updatePhoto(userUpdateDto, mockUser);
        Mockito.verify(mockUser, Mockito.times(1)).setPhoto(Mockito.any(Photo.class));
    }

    @Test
    public void updatePhoto_should_update_user_photo_if_image_is_not_Null() throws IOException {
        User mockUser = Mockito.mock(User.class);
        UserUpdateDto userUpdateDto = Mockito.mock(UserUpdateDto.class);
        MultipartFile photoMf = Mockito.mock(MultipartFile.class);
        Photo photoBase = Mockito.mock(Photo.class);

        Mockito.when(photoMf.isEmpty()).thenReturn(false);
        Mockito.when(userUpdateDto.getPhoto()).thenReturn(photoMf);
        Mockito.when(mockUser.getPhoto()).thenReturn(photoBase);

        userService.updatePhoto(userUpdateDto, mockUser);
        Mockito.verify(photoBase, Mockito.times(1)).setPhoto(photoMf.getBytes());
    }

    @Test
    public void updatePhotoPrivacy_should_set_photo_privacy() {
        User mockUser = Mockito.mock(User.class);
        Photo mockPhoto = Mockito.mock(Photo.class);
        Privacy mockPrivacy = Mockito.mock(Privacy.class);
        Mockito.when(mockUser.getPhoto()).thenReturn(mockPhoto);
        userService.updatePhotoPrivacy(mockUser, mockPrivacy);
    }

    @Test
    public void updateUserFields_should_update_user_when_there_are_no_errors() throws IOException {
        User mockUser = Mockito.mock(User.class);
        Photo mockPhoto = Mockito.mock(Photo.class);
        UserUpdateDto userUpdateDto = Mockito.mock(UserUpdateDto.class);
        Occupation occupationMock = Mockito.mock(Occupation.class);
        Privacy mockPrivacy = Mockito.mock(Privacy.class);


        Mockito.when(userUpdateDto.getFirstName()).thenReturn(String.valueOf(String.class));
        Mockito.when(userUpdateDto.getLastName()).thenReturn(String.valueOf(String.class));
        Mockito.when(userUpdateDto.getOccupation()).thenReturn(occupationMock);
        Mockito.when(occupationMock.getOccupationArea()).thenReturn("NotNone");
        Mockito.when(mockUser.getPhoto()).thenReturn(mockPhoto);

        userService.updateUserFields(userUpdateDto, mockUser);
    }

    @Test
    public void updateUser() throws IOException {
        UserUpdateDto userUpdateDto = Mockito.mock(UserUpdateDto.class);
        Optional<User> userOpt = Optional.of(new User());
        Occupation occupationMock = Mockito.mock(Occupation.class);
        Photo photoMock = Mockito.mock(Photo.class);
        Privacy privacyMock = Mockito.mock(Privacy.class);

        Mockito.when(userUpdateDto.getFirstName()).thenReturn(String.valueOf(String.class));
        Mockito.when(userUpdateDto.getLastName()).thenReturn(String.valueOf(String.class));
        Mockito.when(userUpdateDto.getOccupation()).thenReturn(occupationMock);
        Mockito.when(occupationMock.getOccupationArea()).thenReturn("NotNone");
        Mockito.when(userRepository.findById(anyLong())).thenReturn(userOpt);
        User user = userOpt.get();
        user.setPhoto(photoMock);
        user.getPhoto().setPrivacy(privacyMock);

        userService.updateUser(userUpdateDto, anyLong());
    }
    @Test
    public void findAll_should_return_list_of_users_when_valid(){
        User one = new User();
        User two = new User();
        List<User> userList = new ArrayList<>();
        userList.add(one);
        userList.add(two);
        Mockito.when(userRepository.findAll()).thenReturn(userList);
        List<User> result = userService.findAll();
        Assert.assertEquals(userList.size(),result.size());
    }

    @Test
    public void findAllByEnabled_should_return_list_of_users_when_valid(){
        User one = new User();
        one.setValid(true);
        User two = new User();
        two.setValid(true);
        List<User> userList = new ArrayList<>();
        userList.add(one);
        userList.add(two);
        Mockito.when(userRepository.findAllByValidIsTrue()).thenReturn(userList);
        List<User> result = userService.findAllByEnabled();
        Assert.assertEquals(2,result.size());
    }

    @Test
    public void findAllByPages() {
        User one = new User();
        one.setValid(true);
        User two = new User();
        two.setValid(true);
        List<User> userList = new ArrayList<>();
        userList.add(one);
        userList.add(two);
        int fromPage = 0;
        int usersPerPage = 2;
        Pageable pageable = PageRequest.of(fromPage, usersPerPage, Sort.by(Sort.Direction.ASC, "email"));
        Mockito.when(userRepository.findAllByValidIsTrue(pageable)).thenReturn(userList);
        userService.findAllByPages(fromPage, usersPerPage);
    }
}