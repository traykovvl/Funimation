package team1.socnet.services;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import team1.socnet.models.Authority;
import team1.socnet.models.Comment;
import team1.socnet.models.User;
import team1.socnet.repositories.CommentRepository;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

public class ReplyCommentServiceImplTest {
    @Mock
    CommentRepository commentRepository;
    @InjectMocks
    ReplyCommentServiceImpl replyCommentServiceImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findAll_should_return_list_Of_replies_to_comment() {
        Comment comment = Mockito.mock(Comment.class);
        when(commentRepository.findAllByCommentToReplyIsNotNullOrderByTimeStampAsc()).thenReturn(Arrays.<Comment>asList(comment));

        List<Comment> result = replyCommentServiceImpl.findAll();
        Assert.assertEquals(Arrays.<Comment>asList(comment), result);
    }

    @Test
    public void saveReply_should_save_new_replyToComment() {
        Comment reply = Mockito.mock(Comment.class);
        Comment comment = Mockito.mock(Comment.class);
        User testUser = new User("firstName", "lastName", "email", "password", new HashSet<Authority>(Arrays.asList(new Authority())), true);
        replyCommentServiceImpl.saveReply(reply, testUser, comment);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateReply_should_return_error_if_not_exists() {
        replyCommentServiceImpl.updateReply(new Comment());
    }
    @Test
    public void updateReply_should_update_reply_if_exists() {
        Optional<Comment> replyOpt = Optional.of(new Comment());
        Comment reply = Mockito.mock(Comment.class);
        Comment dbReply = Mockito.mock(Comment.class);
        Mockito.when(commentRepository.findById(anyLong())).thenReturn(replyOpt);
        Mockito.when(commentRepository.getOne(dbReply.getCommentId())).thenReturn(dbReply);
        replyCommentServiceImpl.updateReply(reply);
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteReply_should_return_error_message_when_not_exists() {
        replyCommentServiceImpl.deleteReply(0L);
    }
    @Test
    public void deleteReply_should_delete_replyToComment_when_exists() {
        Comment replyOpt = Mockito.mock(Comment.class);
        Mockito.when(commentRepository.findById(anyLong())).thenReturn(Optional.of(replyOpt));
        replyCommentServiceImpl.deleteReply(replyOpt.getCommentId());
    }

    @Test
    public void findAllRepliesToComment_should_return_list_Of_replies_to_specific_comment() {
        Comment reply = Mockito.mock(Comment.class);
        when(commentRepository.findAllByCommentToReplyEqualsOrderByTimeStampAsc(any())).thenReturn(Arrays.<Comment>asList(reply));

        List<Comment> result = replyCommentServiceImpl.findAllRepliesToComment(reply);
        Assert.assertEquals(Arrays.<Comment>asList(reply), result);
    }
}