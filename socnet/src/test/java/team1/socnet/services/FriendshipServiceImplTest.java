package team1.socnet.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import team1.socnet.models.Friendship;
import team1.socnet.models.FriendshipStatus;
import team1.socnet.models.User;
import team1.socnet.repositories.FriendShipRepository;
import team1.socnet.repositories.FriendshipStatusRepository;
import team1.socnet.repositories.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class FriendshipServiceImplTest {
    @Mock
    FriendShipRepository mockFriendShipRepository;
    @Mock
    UserRepository userRepository;
    @Mock
    FriendshipStatusRepository friendshipStatusRepository;

    @InjectMocks
    FriendshipServiceImpl friendShipService;

    @Test(expected = IllegalArgumentException.class)
    public void saveFriendshipRequest_should_return_error_message_when_friendshipRequest_exists() {
        Optional<User> userOpt = Optional.of(new User());
        Mockito.when(userRepository.findById((long) 0)).thenReturn(userOpt);
        User testUser = friendShipService.getUserById((long) 0);
        User mockUser1 = new User();
        FriendshipStatus status = new FriendshipStatus();
        status.setStatus("pending");
        Friendship friendShip = new Friendship(testUser, mockUser1, status);
        Mockito.when(mockFriendShipRepository.findByUserReceivingRequestAndUserSendingRequest(testUser, mockUser1))
                .thenReturn(friendShip);
        friendShipService.saveFriendshipRequest(testUser.getUserId(), mockUser1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveFriendshipRequest_should_return_error_message_when_friendship_exists() {
        Optional<User> userOpt = Optional.of(new User());
        Mockito.when(userRepository.findById((long) 0)).thenReturn(userOpt);
        User testUser = friendShipService.getUserById((long) 0);
        User mockUser1 = new User();
        FriendshipStatus status = new FriendshipStatus();
        status.setStatus("connected");
        Friendship friendShip = new Friendship(testUser, mockUser1, status);
        Mockito.when(mockFriendShipRepository.findByUserReceivingRequestAndUserSendingRequest(testUser, mockUser1))
                .thenReturn(friendShip);
        friendShipService.saveFriendshipRequest(testUser.getUserId(), mockUser1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getFriendShipStatus_should_return_error_message_when_status_not_exists() {
        friendShipService.getFriendshipStatus("none");
    }

    @Test
    public void saveFriendshipRequest_should_create_new_friendship_when_valid() {
        Optional<User> userOpt = Optional.of(new User());
        Mockito.when(userRepository.findById((long) 0)).thenReturn(userOpt);
        User testUser = friendShipService.getUserById((long) 0);
        User mockUser1 = new User();
        FriendshipStatus status = new FriendshipStatus();
        Mockito.when(friendshipStatusRepository.findByStatus("pending")).thenReturn(status);
        friendShipService.saveFriendshipRequest(testUser.getUserId(), mockUser1);
        Mockito.verify(mockFriendShipRepository, Mockito.times(1))
                .findByUserReceivingRequestAndUserSendingRequest(testUser, mockUser1);
    }

    @Test
    public void getUser_should_return_user_by_id() {
        Optional<User> userOpt = Optional.of(new User());
        Mockito.when(userRepository.findById((long) 0)).thenReturn(userOpt);
        User testUser = friendShipService.getUserById((long) 0);
        Assert.assertEquals(userOpt.get().getUserId(), testUser.getUserId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getUser_should_return_error_message_when_not_valid() {
        friendShipService.getUserById(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void acceptFriendshipRequest_should_return_error_message_when_not_valid() {
        Optional<User> userOpt = Optional.of(new User());
        Mockito.when(userRepository.findById((long) 0)).thenReturn(userOpt);
        User testUser = friendShipService.getUserById((long) 0);
        User mockUser1 = new User();
        friendShipService.acceptFriendshipRequest(testUser.getUserId(), mockUser1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void acceptFriendshipRequest_should_return_message_friendship_exists_when_not_valid() {
        Optional<User> userOpt = Optional.of(new User());
        Mockito.when(userRepository.findById((long) 0)).thenReturn(userOpt);
        User testUser = friendShipService.getUserById((long) 0);
        User mockUser1 = new User();
        FriendshipStatus status = new FriendshipStatus();
        status.setStatus("connected");
        Friendship friendShip = new Friendship(testUser, mockUser1, status);
        Mockito.when(mockFriendShipRepository.findByUserReceivingRequestAndUserSendingRequest(testUser, mockUser1))
                .thenReturn(friendShip);
        friendShipService.acceptFriendshipRequest(testUser.getUserId(), mockUser1);
    }

    @Test
    public void acceptFriendshipRequest_should_update_friendship_status_when_valid() {
        Optional<User> userOpt = Optional.of(new User());
        Mockito.when(userRepository.findById((long) 0)).thenReturn(userOpt);
        User testUser = friendShipService.getUserById((long) 0);
        User mockUser1 = new User();
        FriendshipStatus status = new FriendshipStatus();
        status.setStatus("pending");
        Mockito.when(friendshipStatusRepository.findByStatus("connected")).thenReturn(status);
        Friendship friendShip = new Friendship(mockUser1, testUser, status);
        Mockito.when(mockFriendShipRepository.findByUserReceivingRequestAndUserSendingRequest(testUser, mockUser1))
                .thenReturn(friendShip);
        friendShipService.acceptFriendshipRequest(testUser.getUserId(), mockUser1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteFriendship_should_return_error_message_when_not_exists() {
        friendShipService.deleteFriendship(0);
    }

    @Test
    public void deleteFriendship_should_delete_friendship_when_valid() {
        Friendship friendShip = new Friendship();
        Mockito.when(mockFriendShipRepository.findByConnectionId(friendShip.getConnectionId()))
                .thenReturn(friendShip);
        friendShipService.deleteFriendship(friendShip.getConnectionId());
        Mockito.verify(mockFriendShipRepository, Mockito.times(1))
                .deleteById(friendShip.getConnectionId());
    }

    @Test
    public void should_return_listOfFriends_when_valid() {
        User user = new User();
        User mockUser1 = new User();
        User mockUser2 = new User();
        FriendshipStatus status = new FriendshipStatus();
        Friendship friendShip = new Friendship(mockUser1, user, status);
        Friendship friendship1 = new Friendship(user, mockUser2, status);
        List<Friendship> friendshipList = new ArrayList<>();
        friendshipList.add(friendShip);
        friendshipList.add(friendship1);

        Mockito.when(mockFriendShipRepository
                .findByFriendshipStatus_StatusAndUserReceivingRequestOrUserSendingRequestAndFriendshipStatus_Status("connected", user, user, "connected"))
                .thenReturn(friendshipList);
        List<Friendship> result = friendShipService.findAllFriends(user);
        Assert.assertEquals(friendshipList.size(), result.size());
    }

    @Test
    public void findAllPendingFriendShips_should_return_list_of_pending_requests() {
        User user = new User();
        User mockUser1 = new User();
        User mockUser2 = new User();
        FriendshipStatus status = new FriendshipStatus();
        Friendship friendShip = new Friendship(user, mockUser1, status);
        Friendship friendship1 = new Friendship(user, mockUser2, status);
        List<Friendship> friendshipList = new ArrayList<>();
        friendshipList.add(friendShip);
        friendshipList.add(friendship1);

        Mockito.when(mockFriendShipRepository
                .findByUserReceivingRequestAndFriendshipStatus_Status(user, "pending"))
                .thenReturn(friendshipList);
        List<Friendship> result = friendShipService.findAllPendingFriends(user);
        Assert.assertEquals(friendshipList.size(), result.size());
    }

    @Test
    public void areFriends_should_return_false_when_not_valid() {
        Optional<User> userOpt = Optional.of(new User());
        Mockito.when(userRepository.findById((long) 0)).thenReturn(userOpt);
        User testUser = friendShipService.getUserById((long) 0);
        User mockUser1 = new User();
        FriendshipStatus status = new FriendshipStatus();
        status.setStatus("pending");
        Friendship friendShip = new Friendship(mockUser1, testUser, status);
        Mockito.when(mockFriendShipRepository.findByUserReceivingRequestAndUserSendingRequest(testUser, mockUser1))
                .thenReturn(friendShip);
        friendShipService.areFriends(0, mockUser1);
        Assert.assertFalse(mockFriendShipRepository
                .existsByConnectionIdAndFriendshipStatus_Status(friendShip.getConnectionId(), "connected"));
    }
}

