package team1.socnet.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import team1.socnet.models.Authority;
import team1.socnet.repositories.AuthorityRepository;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class AuthorityServiceImplTest {
    @Mock
    AuthorityRepository mockAuthorityRepository;
    @InjectMocks
    AuthorityServiceImpl authorityService;

    @Test
    public void findAll_should_return_list_of_authorities(){
        Authority authority1 = new Authority();
        Authority authority2 = new Authority();
        List<Authority> authorityList = new ArrayList<>();
        authorityList.add(authority1);
        authorityList.add(authority2);
        Mockito.when(mockAuthorityRepository.findAll()).thenReturn(authorityList);
        List<Authority> result = authorityService.findAll();
        Assert.assertEquals(authorityList.get(0),result.get(0));
    }

    @Test(expected = EntityNotFoundException.class)
    public void getUserAuthority_should_return_error_message_when_not_exist(){
        authorityService.getUserAuthority("admin");
    }

    @Test
    public void getUserAuthority_should_return_authority_when_valid(){
        Authority authority = new Authority();
        authority.setAuthorityName("name");
        Mockito.when(mockAuthorityRepository.getByAuthorityName("name")).thenReturn(authority);
        Authority result = authorityService.getUserAuthority("name");
        Assert.assertEquals("name",result.getAuthorityName());
    }
    @Test
    public void updateAuthority_should_save_authority(){
        Authority authority = new Authority();
        authorityService.updateAuthority(authority);
        Mockito.verify(mockAuthorityRepository, Mockito.times(1)).save(authority);
    }
}
