package team1.socnet.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import team1.socnet.models.Occupation;
import team1.socnet.repositories.OccupationRepository;
import team1.socnet.services.OccupationServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyLong;

@RunWith(MockitoJUnitRunner.class)
public class OccupationServiceImplTest {

    @Mock
    OccupationRepository occupationRepository;

    @InjectMocks
    OccupationServiceImpl occupationService;

    @Test(expected = IllegalArgumentException.class)
    public void getById_should_return_error_message_when_not_exists(){
        occupationService.getById((long) 0);
    }
    @Test
    public void getById_should_return_occupation_by_validId() {
        Optional<Occupation> occupationOpt = Optional.of(new Occupation());
        Mockito.when(occupationRepository.findById(anyLong())).thenReturn(occupationOpt);
        occupationService.getById(anyLong());
    }

    @Test
    public void findAll_should_return_all_occupations_list() {
        List<Occupation> occupationList = new ArrayList<>();
        Mockito.when(occupationRepository.findAll()).thenReturn(occupationList);
        List<Occupation> result = occupationService.findAll();
        Assert.assertEquals(occupationList.size(),result.size());
    }

}