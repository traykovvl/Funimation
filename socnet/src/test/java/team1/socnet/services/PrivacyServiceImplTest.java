package team1.socnet.services;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import team1.socnet.models.Privacy;
import team1.socnet.repositories.PrivacyRepository;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

public class PrivacyServiceImplTest {
    @Mock
    PrivacyRepository privacyRepository;
    @InjectMocks
    PrivacyServiceImpl privacyServiceImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findAll_should_return_list_of_privacy() {
        Privacy privacy = new Privacy();
        when(privacyRepository.findAllByOrderByPrivacyIdDesc()).thenReturn(Arrays.<Privacy>asList(privacy));

        List<Privacy> result = privacyServiceImpl.findAll();
        Assert.assertEquals(Arrays.<Privacy>asList(privacy), result);
    }
}