package team1.socnet.services;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import team1.socnet.models.Authority;
import team1.socnet.models.Post;
import team1.socnet.models.Privacy;
import team1.socnet.models.User;
import team1.socnet.repositories.PostRepository;
import team1.socnet.repositories.PrivacyRepository;
import team1.socnet.repositories.UserRepository;
import team1.socnet.services.contracts.FriendshipService;

import java.util.*;

import static org.mockito.Mockito.*;

public class PostServiceImplTest {
    @Mock
    PostRepository postRepository;
    @Mock
    UserRepository userRepository;
    @Mock
    PrivacyRepository privacyRepository;
    @Mock
    FriendshipService friendShipService;
    @InjectMocks
    PostServiceImpl postServiceImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findAll_should_return_list_of_posts() {
        Post post = new Post();
        List<Post> postList = new ArrayList<>();
        postList.add(post);
        Mockito.when(postRepository.findAll()).thenReturn(postList);
        List<Post> result = postServiceImpl.findAll();
        Assert.assertEquals(result.get(0), postList.get(0));
    }

    @Test
    public void findPublicPost_should_return_public_posts() {
        Post post = new Post("title", "postMessage", new User("firstName", "lastName", "email", "password", new HashSet<Authority>(Arrays.asList(new Authority())), true));
        when(postRepository.findAllByPrivacy_PrivacyTypeAndTitleIsNotContainingOrderByTimeStampDesc(anyString(), anyString())).thenReturn(Arrays.<Post>asList(post));
        List<Post> result = postServiceImpl.findPublicPost();
        Assert.assertEquals(Arrays.<Post>asList(post), result);
    }

    @Test
    public void findAllRequestOfferForKids_should_return_list_of_request_for_offers_for_kids_entertainer() {
        Post post = new Post("Request offer for kid's entertainer", "postMessage", new User("firstName", "lastName", "email", "password", new HashSet<Authority>(Arrays.asList(new Authority())), true));
        when(postRepository.findAllByTitleOrderByTimeStampDesc("Request offer for kid's entertainer")).thenReturn(Arrays.<Post>asList(post));
        List<Post> result = postServiceImpl.findAllRequestOfferForKids();
        Assert.assertEquals(Arrays.<Post>asList(post), result);
    }

    @Test
    public void findAllRequestOffersForAdults_should_return_list_of_request_for_offers_for_adults_entertainer() {
        Post post = new Post("Request offer for stand up comedian", "postMessage", new User("firstName", "lastName", "email", "password", new HashSet<Authority>(Arrays.asList(new Authority())), true));
        when(postRepository.findAllByTitleOrderByTimeStampDesc("Request offer for stand up comedian")).thenReturn(Arrays.<Post>asList(post));
        List<Post> result = postServiceImpl.findAllRequestOffersForAdults();
        Assert.assertEquals(Arrays.<Post>asList(post), result);
    }

    @Test
    public void filterAllPublicPostByPostMessageContaining_should_return_list_of_posts_by_given_param() {
        Post post = Mockito.mock(Post.class);
        Privacy privacy = Mockito.mock(Privacy.class);
        Mockito.when(post.getPostMessage()).thenReturn("any");
        Mockito.when(post.getPrivacy()).thenReturn(privacy);
        Mockito.when(privacy.getPrivacyType()).thenReturn("public");
        String filterParam = "any";
        when(postRepository.findAllByTitleContainsOrPostMessageContainsAndPrivacy_PrivacyType(filterParam, filterParam, "public")).thenReturn(Collections.singletonList(post));
        List<Post> result = postServiceImpl.filterAllPublicPostByPostMessageContaining(filterParam);
        Assert.assertEquals(Collections.singletonList(post), result);
    }

    @Test
    public void savePost_should_save_new_entity() {
        User user = new User("firstName", "lastName", "email", "password", new HashSet<Authority>(Arrays.asList(new Authority())), true);
        Post post = new Post(null, null, user);
        Privacy privacy = new Privacy();
        List<Privacy> privacyList = new ArrayList<>();
        privacyList.add(privacy);
        Mockito.when(privacyRepository.findAll()).thenReturn(privacyList);
        post.setPrivacy(privacyList.get(0));
        postServiceImpl.savePost(post, user);
    }

    @Test(expected = NullPointerException.class)
    public void savePost_should_return_error() {
        User user = new User("firstName", "lastName", "email", "password", new HashSet<Authority>(Arrays.asList(new Authority())), true);
        Post post = new Post(null, null, user);
        postServiceImpl.savePost(post, user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findById_should_return_error_message_when_entity_not_exist() {
        postServiceImpl.findById(0L);
    }

    @Test
    public void findById_should_return_entity_when_valid() {
        Post post = new Post("title", "postMessage", new User("firstName",
                "lastName", "email", "password", new HashSet<Authority>(Arrays.asList(new Authority())), true));
        Mockito.when(postRepository.findById(0L)).thenReturn(Optional.of(post));
        Mockito.when(postRepository.getOne(0L)).thenReturn(post);
        Post result = postServiceImpl.findById(0L);
        Assert.assertEquals(post, result);
    }

    @Test
    public void updatePost_should_amend_entity() {
        Post post = new Post("title", "postMessage", new User("firstName", "lastName", "email", "password", new HashSet<Authority>(Arrays.asList(new Authority())), true));
        Mockito.when(postRepository.getOne(0L)).thenReturn(post);
        postServiceImpl.updatePost(post);
        Mockito.verify(postRepository, Mockito.times(1)).save(post);
    }

    @Test(expected = IllegalArgumentException.class)
    public void deletePost_should_return_error_message_when_entity_not_exists() {
        postServiceImpl.deletePost(0L);
    }

    @Test
    public void deletePost_should_delete_entry_when_valid() {
        Post post = new Post();
        Mockito.when(postRepository.findById(0L)).thenReturn(Optional.of(post));
        postServiceImpl.deletePost(0L);
        Mockito.verify(postRepository, Mockito.times(1)).deleteById(0L);
    }

    @Test
    public void findByAuthor_should_return_list_of_authors_posts() {
        Post post = new Post("title", "postMessage", new User("firstName", "lastName", "email", "password", new HashSet<Authority>(Arrays.asList(new Authority())), true));
        when(postRepository.findByAuthorOrderByTimeStampDesc(any())).thenReturn(Arrays.<Post>asList(post));
        when(userRepository.findByUserId(anyLong())).thenReturn(new User("firstName", "lastName", "email", "password", new HashSet<Authority>(Arrays.asList(new Authority())), true));

        List<Post> result = postServiceImpl.findByAuthor(0L);
        Assert.assertEquals(Arrays.<Post>asList(post), result);
    }

    @Test
    public void findAuthorPublicPost_should_return_only_public_post_of_author() {
        Post post = new Post("title", "postMessage", new User("firstName", "lastName", "email", "password", new HashSet<Authority>(Arrays.asList(new Authority())), true));
        when(postRepository.findByAuthorAndPrivacy_PrivacyTypeOrderByTimeStampDesc(any(), anyString())).thenReturn(Arrays.<Post>asList(post));
        when(userRepository.findByUserId(anyLong())).thenReturn(new User("firstName", "lastName", "email", "password", new HashSet<Authority>(Arrays.asList(new Authority())), true));

        List<Post> result = postServiceImpl.findAuthorPublicPost(0L);
        Assert.assertEquals(Arrays.<Post>asList(post), result);
    }

    @Test
    public void allFriendsPosts_should_return_list_of_friends_and_authors_posts() {

        User user = new User("firstName", "lastName", "email", "password", new HashSet<Authority>(Arrays.asList(new Authority())), true);
        Post post = new Post(null, null, user);
        when(postRepository.findAllByOrderByTimeStampDesc()).thenReturn(Arrays.<Post>asList(post));
        when(friendShipService.areFriends(anyLong(), any())).thenReturn(true);

        List<Post> result = postServiceImpl.allFriendsPosts(user);
        Assert.assertEquals(Arrays.<Post>asList(post), result);
    }

    @Test
    public void likeDislikePost_should_add_entity_to_list() {
        List<User> postLikes = new ArrayList<>();
        User userTest = new User();
        postLikes.add(userTest);
        Post postToLike = new Post();
        User loggedUser = new User();
        boolean userExists = postLikes
                .stream()
                .anyMatch(user -> user.getUserId() == loggedUser.getUserId());

        if (!userExists) {
            postLikes.add(loggedUser);
        } else {
            postLikes.stream()
                    .filter(likedByUser -> likedByUser.getUserId() == loggedUser.getUserId())
                    .findFirst()
                    .ifPresent(postLikes::remove);
        }
        postToLike.setUsersWhoLikedPost(postLikes);
        postServiceImpl.likeDislikePost(postToLike, loggedUser);
        Mockito.verify(postRepository, Mockito.times(1)).save(postToLike);
    }

    @Test
    public void likeDislikePost_should_remove_userLike_from_list_when_unlikes() {
        List<User> postLikes = new ArrayList<>();
        User userTest = new User();
        postLikes.add(userTest);
        Post postToLike = new Post();
        User loggedUser = new User();
        postLikes.add(loggedUser);
        boolean userExists = postLikes
                .stream()
                .anyMatch(user -> user.getUserId() == loggedUser.getUserId());

        if (!userExists) {
            postLikes.add(loggedUser);
        } else {
            postLikes.stream()
                    .filter(likedByUser -> likedByUser.getUserId() == loggedUser.getUserId())
                    .findFirst()
                    .ifPresent(postLikes::remove);
        }
        postToLike.setUsersWhoLikedPost(postLikes);
        postServiceImpl.likeDislikePost(postToLike, loggedUser);
        Mockito.verify(postRepository, Mockito.times(1)).save(postToLike);
    }
}
